/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-event
 *  File       : ExampleAnnotationBasedEventListener.java
 *  ClassName  : ExampleAnnotationBasedEventListener
 *  Modified   : 140411412
 */

package com.deboxsoft.event.mock;

import com.deboxsoft.event.api.EventListener;

public class ExampleAnnotationBasedEventListener {
    public MockEvent event;

    @EventListener
    public void onEvent(MockEvent event){
        this.event = event;
    }

    public void reset(){
        event = null;
    }

}
