/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-event
 *  File       : MockEvent.java
 *  ClassName  : MockEvent
 *  Modified   : 140411412
 */

package com.deboxsoft.event.mock;

public class MockEvent {}
