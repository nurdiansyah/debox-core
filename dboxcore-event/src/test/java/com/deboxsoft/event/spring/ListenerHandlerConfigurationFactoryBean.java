/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-event
 *  File       : ListenerHandlerConfigurationFactoryBean.java
 *  ClassName  : ListenerHandlerConfigurationFactoryBean
 *  Modified   : 140411411
 */

package com.deboxsoft.event.spring;

import com.deboxsoft.event.config.ListenerHandlerConfiguration;
import com.deboxsoft.event.spi.ListenerHandler;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

@ContextConfiguration(locations = "classpath:event-registration-context.xml")

public class ListenerHandlerConfigurationFactoryBean extends AbstractFactoryBean<ListenerHandlerConfiguration> {
    private final List<ListenerHandler> listenerHandlers;

    public ListenerHandlerConfigurationFactoryBean(final List<ListenerHandler> listenerHandlers) {
        this.listenerHandlers = listenerHandlers;
    }

    @Override
    public Class<?> getObjectType() {
        return ListenerHandlerConfiguration.class;
    }

    @Override
    protected ListenerHandlerConfiguration createInstance() throws Exception {
        return new ListenerHandlerConfiguration() {
            @Override
            public List<ListenerHandler> getListenerHandlers() {
                return listenerHandlers;
            }
        };
    }
}
