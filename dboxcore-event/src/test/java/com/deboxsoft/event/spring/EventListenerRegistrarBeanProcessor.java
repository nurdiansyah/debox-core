/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-event
 *  File       : EventListenerRegistrarBeanProcessor.java
 *  ClassName  : EventListenerRegistrarBeanProcessor
 *  Modified   : 140411412
 */

package com.deboxsoft.event.spring;

import com.deboxsoft.event.api.EventListenerRegistrar;
import com.deboxsoft.event.config.ListenerHandlerConfiguration;
import com.deboxsoft.event.spi.ListenerHandler;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.Ordered;

import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class EventListenerRegistrarBeanProcessor implements DestructionAwareBeanPostProcessor,
        BeanFactoryAware,
        Ordered,
        ApplicationListener {

    private static final Logger logger = LoggerFactory.getLogger(EventListenerRegistrarBeanProcessor.class);

    private final String eventListenerRegistrarBeanName;
    private final ListenerHandlerConfiguration listenerHandlersConfiguration;

    private final Map<String, Object> listenersToBeRegistered = Maps.newHashMap();

    private ConfigurableBeanFactory beanFactory;
    private EventListenerRegistrar eventListenerRegistrar;
    private boolean ignoreFurtherBeanProcessing;

    public EventListenerRegistrarBeanProcessor(
            String eventListenerRegistrarBeanName,
            ListenerHandlerConfiguration listenerHandlersConfiguration) {
        this.eventListenerRegistrarBeanName = checkNotNull(eventListenerRegistrarBeanName);
        this.listenerHandlersConfiguration = checkNotNull(listenerHandlersConfiguration);
    }

    @Override
       public int getOrder()
       {
           // process EventListenerRegistrarBeanProcessor as early as possible to guarantee that we don't get passed any AOP-ed proxies.
           return 1;
       }
   

       @Override
       public void onApplicationEvent(ApplicationEvent applicationEvent)
       {
           if (applicationEvent instanceof ContextRefreshedEvent)
           {
               // Once the spring context has been refreshed the only beans to be processed from that point onwards will be prototypes.
               // These should not be listening for Events in the first place, and so we can safely ignore them.
               ignoreFurtherBeanProcessing = true;
           }
       }
   
       @Override
       public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException
       {
           if (beanName.equals(eventListenerRegistrarBeanName))
           {
               eventListenerRegistrar = (EventListenerRegistrar) bean;
               if (isAListener(this))
               {
                   // If there is a ListenerHandler for @PluginEventListener, then register ourself as a listener
                   eventListenerRegistrar.register(this);
               }
   
               for (Object object : listenersToBeRegistered.values())
               {
                   eventListenerRegistrar.register(object);
               }
   
               listenersToBeRegistered.clear();
           }
           return bean;
       }
   
       @Override
       public void postProcessBeforeDestruction(Object bean, String beanName) throws BeansException
       {
           unregisterListener(bean, beanName);
       }
   
       @Override
       public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException
       {
           if (!ignoreFurtherBeanProcessing && canBeRegisteredAsAListener(beanName, bean))
           {
               registerListener(beanName, bean);
           }
           return bean;
       }
   
       private boolean canBeRegisteredAsAListener(String beanName, Object bean)
       {
           if (isAListener(bean))
           {
               try
               {
                   // The cost of merging is relatively high, which can have a _huge_ impact for large numbers of prototype beans. eg Hibernate Validation
                   // we only care about singleton beans; the prototype beans typically have a short lifespan
                   return beanFactory.getMergedBeanDefinition(beanName).isSingleton();
               }
               catch (NoSuchBeanDefinitionException e)
               {
                   // no bean with that name; must be an anonymous bean, so register it anyway.
                   return true;
               }
           } else
           {
               return false;
           }
       }
   
       @Override
       public void setBeanFactory(BeanFactory beanFactory) throws BeansException
       {
           this.beanFactory = (ConfigurableBeanFactory) beanFactory;
       }
   
       private void registerListener(String beanName, Object bean)
       {
           logger.debug("Registering {} instance as an eventlistener", beanName);
           if (eventListenerRegistrar != null)
           {
               eventListenerRegistrar.register(bean);
           } else
           {
               listenersToBeRegistered.put(beanName, bean);
           }
       }
   
       private void unregisterListener(Object bean, String beanName)
       {
           if (eventListenerRegistrar != null)
           {
               eventListenerRegistrar.unregister(bean);
           } else
           {
               listenersToBeRegistered.remove(beanName);
           }
       }
   
       private boolean isAListener(Object object)
       {
           for (ListenerHandler handler : listenerHandlersConfiguration.getListenerHandlers())
           {
               if (!handler.getInvokers(object).isEmpty())
               {
                   return true;
               }
           }
           return false;
       }
}
