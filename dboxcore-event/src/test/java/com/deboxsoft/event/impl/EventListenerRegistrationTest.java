/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-event
 *  File       : LockFreeEventPublisherTest.java
 *  ClassName  : LockFreeEventPublisherTest
 *  Modified   : 140411408
 */

package com.deboxsoft.event.impl;

import com.deboxsoft.event.api.EventPublisher;
import com.deboxsoft.event.mock.ExampleAnnotationBasedEventListener;
import com.deboxsoft.event.mock.MockEvent;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:event-registration-context.xml")
public class EventListenerRegistrationTest {

    @Autowired
    private ExampleAnnotationBasedEventListener singletonListener;

    @Autowired
    private EventPublisher eventPublisher;

    @After
    public void setUp() throws Exception {
        singletonListener.reset();
    }

    @Test
    public void testPublish() throws Exception {
        MockEvent event = new MockEvent();
        eventPublisher.publish(event);
        assertThat(singletonListener.event, is(sameInstance(event)));
    }


}
