/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : AsynchronousEventResolver.java
 *  ClassName  : AsynchronousEventResolver
 *  Modified   : 14024312
 */

package com.deboxsoft.event.spi;

/**
 * An interface to resolve whether an event can be handled asynchronously or not.
 */
public interface AsynchronousEventResolver {
    /**
     * Tells whether the event can be handled asynchronously or not
     * @param event the event to check
     * @return {@code true} if the event can be handled asynchronously, {@code false} otherwise.
     * @throws NullPointerException if the event is {@code null}
     */
    boolean isAsynchronousEvent(Object event);
}
