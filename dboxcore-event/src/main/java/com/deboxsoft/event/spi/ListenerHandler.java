/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : ListenerHandler.java
 *  ClassName  : ListenerHandler
 *  Modified   : 14024312
 */

package com.deboxsoft.event.spi;

import java.util.List;

/**
 * Interface to find invokers for a given listener objects. A typical example might be listeners that implement a
 * specific interface or that have annotated listener methods.
 */
public interface ListenerHandler {
    /**
     * Retrieves the list of invokers for the given listener.
     * @param listener the listener object to get invokers for
     * @return a list of invokers linked to the listener object.
     */
    List<? extends ListenerInvoker> getInvokers(Object listener);
}
