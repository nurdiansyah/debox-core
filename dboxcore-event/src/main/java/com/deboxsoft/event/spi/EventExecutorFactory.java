/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : EventExecutorFactory.java
 *  ClassName  : EventExecutorFactory
 *  Modified   : 14024311
 */

package com.deboxsoft.event.spi;

import java.util.concurrent.Executor;

/**
 * <p>A factory to create executors for asynchronous event handling</p>
 */
public interface EventExecutorFactory
{
    /**
     * @return a new {@link Executor}
     */
    Executor getExecutor();
}
