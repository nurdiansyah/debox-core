/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : EventThreadPoolConfiguration.java
 *  ClassName  : EventThreadPoolConfiguration
 *  Modified   : 14024311
 */

package com.deboxsoft.event.config;

import java.util.concurrent.TimeUnit;

public interface EventThreadPoolConfiguration {
    int getCorePoolSize();

    int getMaximumPoolSize();

    long getKeepAliveTime();

    TimeUnit getTimeUnit();
}
