/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : ListenerHandlerConfiguration.java
 *  ClassName  : ListenerHandlerConfiguration
 *  Modified   : 14024311
 */

package com.deboxsoft.event.config;

import com.deboxsoft.event.spi.ListenerHandler;

import java.util.List;

/**
 * Specifies a listener handler configuration to use
 */
public interface ListenerHandlerConfiguration {
    List<ListenerHandler> getListenerHandlers();
}
