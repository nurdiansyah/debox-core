/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : AsynchronousPreferred.java
 *  ClassName  : AsynchronousPreferred
 *  Modified   : 14024312
 */

package com.deboxsoft.event.api;

import java.lang.annotation.*;

/**
 * <p>Annotation to be used with events to tell whether they can be handled asynchronously</p>
 * <p>This is the default annotation to be used with {@link com.deboxsoft.event.impl.AnnotationAsynchronousEventResolver}</p>
 * @see com.deboxsoft.event.impl.AnnotationAsynchronousEventResolver
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface AsynchronousPreferred {
}
