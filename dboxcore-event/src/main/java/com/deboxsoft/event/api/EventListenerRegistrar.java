/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : EventListenerRegistrar.java
 *  ClassName  : EventListenerRegistrar
 *  Modified   : 14024312
 */

package com.deboxsoft.event.api;

public interface EventListenerRegistrar
{
    /**
     * Register a listener to receive events. All implementations must support registration of listeners
     * where event handling methods are indicated by the {@link com.deboxsoft.event.api.EventListener} annotation. Legacy
     * implementations may also support listeners which implement the now-deprecated
     *
     * @param listener The listener that is being registered
     * @throws NullPointerException if the listener is {@code null}
     * @throws IllegalArgumentException if the parameter is not found to be an actual listener
     * @see com.deboxsoft.event.api.EventListener annotation which can be used to indicate event listener methods
     */
    void register(Object listener);

    /**
     * Un-register a listener so that it will no longer receive events. If the given listener is not registered nothing
     * will happen.
     * @param listener The listener to un-register
     * @throws NullPointerException if the listener is {@code null}
     */
    void unregister(Object listener);

    /**
     * Un-register all listeners that this registrar knows about.
     */
    void unregisterAll();
}
