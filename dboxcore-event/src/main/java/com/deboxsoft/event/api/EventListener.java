/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : EventListener.java
 *  ClassName  : EventListener
 *  Modified   : 14024311
 */

package com.deboxsoft.event.api;

import java.lang.annotation.*;

/**
 * Used to annotate event listener methods. Methods should be public and take
 * one parameter which is the event to be handled.
 * <p/>
 * For example, the following class implements a simple event listener:
 * <pre><tt>      public class TestListener {
 *        &#64;EventListener
 *        public void onEvent(SampleEvent event) {
 *            System.out.println("Handled an event: " + event);
 *        }
 *    }
 * </tt></pre>
 * @see com.deboxsoft.event.impl.AnnotatedMethodsListenerHandler
 * @since 2.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Documented
public @interface EventListener
{
}
