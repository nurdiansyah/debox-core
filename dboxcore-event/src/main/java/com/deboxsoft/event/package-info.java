package com.deboxsoft.event;
/**
 * <p>This package contains the legacy event system public classes and interfaces. Those are all deprecated and one
 * should rely on interfaces in the {@link com.deboxsoft.event.api} package instead.</p>
 * <p>Classes in this package will be removed in a future version of Atlassian Event</p>
 */