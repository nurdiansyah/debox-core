/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : ClassUtils.java
 *  ClassName  : ClassUtils
 *  Modified   : 14024312
 */

package com.deboxsoft.event.impl;

import com.google.common.collect.Sets;

import java.util.Set;

final class ClassUtils {
    private ClassUtils() {
    }

    /**
     * Finds all super classes and interfaces for a given class
     *
     * @param cls The class to scan
     * @return The collected related classes found
     */
    static Set<Class<?>> findAllTypes(final Class<?> cls) {
        final Set<Class<?>> types = Sets.newLinkedHashSet();
        findAllTypes(cls, types);
        return types;
    }

    /**
     * Finds all super classes and interfaces for a given class
     *
     * @param cls   The class to scan
     * @param types The collected related classes found
     */
    static void findAllTypes(final Class<?> cls, final Set<Class<?>> types) {
        if (cls == null) {
            return;
        }

        // check to ensure it hasn't been scanned yet
        if (types.contains(cls)) {
            return;
        }

        types.add(cls);

        findAllTypes(cls.getSuperclass(), types);
        for (int x = 0; x < cls.getInterfaces().length; x++) {
            findAllTypes(cls.getInterfaces()[x], types);
        }
    }
}
