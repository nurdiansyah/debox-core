/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : AbstractEventExecutorFactory.java
 *  ClassName  : AbstractEventExecutorFactory
 *  Modified   : 14024311
 */

package com.deboxsoft.event.impl;

import com.deboxsoft.event.config.EventThreadPoolConfiguration;
import com.deboxsoft.event.spi.EventExecutorFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A {@link EventExecutorFactory} that allows the {@link Executor} to be produced with a custom {@link BlockingQueue}
 */
public abstract class AbstractEventExecutorFactory implements EventExecutorFactory {
    private final EventThreadPoolConfiguration configuration;
    private final EventThreadFactory eventThreadFactory;

    /**
     * @param configuration
     * @param eventThreadFactory
     * @since 2.0.5
     */
    public AbstractEventExecutorFactory(EventThreadPoolConfiguration configuration, EventThreadFactory eventThreadFactory) {
        this.configuration = checkNotNull(configuration);
        this.eventThreadFactory = checkNotNull(eventThreadFactory);
    }

    public AbstractEventExecutorFactory(EventThreadPoolConfiguration configuration) {
        this(configuration, new EventThreadFactory());
    }

    /**
     * @return a new {@link BlockingQueue<Runnable>} for the construction of a new {@link Executor}
     */
    protected abstract BlockingQueue<Runnable> getQueue();

    public Executor getExecutor() {
        return new ThreadPoolExecutor(configuration.getCorePoolSize(), configuration.getMaximumPoolSize(), configuration.getKeepAliveTime(), configuration.getTimeUnit(), getQueue(), eventThreadFactory);
    }
}