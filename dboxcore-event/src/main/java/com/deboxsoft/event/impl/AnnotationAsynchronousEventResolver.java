/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : AnnotationAsynchronousEventResolver.java
 *  ClassName  : AnnotationAsynchronousEventResolver
 *  Modified   : 14024312
 */

package com.deboxsoft.event.impl;

import com.deboxsoft.event.api.AsynchronousPreferred;
import com.deboxsoft.event.spi.AsynchronousEventResolver;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * <p>Annotation based {@link AsynchronousEventResolver}. This will check whether the event is annotated with the given
 * annotation.</p>
 * <p>The default annotation used is {@link com.deboxsoft.event.api.AsynchronousPreferred}</p>
 *
 * @see com.deboxsoft.event.api.AsynchronousPreferred
 * @since 2.0
 */
final class AnnotationAsynchronousEventResolver implements AsynchronousEventResolver {
    private final Class annotationClass;

    AnnotationAsynchronousEventResolver() {
        this(AsynchronousPreferred.class);
    }

    AnnotationAsynchronousEventResolver(Class annotationClass) {
        this.annotationClass = checkNotNull(annotationClass);
    }

    @SuppressWarnings("unchecked")
    public boolean isAsynchronousEvent(Object event) {
        return checkNotNull(event).getClass().getAnnotation(annotationClass) != null;
    }
}
