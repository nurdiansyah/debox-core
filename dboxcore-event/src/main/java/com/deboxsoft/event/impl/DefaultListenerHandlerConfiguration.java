/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : ListenerHandlersConfiguration.java
 *  ClassName  : ListenerHandlersConfiguration
 *  Modified   : 14024312
 */

package com.deboxsoft.event.impl;

import com.deboxsoft.event.config.ListenerHandlerConfiguration;
import com.deboxsoft.event.spi.ListenerHandler;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * <p>The default configuration that only uses the {@link com.deboxsoft.event.impl.AnnotatedMethodsListenerHandler}.</p>
 * <p>Products that need to remain backward compatible will have to override this configuration</p>
 */
public class DefaultListenerHandlerConfiguration implements ListenerHandlerConfiguration{
    public List<ListenerHandler> getListenerHandlers()
       {
           return Lists.<ListenerHandler>newArrayList(new AnnotatedMethodsListenerHandler());
       }
}
