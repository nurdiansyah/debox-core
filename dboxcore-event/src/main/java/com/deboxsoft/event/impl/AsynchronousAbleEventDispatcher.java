/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : AsynchronousAbleEventDispatcher.java
 *  ClassName  : AsynchronousAbleEventDispatcher
 *  Modified   : 14024311
 */

package com.deboxsoft.event.impl;

import com.deboxsoft.event.spi.AsynchronousEventResolver;
import com.deboxsoft.event.spi.EventDispatcher;
import com.deboxsoft.event.spi.EventExecutorFactory;
import com.deboxsoft.event.spi.ListenerInvoker;
import com.deboxsoft.utils.base.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executor;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This dispatcher will dispatch event asynchronously if:
 * <ul>
 * <li>the event 'is' asynchronous, as resolved by the {@link AsynchronousEventResolver} and</li>
 * <li>the invoker {@link com.deboxsoft.event.spi.ListenerInvoker#supportAsynchronousEvents() supports asynchronous events}</li>
 * </ul>
 *
 * @since 2.0
 */
public final class AsynchronousAbleEventDispatcher implements EventDispatcher {
    private static final Logger log = LoggerFactory.getLogger(AsynchronousAbleEventDispatcher.class);

    /**
     * An executor that execute commands synchronously
     */
    private static final Executor SYNCHRONOUS_EXECUTOR = new Executor() {
        public void execute(@NotNull Runnable command) {
            command.run();
        }
    };

    /**
     * An asynchronous executor
     */
    private final Executor asynchronousExecutor;

    private final AsynchronousEventResolver asynchronousEventResolver;

    /**
     * The only public constructor, uses an {@link com.deboxsoft.event.impl.AnnotationAsynchronousEventResolver}
     *
     * @param executorFactory the executor to use for asynchronous event listener invocations
     */
    public AsynchronousAbleEventDispatcher(EventExecutorFactory executorFactory) {
        this(executorFactory, new AnnotationAsynchronousEventResolver());
    }

    AsynchronousAbleEventDispatcher(EventExecutorFactory executorFactory, AsynchronousEventResolver asynchronousEventResolver) {
        this.asynchronousEventResolver = checkNotNull(asynchronousEventResolver);
        this.asynchronousExecutor = checkNotNull(executorFactory).getExecutor();
    }

    public void dispatch(final ListenerInvoker invoker, final Object event) {
        getExecutor(checkNotNull(invoker), checkNotNull(event)).execute(new Runnable() {
            public void run() {
                try {
                    invoker.invoke(event);
                } catch (Exception e) {
                    log.error("There was an exception thrown trying to dispatch event '" + event +
                            "' from the invoker '" + invoker + "'.", e);
                }
            }
        });
    }

    private Executor getExecutor(ListenerInvoker invoker, Object event) {
        return asynchronousEventResolver.isAsynchronousEvent(event) && invoker.supportAsynchronousEvents() ? asynchronousExecutor : SYNCHRONOUS_EXECUTOR;
    }
}
