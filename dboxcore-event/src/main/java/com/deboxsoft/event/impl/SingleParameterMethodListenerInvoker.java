/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *  
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : SingleParameterMethodListenerInvoker.java
 *  ClassName  : SingleParameterMethodListenerInvoker
 *  Modified   : 14024312
 */

package com.deboxsoft.event.impl;

import com.deboxsoft.event.spi.ListenerInvoker;
import com.google.common.collect.Sets;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A listener invoker that knows how to call a given single parameter method on a given object.
 */
final class SingleParameterMethodListenerInvoker implements ListenerInvoker {
    private final Method method;
    private final Object listener;

    public SingleParameterMethodListenerInvoker(Object listener, Method method) {
        this.listener = checkNotNull(listener);
        this.method = checkNotNull(method);
    }

    public Set<Class<?>> getSupportedEventTypes() {
        return Sets.newHashSet(method.getParameterTypes());
    }

    public void invoke(Object event) {
        try {
            method.invoke(listener, event);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            if (e.getCause() == null) {
                throw new RuntimeException(e);
            } else if (e.getCause().getMessage() == null) {
                throw new RuntimeException(e.getCause());
            } else {
                throw new RuntimeException(e.getCause().getMessage(), e.getCause());
            }
        }
    }

    public boolean supportAsynchronousEvents() {
        return true;
    }

    @Override
    public String toString() {
        return "SingleParameterMethodListenerInvoker{method=" + method + ", listener=" + paranoidToString(listener) + '}';
    }

    /**
     * Calls the object's toString() method. If an exception is thrown from the object's toString() method then this
     * method falls back to the "identity" toString() (as per JVM defaults).
     *
     * @param object an Object
     * @return a human-readable String representation of the object
     */
    private String paranoidToString(Object object) {
        try {
            return String.valueOf(object);
        } catch (RuntimeException e) {
            return object.getClass().getName() + '@' + Integer.toHexString(System.identityHashCode(object));
        }
    }
}
