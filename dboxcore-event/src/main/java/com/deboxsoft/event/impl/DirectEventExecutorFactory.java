/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : DirectEventExecutorFactory.java
 *  ClassName  : DirectEventExecutorFactory
 *  Modified   : 14024311
 */

package com.deboxsoft.event.impl;

import com.deboxsoft.event.config.EventThreadPoolConfiguration;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * <p>Uses a {@link SynchronousQueue} to hand off tasks to the {@link Executor}. An attempt to to queue a task will fail if no threads are immediately available to run it</p>
 * <p/>
 * <p>See {@link ThreadPoolExecutor} for more information.</p>
 */
public class DirectEventExecutorFactory extends AbstractEventExecutorFactory {
    public DirectEventExecutorFactory(final EventThreadPoolConfiguration configuration, final EventThreadFactory eventThreadFactory) {
        super(configuration, eventThreadFactory);
    }

    public DirectEventExecutorFactory(final EventThreadPoolConfiguration configuration) {
        super(configuration);
    }

    /**
     * @return a new {@link SynchronousQueue<Runnable>}
     */
    @Override
    protected BlockingQueue<Runnable> getQueue() {
        return new SynchronousQueue<>();
    }
}
