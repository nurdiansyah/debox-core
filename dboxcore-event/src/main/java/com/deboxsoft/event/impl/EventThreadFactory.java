/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : EventThreadFactory.java
 *  ClassName  : EventThreadFactory
 *  Modified   : 14024311
 */

package com.deboxsoft.event.impl;

import com.deboxsoft.utils.base.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * <p>A thread factory that will name the threads <strong>AtlassianEvent::[thread_name]</strong>.</p>
 * <p>If you need your own {@link java.util.concurrent.ThreadFactory} we recommend delegating the Thread creation to
 * this implementation.</p>
 */
public class EventThreadFactory implements ThreadFactory {
    private final ThreadFactory delegateThreadFactory;

    public EventThreadFactory() {
        this(Executors.defaultThreadFactory());
    }

    public EventThreadFactory(ThreadFactory delegateThreadFactory) {
        this.delegateThreadFactory = checkNotNull(delegateThreadFactory);
    }

    @Override
    public Thread newThread(@NotNull final Runnable r) {
        final Thread dbx_thread = delegateThreadFactory.newThread(r);
        dbx_thread.setName("Debox-Event=>" + dbx_thread.getName());
        return dbx_thread;
    }
}
