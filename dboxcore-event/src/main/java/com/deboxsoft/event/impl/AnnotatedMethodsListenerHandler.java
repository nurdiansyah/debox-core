/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-event
 *  File       : AnnotatedMethodsListenerHandler.java
 *  ClassName  : AnnotatedMethodsListenerHandler
 *  Modified   : 14024312
 */

package com.deboxsoft.event.impl;

import com.deboxsoft.event.api.EventListener;
import com.deboxsoft.event.spi.ListenerHandler;
import com.deboxsoft.event.spi.ListenerInvoker;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * <p>A listener handler that will check for single parameter methods annotated with the given annotation.</p>
 * <p>The default annotation for methods is {@link com.deboxsoft.event.api.EventListener}.</p>
 *
 * @see com.deboxsoft.event.api.EventListener
 */
public final class AnnotatedMethodsListenerHandler implements ListenerHandler {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final String exceptionCheckPackageList = "com.deboxsoft.dao.repository";

    private final Class annotationClass;

    public AnnotatedMethodsListenerHandler() {
        this(EventListener.class);
    }

    public AnnotatedMethodsListenerHandler(Class annotationClass) {
        this.annotationClass = checkNotNull(annotationClass);
    }

    public List<? extends ListenerInvoker> getInvokers(final Object listener) {
        final List<Method> validMethods = getValidMethods(checkNotNull(listener));

        if (validMethods.isEmpty()) {
            logger.debug("Couldn't find any valid listener methods on class <{}>", listener.getClass().getName());
        }

        return Lists.transform(
                validMethods, new Function<Method, ListenerInvoker>() {
                    public ListenerInvoker apply(Method method) {
                        return new SingleParameterMethodListenerInvoker(listener, method);
                    }
                });
    }

    private List<Method> getValidMethods(Object listener) {
        final List<Method> annotatedMethods = Lists.newArrayList();
        for (Method method : listener.getClass().getMethods()) {
            if (isValidMethod(method)) {
                annotatedMethods.add(method);
            }
        }
        return annotatedMethods;
    }

    private boolean isValidMethod(Method method) {
        if (isAnnotated(method)) {
            if (hasOneAndOnlyOneParameter(method)) {
                return true;
            } else {
                throw new RuntimeException(
                        "Method <" + method + "> of class <" + method.getDeclaringClass() + "> " +
                        "is annotated with <" + annotationClass.getName() + "> but has 0 or more than 1 parameters! " +
                        "Listener methods MUST have 1 and only 1 parameter.");
            }
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    private boolean isAnnotated(Method method) {
        return method.getAnnotation(annotationClass) != null;
    }

    private boolean hasOneAndOnlyOneParameter(Method method) {
        return method.getParameterTypes().length == 1;
    }
}
