/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-event
 *  File       : LockFreeEventPublisher.java
 *  ClassName  : LockFreeEventPublisher
 *  Modified   : 140411015
 */

package com.deboxsoft.event.impl;

import com.deboxsoft.event.api.EventPublisher;
import com.deboxsoft.event.config.ListenerHandlerConfiguration;
import com.deboxsoft.event.spi.EventDispatcher;
import com.deboxsoft.event.spi.ListenerHandler;
import com.deboxsoft.event.spi.ListenerInvoker;
import com.deboxsoft.utils.base.NotNull;
import com.google.common.base.Function;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.MapMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutionException;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;

/**
 * A non-blocking implementation of the {@link com.deboxsoft.event.api.EventPublisher} interface.
 * <p/>
 * This class is a drop-in replacement for {@link EventPublisherImpl} except that it does not
 * synchronise on the internal map of event type to {@link ListenerInvoker}, and should handle
 * much higher parallelism of event dispatch.
 * <p/>
 * One can customise the event listening by instantiating with custom
 * {@link com.deboxsoft.event.spi.ListenerHandler listener handlers} and the event dispatching through
 * {@link com.deboxsoft.event.spi.EventDispatcher}. See the {@link com.deboxsoft.event.spi} package
 * for more information.
 *
 * @see com.deboxsoft.event.spi.ListenerHandler
 * @see com.deboxsoft.event.spi.EventDispatcher
 * @since 2.0.2
 */
public class LockFreeEventPublisher implements EventPublisher {
    private static final Logger logger = LoggerFactory.getLogger(LockFreeEventPublisher.class);
    /**
     * Gets the {@link ListenerInvoker invokers} for a listener
     */
    private final InvokerBuilder invokerBuilder;

    /**
     * Publishes an event.
     */
    private final Publisher publisher;

    /**
     * <strong>Note:</strong> this field makes this implementation stateful
     */
    private final Listeners listeners = new Listeners();

    /**
     * If you need to customise the asynchronous handling, you should use the
     * {@link AsynchronousAbleEventDispatcher}
     * together with a custom executor.
     * <p/>
     * You might also want to have a look at using the
     * {@link EventThreadFactory} to keep the naming
     * of event threads consistent with the default naming of the Deboxsoft Event
     * library.
     *
     * @param eventDispatcher               the event dispatcher to be used with the publisher
     * @param listenerHandlerConfiguration the list of listener handlers to be used with this publisher
     * @see AsynchronousAbleEventDispatcher
     * @see EventThreadFactory
     */
    public LockFreeEventPublisher(final EventDispatcher eventDispatcher, ListenerHandlerConfiguration listenerHandlerConfiguration) {
        this.invokerBuilder = new InvokerBuilder(checkNotNull(listenerHandlerConfiguration).getListenerHandlers());
        this.publisher = new Publisher(eventDispatcher, listeners);
    }

    @Override
    public void publish(final @NotNull Object event) {
        checkNotNull(event);
        publisher.dispatch(event);
    }

    @Override
    public void register(final @NotNull Object listener) {
        checkNotNull(listener);
        if (logger.isDebugEnabled()) logger.debug("register listener '{}'.", listener);
        listeners.register(listener, invokerBuilder.build(listener));
    }

    @Override
    public void unregister(final Object listener) {
        checkNotNull(listener);
        listeners.remove(listener);
    }

    @Override
    public void unregisterAll() {
        listeners.clear();
    }

    /**
     * Holds all configured {@link com.deboxsoft.event.spi.ListenerHandler handlers}
     */
    static final class InvokerBuilder {
        private final Iterable<ListenerHandler> listenerHandlers;


        private InvokerBuilder(final @NotNull Iterable<ListenerHandler> listenerHandlers) {
            this.listenerHandlers = checkNotNull(listenerHandlers, "listenerHandler can not null");
        }

        Iterable<ListenerInvoker> build(final Object listener) throws IllegalArgumentException {
            final ImmutableList.Builder<ListenerInvoker> builder = ImmutableList.builder();
            for (final ListenerHandler listenerHandler : listenerHandlers) {
                builder.addAll(listenerHandler.getInvokers(listener));
            }
            final List<ListenerInvoker> invokers = builder.build();
            if (invokers.isEmpty()) {
                throw new IllegalArgumentException("No listener invokers were found for listener <" + listener + ">");
            }
            return invokers;
        }
    }

    /**
     * Responsible for publishing an event.
     * <p/>
     * Must first get the Set of all ListenerInvokers that
     * are registered for that event and then use the
     * {@link EventDispatcher} to send the event to them.
     */
    static final class Publisher {
        private static final Logger logger = LoggerFactory.getLogger(Publisher.class);
        private final Listeners listeners;
        private final EventDispatcher eventDispatcher;

        /**
         * transform an event class into the relevant invokers
         */
        private final Function<Class, Iterable<ListenerInvoker>> eventClassToInvokersTransformer = new Function<Class, Iterable<ListenerInvoker>>() {
            @Override
            public Iterable<ListenerInvoker> apply(final Class eventClass) {
                return listeners.get(eventClass);
            }
        };

        Publisher(final EventDispatcher eventDispatcher, final Listeners listeners) {
            this.listeners = listeners;
            this.eventDispatcher = eventDispatcher;
        }

        public void dispatch(final Object event) {
            for (final ListenerInvoker invoker : getInvokers(event)) {
                // EVENT-14 -  we should continue to process all listeners even if one throws some horrible exception
                try {
                    eventDispatcher.dispatch(invoker, event);
                } catch (Exception e) {
                    logger.error(
                            "There was an exception thrown trying to dispatch event '" + event +
                            "' from the invoker '" + invoker + "'.", e
                    );
                }
            }
        }

        /**
         * Get all classes and interfaces an object extends or implements and then find all ListenerInvokers that apply
         *
         * @param event to find its classes/interfaces
         * @return an iterable of the invokers for those classes.
         */
        Iterable<ListenerInvoker> getInvokers(final Object event) {
            final Set<Class<?>> allEventTypes = ClassUtils.findAllTypes(event.getClass());
            return ImmutableSet.copyOf(concat(transform(allEventTypes, eventClassToInvokersTransformer)));
        }
    }

    /**
     * Maps classes to the relevant {@link Invokers}
     */
    static final class Listeners {
        private final LoadingCache<Class<?>, Invokers> invokers = CacheBuilder.newBuilder().build(
                new CacheLoader<Class<?>, Invokers>() {
                    @Override
                    public Invokers load(
                            @NotNull final Class<?> key) throws Exception {
                        return new Invokers();
                    }
                }
        );

        void register(final Object listener, @NotNull Iterable<ListenerInvoker> listenerInvokers) {
            for (ListenerInvoker listenerInvoker : listenerInvokers) {
                register(listener, listenerInvoker);
            }
        }

        private void register(final Object listener, @NotNull final ListenerInvoker listenerInvoker) {
            checkNotNull(listenerInvoker, "ListenerInvoker is null");
            if (listenerInvoker.getSupportedEventTypes().isEmpty()) {
                try {
                    invokers.get(Object.class).add(listener, listenerInvoker);
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            } else {
                for (final Class<?> eventClass : listenerInvoker.getSupportedEventTypes()) {
                    try {
                        invokers.get(eventClass).add(listener, listenerInvoker);
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        void remove(Object listener) {
            invokers.invalidate(listener);
        }

        void clear() {
            invokers.invalidateAll();
        }

        public Iterable<ListenerInvoker> get(final Class<?> eventClass) {
            try {
                return invokers.get(eventClass).all();
            } catch (ExecutionException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * map of Key to Set of ListenerInvoker
     */
    static final class Invokers {
        private final ConcurrentMap<Object, ListenerInvoker> listeners = new MapMaker().weakKeys().makeMap();

        Iterable<ListenerInvoker> all() {
            return listeners.values();
        }

        public void add(final Object key, final ListenerInvoker listenerInvoker) {
            listeners.put(key, listenerInvoker);
        }

        public void remove(final Object key) {
            listeners.remove(key);
        }
    }
}
