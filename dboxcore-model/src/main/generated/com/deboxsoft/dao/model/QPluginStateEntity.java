package com.deboxsoft.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QPluginStateEntity is a Querydsl query type for PluginStateEntity
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPluginStateEntity extends EntityPathBase<PluginStateEntity> {

    private static final long serialVersionUID = 657488857L;

    public static final QPluginStateEntity pluginStateEntity = new QPluginStateEntity("pluginStateEntity");

    public final BooleanPath enabled = createBoolean("enabled");

    public final StringPath name = createString("name");

    public QPluginStateEntity(String variable) {
        super(PluginStateEntity.class, forVariable(variable));
    }

    public QPluginStateEntity(Path<? extends PluginStateEntity> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPluginStateEntity(PathMetadata<?> metadata) {
        super(PluginStateEntity.class, metadata);
    }

}

