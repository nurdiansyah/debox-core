package com.deboxsoft.dao;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QAbstractAuditable is a Querydsl query type for AbstractAuditable
 */
@Generated("com.mysema.query.codegen.SupertypeSerializer")
public class QAbstractAuditable extends EntityPathBase<AbstractAuditable<?, ? extends java.io.Serializable>> {

    private static final long serialVersionUID = -660720592L;

    public static final QAbstractAuditable abstractAuditable = new QAbstractAuditable("abstractAuditable");

    public final QAbstractObjectPersistable _super = new QAbstractObjectPersistable(this);

    public final SimplePath<Object> createdBy = createSimple("createdBy", Object.class);

    //inherited
    public final DateTimePath<java.util.Date> createdDate = _super.createdDate;

    //inherited
    public final SimplePath<java.io.Serializable> id = _super.id;

    public final SimplePath<Object> lastModifiedBy = createSimple("lastModifiedBy", Object.class);

    //inherited
    public final DateTimePath<java.util.Date> lastModifiedDate = _super.lastModifiedDate;

    @SuppressWarnings("all")
    public QAbstractAuditable(String variable) {
        super((Class)AbstractAuditable.class, forVariable(variable));
    }

    @SuppressWarnings("all")
    public QAbstractAuditable(Path<? extends AbstractAuditable> path) {
        super((Class)path.getType(), path.getMetadata());
    }

    @SuppressWarnings("all")
    public QAbstractAuditable(PathMetadata<?> metadata) {
        super((Class)AbstractAuditable.class, metadata);
    }

}

