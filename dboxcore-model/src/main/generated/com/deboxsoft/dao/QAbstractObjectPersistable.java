package com.deboxsoft.dao;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QAbstractObjectPersistable is a Querydsl query type for AbstractObjectPersistable
 */
@Generated("com.mysema.query.codegen.SupertypeSerializer")
public class QAbstractObjectPersistable extends EntityPathBase<AbstractObjectPersistable<? extends java.io.Serializable>> {

    private static final long serialVersionUID = -1855661590L;

    public static final QAbstractObjectPersistable abstractObjectPersistable = new QAbstractObjectPersistable("abstractObjectPersistable");

    public final org.springframework.data.jpa.domain.QAbstractPersistable _super = new org.springframework.data.jpa.domain.QAbstractPersistable(this);

    public final DateTimePath<java.util.Date> createdDate = createDateTime("createdDate", java.util.Date.class);

    //inherited
    public final SimplePath<java.io.Serializable> id = _super.id;

    public final DateTimePath<java.util.Date> lastModifiedDate = createDateTime("lastModifiedDate", java.util.Date.class);

    @SuppressWarnings("all")
    public QAbstractObjectPersistable(String variable) {
        super((Class)AbstractObjectPersistable.class, forVariable(variable));
    }

    @SuppressWarnings("all")
    public QAbstractObjectPersistable(Path<? extends AbstractObjectPersistable> path) {
        super((Class)path.getType(), path.getMetadata());
    }

    @SuppressWarnings("all")
    public QAbstractObjectPersistable(PathMetadata<?> metadata) {
        super((Class)AbstractObjectPersistable.class, metadata);
    }

}

