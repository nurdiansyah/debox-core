/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-model
 *  File       : PluginStateRepo.java
 *  ClassName  : PluginStateRepo
 *  Modified   : 140410909
 */

package com.deboxsoft.dao.repository;

import com.deboxsoft.dao.model.PluginStateEntity;
import org.springframework.data.repository.CrudRepository;

public interface PluginStateRepo extends CrudRepository<PluginStateEntity, String>{
}
