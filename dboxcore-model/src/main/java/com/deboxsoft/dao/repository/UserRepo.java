/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *  
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-model
 *  File       : UserRepo.java
 *  ClassName  : UserRepo
 *  Modified   : 14036108
 */

package com.deboxsoft.dao.repository;

import com.deboxsoft.dao.model.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface UserRepo extends JpaRepository<UserEntity, Integer>, QueryDslPredicateExecutor<UserEntity> {
    UserEntity findByUsername(String username);
}
