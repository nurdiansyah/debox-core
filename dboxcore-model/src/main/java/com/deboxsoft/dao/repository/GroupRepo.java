/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-model
 *  File       : GroupRepo.java
 *  ClassName  : GroupRepo
 *  Modified   : 14036108
 */

package com.deboxsoft.dao.repository;/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-inhuaschool
 *  Module     : model
 *  File       : GroupRepo.java
 *  ClassName  : GroupRepo
 *  Modified   : 14025712
 */

import com.deboxsoft.dao.model.GroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepo extends JpaRepository<GroupEntity, Integer> {
}
