/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-model
 *  File       : AbstractAuditable.java
 *  ClassName  : AbstractAuditable
 *  Modified   : 14037210
 */

package com.deboxsoft.dao;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
public abstract class AbstractAuditable<U, PK extends Serializable> extends AbstractObjectPersistable<PK> {

    private static final long serialVersionUID = 4834362084918949158L;

    @ManyToOne
    @JoinColumn(name = "createdBy")
    private U createdBy;

    @ManyToOne
    @JoinColumn(name = "lastModifiedBy")
    private U lastModifiedBy;

    /*
    * (non-Javadoc)
    *
    * @see org.springframework.data.domain.Auditable#getCreatedBy()
    */
    public U getCreatedBy() {

        return createdBy;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.data.domain.Auditable#setCreatedBy(java.lang.Object)
     */
    public void setCreatedBy(final U createdBy) {

        this.createdBy = createdBy;
    }


    /*
         * (non-Javadoc)
         *
         * @see org.springframework.data.domain.Auditable#getLastModifiedBy()
         */
    public U getLastModifiedBy() {

        return lastModifiedBy;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.data.domain.Auditable#setLastModifiedBy(java.lang
     * .Object)
     */
    public void setLastModifiedBy(final U lastModifiedBy) {

        this.lastModifiedBy = lastModifiedBy;
    }


}
