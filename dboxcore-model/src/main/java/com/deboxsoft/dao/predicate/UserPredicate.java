/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-model
 *  File       : predicate.java
 *  ClassName  : predicate
 *  Modified   : 14037309
 */

package com.deboxsoft.dao.predicate;

import com.deboxsoft.dao.model.QUserEntity;
import com.mysema.query.types.Predicate;

public class UserPredicate {
    public static Predicate usernameFilter(final String username){
        QUserEntity userEntity = QUserEntity.userEntity;
        return userEntity.username.eq(username);
    }
}
