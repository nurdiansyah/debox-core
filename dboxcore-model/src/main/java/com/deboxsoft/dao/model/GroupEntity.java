/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-model
 *  File       : GroupEntity.java
 *  ClassName  : GroupEntity
 *  Modified   : 14037110
 */

package com.deboxsoft.dao.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Collection;

@Entity
public class GroupEntity {

    @Id
    private Long id;

    @Column(length = 50, nullable = false)
    private String name;

    @OneToMany(mappedBy = "group")
    private Collection<UserEntity> user;

    public Long getId() {
        return id;
    }

    public void setId(final Long dbx_id) {
        this.id = dbx_id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String dbx_name) {
        this.name = dbx_name;
    }

    public Collection<UserEntity> getUser() {
        return user;
    }

    public void setUser(final Collection<UserEntity> dbx_user) {
        this.user = dbx_user;
    }
}
