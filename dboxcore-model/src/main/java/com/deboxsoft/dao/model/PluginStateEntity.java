/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-model
 *  File       : PluginStateEntity.java
 *  ClassName  : PluginStateEntity
 *  Modified   : 140410906
 */

package com.deboxsoft.dao.model;

import com.deboxsoft.app.plugin.PluginState;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PluginStateEntity implements PluginState {

    @Id
    private String name;

    private boolean enabled;

    public PluginStateEntity(final String name, final boolean enabled) {
        this.name = name;
        this.enabled = enabled;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final PluginStateEntity that = (PluginStateEntity) o;

        if (enabled != that.enabled) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (enabled ? 1 : 0);
        return result;
    }
}
