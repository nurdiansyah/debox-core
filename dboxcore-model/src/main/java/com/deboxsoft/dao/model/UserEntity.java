/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-model
 *  File       : UserEntity.java
 *  ClassName  : UserEntity
 *  Modified   : 14037113
 */

package com.deboxsoft.dao.model;

import com.deboxsoft.app.user.DboxUser;
import com.deboxsoft.db.hibernate.extras.HibernateIdGenerator;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UserEntity implements DboxUser {

    @Id
    @GeneratedValue(generator = "userIdGenerator", strategy = GenerationType.TABLE)
    @TableGenerator(name = "userIdGenerator", table = HibernateIdGenerator.TABLE_GENERATOR_NAME, pkColumnName = "user")
    private Long id;

    @Column(length = 100, nullable = false)
    private String password;

    @Column(unique = true, length = 75, nullable = false)
    private String username;

    @Column(length = 100)
    private String firstName = "";

    @Column(length = 100, nullable = false)
    private String lastName;

    @Column(length = 205)
    private String displayName;


    @Column(length = 125, nullable = false)
    private String email;

    private boolean active = false;

    @ManyToOne
    private GroupEntity group;

    public void setId(final Long dbx_id) {
        this.id = dbx_id;
    }


    public Long getId() {
        return id;
    }

    public void setUsername(final String dbx_username) {
        this.username = dbx_username;
    }

    public String getUsername() {
        return username;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(final String dbx_password) {
        this.password = dbx_password;
    }

    public void setFirstName(final String dbx_firstName) {
        this.firstName = dbx_firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(final String dbx_lastName) {
        this.lastName = dbx_lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setDisplayName(final String dbx_displayName) {
        this.displayName = dbx_displayName;
    }

    public String getDisplayName() {
        if (StringUtils.isBlank(displayName)) {
            StringBuilder name = new StringBuilder();
            name.append(firstName);
            if (StringUtils.isNotBlank(lastName)) name.append(" ").append(lastName);
            return name.toString();
        } else {
            return displayName;
        }
    }

    public void setActive(final boolean dbx_active) {
        this.active = dbx_active;
    }

    public boolean isActive() {
        return active;
    }

    public void setEmail(final String dbx_email) {
        this.email = dbx_email;
    }

    public String getEmail() {
        return email;
    }

    public String getSlug() {
        return username.toLowerCase();
    }

    public GroupEntity getGroup() {
        return group;
    }

    public void setGroup(final GroupEntity dbx_group) {
        this.group = dbx_group;
    }
}
