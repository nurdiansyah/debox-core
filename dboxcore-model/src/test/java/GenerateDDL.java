/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-model
 *  File       : GenerateDDL.java
 *  ClassName  : GenerateDDL
 *  Modified   : 121235514
 */

import com.deboxsoft.db.hibernate.DboxNamingStrategy;
import com.deboxsoft.db.hibernate.dialect.DboxMysql5Dialect;
import org.hibernate.cfg.Configuration;
import org.hibernate.dialect.H2Dialect;
import org.hibernate.dialect.HSQLDialect;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SingleLineSqlCommandExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GenerateDDL {
    private static final Logger logger = LoggerFactory.getLogger(GenerateDDL.class);
    Configuration cfg;
    String packageName;

    public GenerateDDL(String packageName) throws Exception {
        this.packageName = packageName;
    }

    /**
     * @param args argument main
     */
    public static void main(String[] args) throws Exception {
        GenerateDDL gen = new GenerateDDL("com.deboxsoft.dao.model");
        gen.generate(Dialect.MYSQL);
        gen.generate(Dialect.HSQL);
        gen.generate(Dialect.H2);
        System.out.println("");
        System.out.println("==========================FINISH==========================");
        System.out.println("");
    }

    /**
     * Method that actually creates the file.
     */
    private void generate(Dialect dbx_dialect) throws Exception {
        logger.info("dialect : {}", dbx_dialect.getDialectClass());
        cfg = new Configuration();
        cfg.setNamingStrategy(new DboxNamingStrategy());
        for (Class clazz : getClasses(packageName)) {
            cfg.addAnnotatedClass(clazz);
        }
        cfg.setProperty("hibernate.dialect", dbx_dialect.getDialectClass());
        File resources = new File("dboxcore-model/src/test/resources");
        String path = resources.getAbsolutePath();
        logger.info(path);
        SchemaExport export = new SchemaExport(cfg);
        export.setDelimiter(";");
        SingleLineSqlCommandExtractor commandExtractor = new SingleLineSqlCommandExtractor();
//        commandExtractor.extractCommands(new FileReader(resources.getAbsolutePath()+ "/import.sql"));
        export.setImportSqlCommandExtractor(commandExtractor);
        export.setOutputFile(resources.getAbsolutePath() + "/ddl_" + dbx_dialect.name().toLowerCase() + ".sql");
        export.execute(true, false, false, false);
    }


    /**
     * Utility method used to fetch Class list based on a package name.
     *
     * @param packageName (should be the package containing your annotated beans.
     */
    private List<Class> getClasses(String packageName) throws Exception {
        List<Class> classes = new ArrayList<Class>();
        File directory = null;
        try {
            ClassLoader cld = Thread.currentThread().getContextClassLoader();
            if (cld == null) {
                throw new ClassNotFoundException("Can't get class loader.");
            }
            String path = packageName.replace('.', '/');
            URL resource = cld.getResource(path);
            if (resource == null) {
                throw new ClassNotFoundException("No resource for " + path);
            }
            directory = new File(resource.getFile());
        } catch (NullPointerException x) {
            throw new ClassNotFoundException(packageName + " (" + directory + ") does not appear to be a valid package");
        }
        if (directory.exists()) {
            String[] files = directory.list();
            for (String file : files) {
                if (file.endsWith(".class")) {
                    // removes the .class extension
                    classes.add(Class.forName(packageName + '.' + file.substring(0, file.length() - 6)));
                }
            }
        } else {
            throw new ClassNotFoundException(packageName + " is not a valid package");
        }

        return classes;
    }

    /**
     * Holds the classnames of hibernate dialects for easy reference.
     */
    private static enum Dialect {
        ORACLE(Oracle10gDialect.class),
        MYSQL(DboxMysql5Dialect.class),
        HSQL(HSQLDialect.class),
        H2(H2Dialect.class);
        private final Class<? extends org.hibernate.dialect.Dialect> dbx_classDialect;


        private Dialect(Class<? extends org.hibernate.dialect.Dialect> classDialect) {
            dbx_classDialect = classDialect;
        }

        public String getDialectClass() {
            return dbx_classDialect.getName();
        }
    }

}
