/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-model
 *  File       : UserRepoTest.java
 *  ClassName  : UserRepoTest
 *  Modified   : 14037316
 */

package com.deboxsoft.dao.repository;

import com.deboxsoft.dao.model.QUserEntity;
import com.deboxsoft.dao.model.UserEntity;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;

public class UserRepoTest extends AbstractTestingUnit{
    @Autowired
    private UserRepo userRepo;

    @Test
    public void testFindUsername() throws Exception {
        UserEntity userEntity = userRepo.findOne(QUserEntity.userEntity.username.eq("nurdiansyah"));
        assertEquals("nurdiansyah", userEntity.getUsername());
    }
}
