/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-plugin
 *  Module     : dboxplugin-core
 *  File       : Unzipper.java
 *  ClassName  : Unzipper
 *  Modified   : 14012322
 */

package com.deboxsoft.utils.zip;

import java.io.File;
import java.io.IOException;
import java.util.zip.ZipEntry;

public interface Unzipper {
    void unzip() throws IOException;

    void conditionalUnzip() throws IOException;

    File unzipFileInArchive(String fileName) throws IOException;

    ZipEntry[] entries() throws IOException;
}
