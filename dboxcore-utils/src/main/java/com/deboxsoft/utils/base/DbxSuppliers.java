package com.deboxsoft.utils.base;

import com.google.common.base.Function;
import com.google.common.base.Supplier;

/**
 * Useful {@link Supplier} implementations.
 */
public class DbxSuppliers {
    /**
     * A {@link Supplier} that always returns the supplied source.
     *
     * @param <T>    the type
     * @param source the object that is always returned.
     * @return a supplier that always returns the supplied argument
     */
    public static <T> Supplier<T> memoize(final T source) {
        return new Supplier<T>() {
            public T get() {
                return source;
            }
        };
    }

    /**
     * A {@link Supplier} that asks the argument function for the result using the
     * input argument.
     *
     * @param <D>      the input type
     * @param <T>      the result type
     * @param input    used as the argument when calling the function.
     * @param function asked to get the result.
     * @return the result
     */
    public static <D, T> Supplier<T> fromFunction(final D input, final Function<D, T> function) {
        return new Supplier<T>() {
            public T get() {
                return function.apply(input);
            }
        };
    }

    /**
     * Map to a google-collections Supplier.
     *
     * @param <T>      type
     * @param supplier the function to map
     * @return the mapped function.
     */
    public static <T> com.google.common.base.Supplier<T> toGoogleSupplier(final Supplier<T> supplier) {
        return new ToGoogleAdapter<>(supplier);
    }

    static class ToGoogleAdapter<T> implements com.google.common.base.Supplier<T> {
        private final Supplier<T> delegate;

        ToGoogleAdapter(final Supplier<T> delegate) {
            this.delegate = delegate;
        }

        @Override
        public T get() {
            return delegate.get();
        }
    }

    /**
     * Map from a google-collections Supplier.
     *
     * @param <T>      type
     * @param supplier the function to map
     * @return the mapped function.
     */
    public static <T> Supplier<T> fromGoogleSupplier(final com.google.common.base.Supplier<T> supplier) {
        return new FromGoogleAdapter<>(supplier);
    }

    static class FromGoogleAdapter<T> implements Supplier<T> {
        private final com.google.common.base.Supplier<T> delegate;

        FromGoogleAdapter(final com.google.common.base.Supplier<T> delegate) {
            this.delegate = delegate;
        }

        @Override
        public T get() {
            return delegate.get();
        }
    }

    /**
         * @return a supplier that always supplies {@code instance}.
         */
        public static <T> Supplier<T> ofInstance(final T instance) {
            return com.google.common.base.Suppliers.ofInstance(instance);
        }

        /**
         * @return a supplier that always supplies {@code true}.
         */
        public static Supplier <Boolean> alwaysTrue() {
            return SupplyTrue.INSTANCE;
        }

        /**
         * @return a supplier that always supplies {@code false}.
         */
        public static Supplier<Boolean> alwaysFalse() {
            return SupplyFalse.INSTANCE;
        }

        /**
         * @return a supplier that always supplies {@code null}.
         */
        public static <A> Supplier<A> alwaysNull() {
            @SuppressWarnings("unchecked")
            final Supplier<A> result = (Supplier<A>) Nulls.NULL;
            return result;
        }

        private enum SupplyTrue implements Supplier<Boolean> {
            INSTANCE;

            public Boolean get() {
                return true;
            }
        }

        private enum SupplyFalse implements Supplier<Boolean> {
            INSTANCE;

            public Boolean get() {
                return false;
            }
        }

        enum Nulls implements Supplier<Object> {
            NULL;

            public Object get() {
                return null;
            }
        }

    // /CLOVER:OFF
    private DbxSuppliers() {
        throw new AssertionError("cannot instantiate!");
    }
    // /CLOVER:ON

}
