package com.deboxsoft.utils.base;

import com.deboxsoft.utils.collection.Iterables;
import com.deboxsoft.utils.collection.Option;
import com.deboxsoft.utils.collection.Pair;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;

import java.util.Iterator;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.ImmutableList.copyOf;

/**
 * Utility methods for DbxFunctions that are in addition to the methods on
 * {@link com.google.common.base.Functions}.
 * <p/>
 * Note that this class defines Partial DbxFunctions to be functions that return an
 * {@link com.deboxsoft.utils.collection.Option} of the result type, and has some methods for creating them.
 *
 * @since 1.1
 */
public class DbxFunctions {
    // /CLOVER:OFF

    private DbxFunctions() {
    }

    // /CLOVER:ON

    /**
     * Apply f to each element in elements, with each application using the result
     * of the previous application as the other argument to f. zero is used as the
     * first 'result' value. The final result is returned.
     *
     * @param f        the function to apply to all the elements
     * @param zero     the starting point for the function
     * @param elements the series of which each element will be accumulated into a
     *                 result
     * @return the result of accumulating the application of f to all elements
     * @since 1.1
     */
    public static <F, T> T fold(final DbxFunction2<T, F, T> f, final T zero, final Iterable<F> elements) {
        T currentValue = zero;
        for (final F element : elements) {
            currentValue = f.apply(currentValue, element);
        }
        return currentValue;
    }

    /**
     * Apply f to each element in elements, with each application using the result
     * of the previous application as the other argument to f. zero is used as the
     * first 'result' value. The final result is returned.
     *
     * @param f        the function to apply to all elements
     * @param zero     the starting point for the function
     * @param elements the series of which each element will be accumulated into a
     *                 result
     * @return the result of accumulating the application of f to all elements
     * @since 1.1
     */
    public static <F, T> T fold(final Function<Pair<T, F>, T> f, final T zero, final Iterable<F> elements) {
        return fold(toFunction2(f), zero, elements);
    }

    /**
     * Function that takes another function and applies it to the argument.
     *
     * @param <A> the argument and function input type
     * @param <B> the result type
     * @param arg the argument that will be applied to any input functions
     * @return a function that takes a function from A to B , applies the arg and
     * returns the result
     * @since 1.1
     */
    public static <A, B> Function<Function<A, B>, B> apply(final A arg) {
        return new Function<Function<A, B>, B>() {
            public B apply(final Function<A, B> f) {
                return f.apply(arg);
            }
        };
    }

    /**
     * Function that takes another function and applies it to the argument
     * supplied by the parameter.
     *
     * @param lazyA the supplier of the argument that will be applied to any input
     *              functions
     * @param <A>   the type of the argument supplied, and the function input type
     * @param <B>   the result type of the function
     * @return a function that takes a function from A to B, applies the argument
     * from the supplier and returns the result
     * @since 2.0
     */
    public static <A, B> Function<Function<A, B>, B> apply(final Supplier<A> lazyA) {
        return new Function<Function<A, B>, B>() {
            @Override
            public B apply(Function<A, B> f) {
                return f.apply(lazyA.get());
            }
        };
    }

    /**
     * Partial Function that does a type check and matches if the value is of the
     * right type.
     *
     * @param cls the type to check against.
     * @return a function that returns a Some with the value if of the right type
     * otherwise a None
     * @since 1.2
     */
    public static <A, B> Function<A, Option<B>> isInstanceOf(Class<B> cls) {
        return new InstanceOf<>(cls);
    }

    public static class InstanceOf<A, B> implements Function<A, Option<B>> {
        private final Class<B> cls;

        InstanceOf(Class<B> cls) {
            this.cls = checkNotNull(cls);
        }

        public Option<B> apply(A a) {
            return (cls.isAssignableFrom(a.getClass())) ? Option.some(cls.cast(a)) : Option.<B>none();
        }
    }

    /**
     * Create a PartialFunction from a {@link Predicate} and a {@link Function}.
     *
     * @param p the predicate to test the value against.
     * @param f the function to apply if the predicate passes.
     * @return a PartialFunction that tests the supplied predicate before applying
     * the function.
     * @since 1.2
     */
    public static <A, B> Function<A, Option<B>> partial(Predicate<? super A> p, Function<? super A, ? extends B> f) {
        return new Partial<>(p, f);
    }

    public static class Partial<A, B> implements Function<A, Option<B>> {
        private final Predicate<? super A> p;
        private final Function<? super A, ? extends B> f;

        Partial(Predicate<? super A> p, Function<? super A, ? extends B> f) {
            this.p = checkNotNull(p);
            this.f = checkNotNull(f);
        }

        public Option<B> apply(A a) {
            return (p.apply(a)) ? Option.option(f.apply(a)) : Option.<B>none();
        }
    }

    /**
     * Compose two PartialFunctions into one.
     * <p/>
     * Kleisli composition. In Haskell it is defined as <code>&gt;=&gt;</code>,
     * AKA <a href="http://stackoverflow.com/a/7833488/210216">
     * "compose, fishy, compose"</a>
     *
     * @param bc partial function from {@code B -> C}
     * @param ab partial function from {@code A -> B}
     * @return a PartialFunction that flatMaps g on to the result of applying f.
     * @since 1.2
     */
    public static <A, B, C> Function<A, Option<C>> compose(Function<? super B, ? extends Option<? extends C>> bc,
                                                           Function<? super A, ? extends Option<? extends B>> ab) {
        return new PartialComposer<>(ab, bc);
    }

    public static class PartialComposer<A, B, C> implements Function<A, Option<C>> {
        private final Function<? super A, ? extends Option<? extends B>> ab;
        private final Function<? super B, ? extends Option<? extends C>> bc;

        PartialComposer(Function<? super A, ? extends Option<? extends B>> ab, Function<? super B, ? extends Option<? extends C>> bc) {
            this.ab = checkNotNull(ab);
            this.bc = checkNotNull(bc);
        }

        public Option<C> apply(A a) {
            //noinspection ConstantConditions
            return ab.apply(a).flatMap(bc);
        }
    }

    /**
     * Converts a function that takes a pair of arguments to a function that takes
     * two arguments
     *
     * @param fpair the source function that takes a pair of arguments
     * @param <A>   the type of the left of the pair
     * @param <B>   the type of the right of the pair
     * @param <C>   the result type
     * @return a function that takes two arguments
     * @since 2.0
     */
    public static <A, B, C> DbxFunction2<A, B, C> toFunction2(final Function<Pair<A, B>, C> fpair) {
        checkNotNull(fpair);
        return new DbxFunction2<A, B, C>() {
            @Override
            public C apply(A a, B b) {
                return fpair.apply(Pair.pair(a, b));
            }
        };
    }

    /**
     * Transforms a function that takes 2 arguments into a function that takes the
     * first argument and return a new function that takes the second argument and
     * return the final result.
     *
     * @param f2  the original function that takes 2 arguments
     * @param <A> the type of the first argument
     * @param <B> the type of the second argument
     * @param <C> the type of the final result
     * @return the curried form of the original function
     * @since 2.0
     */
    public static <A, B, C> Function<A, Function<B, C>> curried(final DbxFunction2<A, B, C> f2) {
        checkNotNull(f2);
        return new CurriedFunction<>(f2);
    }

    private static class CurriedFunction<A, B, C> implements Function<A, Function<B, C>> {
        private final DbxFunction2<A, B, C> f2;

        CurriedFunction(DbxFunction2<A, B, C> f2) {
            this.f2 = f2;
        }

        @Override
        public Function<B, C> apply(final A a) {
            return new Function<B, C>() {
                @Override
                public C apply(B b) {
                    return f2.apply(a, b);
                }
            };
        }
    }

    /**
     * Transforms a function from {@code A -> (B -> C)} into a function from
     * {@code B -> (A -> C)}.
     *
     * @param f2  the original function from {@code A -> (B -> C)}
     * @param <A> the type of the first argument
     * @param <B> the type of the second argument
     * @param <C> the type of the final result
     * @return the flipped form of the original function
     * @since 2.0
     */
    public static <A, B, C> Function<B, Function<A, C>> flip(final Function<A, Function<B, C>> f2) {
        checkNotNull(f2);
        return new FlippedFunction<>(f2);
    }

    private static class FlippedFunction<A, B, C> implements Function<B, Function<A, C>> {
        private final Function<A, Function<B, C>> f2;

        FlippedFunction(Function<A, Function<B, C>> f2) {
            this.f2 = f2;
        }

        @Override
        public Function<A, C> apply(final B b) {
            return new Function<A, C>() {
                @Override
                public C apply(A a) {
                    //noinspection ConstantConditions
                    return f2.apply(a).apply(b);
                }
            };
        }
    }

    /**
     * Creates a stack of matcher functions and returns the first result that
     * matches.
     *
     * @param f1 partial function, tried in order.
     * @param f2 partial function, tried in order.
     * @return a PartialFunction that composes all the functions and tries each
     * one in sequence.
     * @since 1.2
     */
    public static <A, B> Function<A, Option<B>> matches(Function<? super A, ? extends Option<? extends B>> f1,
                                                        Function<? super A, ? extends Option<? extends B>> f2) {
        @SuppressWarnings("unchecked")
        Matcher<A, B> result = matcher(f1, f2);
        return result;
    }

    /**
     * Creates a stack of matcher functions and returns the first result that
     * matches.
     *
     * @param f1 partial function, tried in order.
     * @param f2 partial function, tried in order.
     * @param f3 partial function, tried in order.
     * @return a PartialFunction that composes all the functions and tries each
     * one in sequence.
     * @since 1.2
     */
    public static <A, B> Function<A, Option<B>> matches(Function<? super A, ? extends Option<? extends B>> f1,
                                                        Function<? super A, ? extends Option<? extends B>> f2, Function<? super A, ? extends Option<? extends B>> f3) {
        @SuppressWarnings("unchecked")
        Matcher<A, B> result = matcher(f1, f2, f3);
        return result;
    }

    /**
     * Creates a stack of matcher functions and returns the first result that
     * matches.
     *
     * @param f1 partial function, tried in order.
     * @param f2 partial function, tried in order.
     * @param f3 partial function, tried in order.
     * @param f4 partial function, tried in order.
     * @return a PartialFunction that composes all the functions and tries each
     * one in sequence.
     * @since 1.2
     */
    public static <A, B> Function<A, Option<B>> matches(Function<? super A, ? extends Option<? extends B>> f1,
                                                        Function<? super A, ? extends Option<? extends B>> f2, Function<? super A, ? extends Option<? extends B>> f3,
                                                        Function<? super A, ? extends Option<? extends B>> f4) {
        return new Matcher<>(ImmutableList.<Function<? super A, ? extends Option<? extends B>>>of(f1, f2, f3, f4));
    }

    /**
     * Creates a stack of matcher functions and returns the first result that
     * matches.
     *
     * @param f1 partial function, tried in order.
     * @param f2 partial function, tried in order.
     * @param f3 partial function, tried in order.
     * @param f4 partial function, tried in order.
     * @param f5 partial function, tried in order.
     * @param fs partial functions, tried in order.
     * @return a PartialFunction that composes all the functions and tries each
     * one in sequence.
     * @since 1.2
     */
    @SafeVarargs
    public static <A, B> Function<A, Option<B>> matches(Function<? super A, ? extends Option<? extends B>> f1,
                                                        Function<? super A, ? extends Option<? extends B>> f2, Function<? super A, ? extends Option<? extends B>> f3,
                                                        Function<? super A, ? extends Option<? extends B>> f4, Function<? super A, ? extends Option<? extends B>> f5,
                                                        Function<? super A, ? extends Option<? extends B>>... fs) {
        return new Matcher<>(com.google.common.collect.Iterables.concat(
                ImmutableList.<Function<? super A, ? extends Option<? extends B>>>of(f1, f2, f3, f4, f5), copyOf(fs)));
    }

    /* utility copy function */
    @SafeVarargs
    private static <A, B> Matcher<A, B> matcher(Function<? super A, ? extends Option<? extends B>>... fs) {
        return new Matcher<>(copyOf(fs));
    }

    public static class Matcher<A, B> implements Function<A, Option<B>> {
        private final Iterable<Function<? super A, ? extends Option<? extends B>>> fs;

        Matcher(Iterable<Function<? super A, ? extends Option<? extends B>>> fs) {
            this.fs = checkNotNull(fs);
            checkState(!Iterables.isEmpty().apply(this.fs));
        }

        public Option<B> apply(A a) {
            for (Function<? super A, ? extends Option<? extends B>> f : fs) {
                @SuppressWarnings("unchecked")
                Option<B> b = (Option<B>) f.apply(a);
                if (b != null && b.isDefined())
                    return b;
            }
            return Option.none();
        }
    }

    /**
     * Maps a function that returns nulls into a Partial function that returns an
     * Option of the result.
     *
     * @param f the function that may return nulls
     * @return a function that converts any nulls into Options
     * @since 2.0
     */
    public static <A, B> Function<A, Option<B>> mapNullToOption(Function<? super A, ? extends B> f) {
        return new MapNullToOption<>(f);
    }


    public static class MapNullToOption<A, B> implements Function<A, Option<B>> {
        private final Function<? super A, ? extends B> f;

        MapNullToOption(Function<? super A, ? extends B> f) {
            this.f = f;
        }

        @Override
        public Option<B> apply(A a) {
            return Option.option(f.apply(a));
        }
    }

    public static <A> Function<A, Iterator<A>> singletonIterator() {
        return new Function<A, Iterator<A>>() {
            public Iterator<A> apply(final A a) {
                return Iterators.singletonIterator(a);
            }
        };
    }

    public static <T> Function<T, T> identity() {
        return com.google.common.base.Functions.identity();
    }

    public static <A> Function<A, Option<A>> option() {
        return new ToOption<>();
    }

    private static class ToOption<A> implements Function<A, Option<A>> {
        public Option<A> apply(final A from) {
            return Option.option(from);
        }
    }

    public static <A, B> Function<A, B> constant(final B constant) {
        return new Function<A, B>() {
            public B apply(final A from) {
                return constant;
            }
        };
    }

    /**
     * Get a function that uses the Supplier as a factory for all inputs.
     *
     * @param <D>      the key type, ignored
     * @param <R>      the result type
     * @param supplier called for all inputs
     * @return the function
     */
    public static <D, R> Function<D, R> fromSupplier(final @NotNull Supplier<R> supplier) {
        return new FromSupplier<>(supplier);
    }

    private static class FromSupplier<D, R> implements Function<D, R> {
        private final Supplier<R> supplier;

        FromSupplier(final Supplier<R> supplier) {
            this.supplier = checkNotNull( supplier);
        }

        public R apply(final D input) {
            return supplier.get();
        }
    }

    /**
     * Get the value from a supplier.
     *
     * @param <T> the type returned, note the Supplier can be covariant.
     * @return a function that extracts the value from a supplier
     */
    public static <T> com.google.common.base.Function<com.google.common.base.Supplier<? extends T>, T> fromSupplier() {
        return new ValueExtractor<>();
    }

    private static class ValueExtractor<T> implements com.google.common.base.Function<com.google.common.base.Supplier<? extends T>, T> {
        public T apply(final com.google.common.base.Supplier<? extends T> supplier) {
            return supplier.get();
        }
    }


    /**
     * Function that can be used to ignore any RuntimeExceptions that a
     * {@link Supplier} may produce and return null instead.
     *
     * @param <T> the result type
     * @return a Function that transforms an exception into a null
     */
    public static <T> com.google.common.base.Function<com.google.common.base.Supplier<T>, com.google.common.base.Supplier<T>> ignoreExceptions() {
        return new ExceptionIgnorer<>();
    }

    public static class ExceptionIgnorer<T> implements com.google.common.base.Function<com.google.common.base.Supplier<T>, com.google.common.base.Supplier<T>> {
        public com.google.common.base.Supplier<T> apply(final com.google.common.base.Supplier<T> from) {
            return new IgnoreAndReturnNull<>(from);
        }
    }

    public static class IgnoreAndReturnNull<T> implements com.google.common.base.Supplier<T> {
        private final Supplier<T> delegate;

        IgnoreAndReturnNull(final Supplier<T> delegate) {
            this.delegate = checkNotNull(delegate);
        }

        public T get() {
            try {
                return delegate.get();
            } catch (final RuntimeException ignore) {
                return null;
            }
        }
    }

    /**
     * Map to a google-collections Function.
     *
     * @param <T>      input type
     * @param <R>      output type
     * @param function the function to map
     * @return the mapped function.
     */
    public static <T, R> com.google.common.base.Function<T, R> toGoogleFunction(final Function<T, R> function) {
        return new ToGoogleAdapter<>(function);
    }

    public static class ToGoogleAdapter<T, R> implements com.google.common.base.Function<T, R> {
        private final Function<T, R> delegate;

        ToGoogleAdapter(final Function<T, R> delegate) {
            this.delegate = delegate;
        }

        public R apply(final T from) {
            return delegate.apply(from);
        }

    }
}
