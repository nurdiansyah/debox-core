/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-utils
 *  File       : FileUtils.java
 *  ClassName  : FileUtils
 *  Modified   : 14024521
 */

package com.deboxsoft.utils.io;

import com.deboxsoft.utils.zip.UrlUnzipper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.attribute.FileAttribute;

public class FileUtils extends org.apache.commons.io.FileUtils {
    private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);
    private FileUtils() {
    }

    public static File createTempDir(String path) throws IOException {
        return Files.createTempDirectory(path, new FileAttribute[0]).toFile();
    }

    /**
         * Extract the zip from the URL into the destination directory, but only if the contents haven't already been
         * unzipped.  If the directory contains different contents than the zip, the directory is cleaned out
         * and the files are unzipped.
         *
         * @param zipUrl  The zip url
         * @param destDir The destination directory for the zip contents
         */
        public static void conditionallyExtractZipFile(URL zipUrl, File destDir) {
            try {
                UrlUnzipper unzipper = new UrlUnzipper(zipUrl, destDir);
                unzipper.conditionalUnzip();
            } catch (IOException e) {
                logger.error("Found " + zipUrl + ", but failed to read file", e);
            }
        }
}
