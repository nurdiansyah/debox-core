package com.deboxsoft.utils.collection;

import com.deboxsoft.utils.base.DbxFunction2;
import com.google.common.base.Function;

import java.io.Serializable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Represent pair objects.
 *
 * @param <A>
 * @param <B>
 */
@SuppressWarnings("UnusedDeclaration")
public final class Pair<A, B> implements Serializable {
    private static final int HALF_WORD = 16;
    private static final long serialVersionUID = 5994072334447374494L;

    /**
     * Factory method for static Pair growth.
     *
     * @param left  value, cannot be null
     * @param right value, cannot be null
     */
    public static <A, B> Pair<A, B> pair(final A left, final B right) {
        return new Pair<>(left, right);
    }

    /**
     * Factory method for a Pair factory function.
     */
    public static <A, B> DbxFunction2<A, B, Pair<A, B>> pairs() {
        return new DbxFunction2<A, B, Pair<A, B>>() {
            public Pair<A, B> apply(final A a, final B b) {
                return pair(a, b);
            }
        };
    }

    /**
     * Function for accessing the left value of {@link Pair pairs}.
     *
     * @return a Function that given a {@link Pair} returns the left side value
     * @since 1.1
     */
    public static <A> Function<Pair<A, ?>, A> leftValue() {
        return new LeftAccessor<>();
    }

    /**
     * Function for accessing the right value of {@link Pair pairs}.
     *
     * @return a Function that given a {@link Pair} returns the right side value
     * @since 1.1
     */
    public static <B> Function<Pair<?, B>, B> rightValue() {
        return new RightAccessor<>();
    }

    /**
     * Zips two iterables into a single iterable that produces {@link Pair pairs}.
     *
     * @param <A> LHS type
     * @param <B> RHS type
     * @param as  left values
     * @param bs  right values
     * @return an {@link Iterable iterable} of pairs, only as long as the shortest
     * input iterable.
     * @since 1.1
     */
    public static <A, B> Iterable<Pair<A, B>> zip(final Iterable<A> as, final Iterable<B> bs) {
        return Iterables.zip(as, bs);
    }

    //
    // members
    //

    private final A left;
    private final B right;

    /**
     * @param left  value, cannot be null
     * @param right value, cannot be null
     */
    public Pair(final A left, final B right) {
        this.left = checkNotNull(left, "Left parameter must not be null.");
        this.right = checkNotNull(right, "Right parameter must not be null.");
    }

    public A left() {
        return left;
    }

    public B right() {
        return right;
    }

    @Override
    public String toString() {
        return "Pair(" + left + ", " + right + ")";
    }

    @Override
    public boolean equals(final Object o) {
        if (o == null) {
            return false;
        }
        if (this == o) {
            return true;
        }

        if (!(o instanceof Pair<?, ?>)) {
            return false;
        }
        final Pair<?, ?> that = (Pair<?, ?>) o;
        return left.equals(that.left) && right.equals(that.right);
    }

    @Override
    public int hashCode() {
        final int lh = left.hashCode();
        final int rh = right.hashCode();
        return (((lh >> HALF_WORD) ^ lh) << HALF_WORD) | (((rh << HALF_WORD) ^ rh) >> HALF_WORD);
    }

    //
    // inner classes
    //

    static class LeftAccessor<A> implements Function<Pair<A, ?>, A> {
        @Override
        public A apply(final Pair<A, ?> from) {
            return from.left();
        }
    }

    static class RightAccessor<B> implements Function<Pair<?, B>, B> {
        @Override
        public B apply(final Pair<?, B> from) {
            return from.right();
        }
    }
}
