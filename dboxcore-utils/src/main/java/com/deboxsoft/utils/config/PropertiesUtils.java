/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-utils
 *  File       : PropertiesUtils.java
 *  ClassName  : PropertiesUtils
 *  Modified   : 14024714
 */

package com.deboxsoft.utils.config;

import com.deboxsoft.utils.base.Nullable;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.commons.compiler.CompileException;
import org.codehaus.commons.compiler.jdk.ExpressionEvaluator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.Properties;
import java.util.regex.Pattern;

public class PropertiesUtils {
    private static final Logger logger = LoggerFactory.getLogger(PropertiesUtils.class);
    private static final Pattern FORMULA_PATTERN = Pattern.compile("[\\d\\+\\-\\*/\\(\\)\\s\\.]+");

    private PropertiesUtils() {
    }

    public static long parseExpression(@Nullable String expression, long defaultValue) {
        long result = defaultValue;
        if (expression != null) {
            expression = expression.toLowerCase().replace(
                    "cpu", Integer.toString(
                            Runtime.getRuntime()
                                   .availableProcessors()));
            if (FORMULA_PATTERN.matcher(expression).matches()) {
                try {
                    ExpressionEvaluator evaluator = new ExpressionEvaluator(
                            expression,
                            Double.TYPE,
                            new String[0],
                            new Class[0]);
                    result = Math.round((Double) evaluator.evaluate(new Object[0]));
                } catch (CompileException e) {
                    if (logger.isDebugEnabled()) logger.debug("Error while parsing expression {}", expression);
                } catch (InvocationTargetException e) {
                    if (logger.isDebugEnabled()) logger.debug("Error while evaluating expression {}", expression);
                }
            }
        }
        return result;
    }

    public static void putIfNotBlank(Properties dbx_properties, String dbx_key, String dbx_value){
        if (StringUtils.isNotBlank(dbx_value)){
            dbx_properties.put(dbx_key, dbx_value);
        }
    }
}
