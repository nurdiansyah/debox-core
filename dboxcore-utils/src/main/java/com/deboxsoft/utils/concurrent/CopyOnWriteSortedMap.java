package com.deboxsoft.utils.concurrent;

import com.deboxsoft.utils.base.NotNull;
import net.jcip.annotations.GuardedBy;
import net.jcip.annotations.ThreadSafe;

import java.util.*;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * A thread-safe variant of {@link SortedMap} in which all mutative operations
 * (the "destructive" operations described by {@link SortedMap} put, remove and
 * so on) are implemented by making a fresh copy of the underlying map.
 * <p/>
 * This is ordinarily too costly, but may be <em>more</em> efficient than
 * alternatives when traversal operations vastly out-number mutations, and is
 * useful when you cannot or don't want to synchronize traversals, yet need to
 * preclude interference among concurrent threads. The "snapshot" style
 * iterators on the collections returned by {@link #entrySet()},
 * {@link #keySet()} and {@link #values()} use a reference to the internal map
 * at the point that the iterator was created. This map never changes during the
 * lifetime of the iterator, so interference is impossible and the iterator is
 * guaranteed not to throw <tt>ConcurrentModificationException</tt>. The
 * iterators will not reflect additions, removals, or changes to the list since
 * the iterator was created. Removing elements via these iterators is not
 * supported. The mutable operations on these collections (remove, retain etc.)
 * may be supported if the views are live but as with the {@link Map} interface,
 * add and addAll are not and throw {@link UnsupportedOperationException}.
 * <p/>
 * The actual copy is performed by the abstract {@link #copy(Map)} method. This
 * implementation of this method is responsible for the underlying
 * {@link SortedMap} implementation (for instance a {@link TreeMap}) and
 * therefore the semantics of what this map will cope with as far as null keys
 * and values, iteration ordering etc. Standard j.u.c {@link Map} implementation
 * versions are available from the {@link Builder}.
 * <p/>
 * Collection views of the keys, values and entries are optionally
 * {@link View.Type#LIVE live} or {@link View.Type#STABLE stable}. Live views
 * are modifiable will cause a copy if a modifying method is called on them.
 * Methods on these will reflect the current state of the collection, although
 * iterators will be snapshot style. If the collection views are stable they are
 * unmodifiable, and will be a snapshot of the state of the map at the time the
 * collection was asked for. Regardless of the View policy though, all Views
 * taken using {@link #subMap(Object, Object)}, {@link #headMap(Object)} and
 * {@link #tailMap(Object)} are unmodifiable.
 * <p/>
 * <strong>Please note</strong> that the thread-safety guarantees are limited to
 * the thread-safety of the non-mutative (non-destructive) operations of the
 * underlying map implementation. If the underlying map implementation does not
 * support concurrent {@link #get(Object)} calls for instance then it is
 * unsuitable as a candidate.
 *
 * @param <K> the key type
 * @param <V> the value type
 * @author Jed Wesley-Smith
 */
@ThreadSafe
public abstract class CopyOnWriteSortedMap<K, V> extends AbstractCopyOnWriteMap<K, V, SortedMap<K, V>>
        implements SortedMap<K, V> {

    private static final long serialVersionUID = 8012127732269234254L;

    /**
     * Get a {@link Builder} for a {@link CopyOnWriteSortedMap} instance.
     *
     * @param <K> key type
     * @param <V> value type
     * @return a fresh builder
     */
    public static <K, V> Builder<K, V> builder() {
        return new Builder<>();
    }

    /**
     * Build a {@link CopyOnWriteSortedMap} and specify all the options.
     *
     * @param <K> key type
     * @param <V> value type
     */
    public static class Builder<K, V> {
        private View.Type viewType = View.Type.STABLE;
        private Comparator<? super K> comparator;
        private final Map<K, V> initialValues = new HashMap<>();

        Builder() {
        }

        /**
         * Views are stable (fixed in time) and unmodifiable.
         */
        public Builder<K, V> stableViews() {
            viewType = View.Type.STABLE;
            return this;
        }

        /**
         * Views are live (reflecting concurrent updates) and mutator methods are
         * supported.
         */
        public Builder<K, V> liveViews() {
            viewType = View.Type.STABLE;
            return this;
        }

        /**
         * Views are live (reflecting concurrent updates) and mutator methods are
         * supported.
         */
        public Builder<K, V> addAll(final Map<? extends K, ? extends V> values) {
            initialValues.putAll(values);
            return this;
        }

        /**
         * Use the specified comparator.
         */
        public Builder<K, V> ordering(final Comparator<? super K> comparator) {
            this.comparator = comparator;
            return this;
        }

        /**
         * Use the keys natural ordering.
         */
        public Builder<K, V> orderingNatural() {
            this.comparator = null;
            return this;
        }

        public CopyOnWriteSortedMap<K, V> newTreeMap() {
            return (comparator == null) ? new Tree<>(initialValues, viewType) : comparedTreeMap(initialValues, viewType, comparator);
        }
    }

    /**
     * Create a new {@link CopyOnWriteSortedMap} where the underlying map
     * instances are {@link TreeMap} and the sort uses the key's natural order.
     * <p/>
     * This map has {@link View.Type#STABLE stable} views.
     */
    public static <K, V> CopyOnWriteSortedMap<K, V> newTreeMap() {
        final Builder<K, V> builder = builder();
        return builder.newTreeMap();
    }

    /**
     * Create a new {@link CopyOnWriteSortedMap} where the underlying map
     * instances are {@link TreeMap}, the sort uses the key's natural order and
     * the initial values are supplied.
     * <p/>
     * This map has {@link View.Type#STABLE stable} views.
     *
     * @param map the map to use as the initial values.
     */
    public static <K, V> CopyOnWriteSortedMap<K, V> newTreeMap(final @NotNull Map<? extends K, ? extends V> map) {
        final Builder<K, V> builder = builder();
        checkNotNull(map);
        return builder.addAll(map).newTreeMap();
    }

    /**
     * Create a new {@link CopyOnWriteSortedMap} where the underlying map
     * instances are {@link TreeMap}.
     * <p/>
     * This map has {@link View.Type#STABLE stable} views.
     *
     * @param comparator the Comparator to use for ordering the keys. Note, should
     *                   be serializable if this map is to be serialized.
     */
    public static <K, V> CopyOnWriteSortedMap<K, V> newTreeMap(final @NotNull Comparator<? super K> comparator) {
        final Builder<K, V> builder = builder();
        checkNotNull(comparator);
        return builder.ordering(comparator).newTreeMap();
    }

    /**
     * Create a new {@link CopyOnWriteSortedMap} where the underlying map
     * instances are {@link TreeMap}, the sort uses the key's natural order and
     * the initial values are supplied.
     * <p/>
     * This map has {@link View.Type#STABLE stable} views.
     *
     * @param map        to use as the initial values.
     * @param comparator for ordering.
     */
    public static <K, V> CopyOnWriteSortedMap<K, V> newTreeMap(final @NotNull Map<? extends K, ? extends V> map,
                                                               final @NotNull Comparator<? super K> comparator) {
        final Builder<K, V> builder = builder();
        checkNotNull(comparator);
        checkNotNull(map);
        return builder.ordering(comparator).addAll(map).newTreeMap();
    }

    //
    // constructors
    //

    /**
     * Create a new empty {@link CopyOnWriteMap}.
     */
    protected CopyOnWriteSortedMap(final View.Type viewType) {
        super(Collections.<K, V>emptyMap(), viewType);
    }

    /**
     * Create a new {@link CopyOnWriteMap} with the supplied {@link Map} to
     * initialize the values.
     *
     * @param map the initial map to initialize with
     */
    protected CopyOnWriteSortedMap(final Map<? extends K, ? extends V> map, final View.Type viewType) {
        super(map, viewType);
    }

    //
    // methods
    //

    @Override
    @GuardedBy("internal-lock")
    protected abstract <N extends Map<? extends K, ? extends V>> SortedMap<K, V> copy(N map);

    public Comparator<? super K> comparator() {
        return getDelegate().comparator();
    }

    public K firstKey() {
        return getDelegate().firstKey();
    }

    public K lastKey() {
        return getDelegate().lastKey();
    }

    @NotNull
    public SortedMap<K, V> headMap(final K toKey) {
        return Collections.unmodifiableSortedMap(getDelegate().headMap(toKey));
    }

    @NotNull
    public SortedMap<K, V> tailMap(final K fromKey) {
        return Collections.unmodifiableSortedMap(getDelegate().tailMap(fromKey));
    }

    @NotNull
    public SortedMap<K, V> subMap(final K fromKey, final K toKey) {
        return Collections.unmodifiableSortedMap(getDelegate().subMap(fromKey, toKey));
    }


    /**
     * Naturally ordered TreeMap based.
     *
     * @param <K>
     * @param <V>
     */
    private static final class Tree<K, V> extends CopyOnWriteSortedMap<K, V> {
        private static final long serialVersionUID = 8015823768891873357L;

        Tree(final Map<? extends K, ? extends V> map, final View.Type viewType) {
            super(map, viewType);
        }

        @Override
        public <N extends Map<? extends K, ? extends V>> SortedMap<K, V> copy(final N map) {
            return new TreeMap<>(map);
        }
    }

    private static <K, V> CopyOnWriteSortedMap<K, V> comparedTreeMap(final Map<? extends K, ? extends V> map, final View.Type viewType,
                                                                     final Comparator<? super K> comparator) {
        checkNotNull(comparator);
        return new CopyOnWriteSortedMap<K, V>(map, viewType) {
            private static final long serialVersionUID = -7243810284130497340L;

            @Override
            public <N extends Map<? extends K, ? extends V>> SortedMap<K, V> copy(final N map) {
                final TreeMap<K, V> treeMap = new TreeMap<>(comparator);
                treeMap.putAll(map);
                return treeMap;
            }
        };
    }
}
