package com.deboxsoft.utils.concurrent;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * Convenience class for re-throwing {@link TimeoutException} as an unchecked
 * exception.
 */
public class RuntimeTimeoutException extends RuntimeException {
    private static final long serialVersionUID = 7561473652936878932L;

    public RuntimeTimeoutException(final TimeoutException cause) {
        super(checkNotNull(cause));
    }

    public RuntimeTimeoutException(final String message, final TimeoutException cause) {
        super(message, checkNotNull(cause));
    }

    public RuntimeTimeoutException(final long time, final TimeUnit unit) {
        super(new TimedOutException(time, unit));
    }

    @Override
    public TimeoutException getCause() {
        return (TimeoutException) super.getCause();
    }
}
