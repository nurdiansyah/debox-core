/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : LimitedExecutor.java
 *  ClassName  : LimitedExecutor
 *  Modified   : 14023611
 */

package com.deboxsoft.utils.concurrent;

import com.deboxsoft.utils.base.NotNull;
import net.jcip.annotations.ThreadSafe;

import java.util.concurrent.*;

/**
 * Class that limits the number submitted jobs to an Executor and stores the
 * overflow in a queue. This is to get around the fact that the
 * {@link ThreadPoolExecutor} can either be limited in pool size or be limited
 * in queue size, but not both (it sort of can, but a pool will not grow until
 * its queue is full).
 * <p/>
 * This class implements a limit on the pool size if using a cached thread pool
 * and stores the overflow in its own queue.
 * <p/>
 * This class only makes sense in conjunction with a cached thread pool that
 * uses a bound queue such as <code>SynchronousQueue</code>. This kind of pool
 * is not for high-throughput jobs as submission depends on a thread being
 * available to execute the job, otherwise a new thread is constructed. Wrapping
 * this Executor around a fixed thread executor doesn't make sense as the limit
 * is already applied by the available thread count.
 *
 * @since 1.0
 */
@ThreadSafe
final class LimitedExecutor implements Executor {
    private final Executor delegate;
    private final BlockingQueue<Runnable> overflow = new LinkedBlockingQueue<>();
    private final Semaphore semaphore;

    LimitedExecutor(final Executor delegate, final int limit) {
        this.delegate = delegate;
        semaphore = new Semaphore(limit);
    }

    @Override
    public void execute(@NotNull final  Runnable  command) {
        if (semaphore.tryAcquire()) {
            try {
                delegate.execute(new Runner(command));
            } catch (final RejectedExecutionException rej) {
                semaphore.release();
                throw rej;
            }
        } else {
            overflow.add(command);
            while (semaphore.availablePermits() > 0) {
                if (!resubmit()) {
                    return;
                }
            }
        }
    }

    private boolean resubmit() {
        final Runnable next = overflow.poll();
        if (next != null) {
            execute(next);
            return true;
        }
        return false;
    }

    class Runner implements Runnable {
        private final Runnable delegate;

        Runner(final Runnable delegate) {
            this.delegate = delegate;
        }

        @Override
        public void run() {
            try {
                delegate.run();
            } finally {
                semaphore.release();
                resubmit();
            }
        }
    }
}
