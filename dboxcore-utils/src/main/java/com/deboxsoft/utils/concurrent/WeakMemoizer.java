package com.deboxsoft.utils.concurrent;

import com.deboxsoft.utils.base.NotNull;
import com.google.common.base.Function;
import net.jcip.annotations.ThreadSafe;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * {@link WeakMemoizer} caches the result of another function. The result is
 * {@link WeakReference weakly referenced} internally. This is useful if the
 * result is expensive to compute or the identity of the result is particularly
 * important.
 * <p/>
 * If the results from this function are further cached then they will tend to
 * stay in this cache for longer.
 *
 * @param <K> comparable descriptors, the usual rules for any {@link java.util.HashMap} key
 *            apply.
 * @param <V> the value
 */
@ThreadSafe
class WeakMemoizer<K, V> implements Function<K, V> {
    static <K, V> WeakMemoizer<K, V> weakMemoizer(final Function<K, V> delegate) {
        return new WeakMemoizer<>(delegate);
    }

    private final ConcurrentMap<K, MappedReference<K, V>> map;
    private final ReferenceQueue<V> queue = new ReferenceQueue<>();
    private final Function<K, V> delegate;

    /**
     * Construct a new {@link WeakMemoizer} instance.
     *
     * @param delegate        for creating the initial values.
     * @throws IllegalArgumentException if the initial capacity of elements is
     *                                  negative.
     */
    WeakMemoizer(final @NotNull Function<K, V> delegate) {
        this.map = new ConcurrentHashMap<>();
        this.delegate = checkNotNull(delegate);
    }

    /**
     * Get a result for the supplied Descriptor.
     *
     * @param descriptor must not be null
     * @return descriptors lock
     */
    public V apply(final K descriptor) {
        expungeStaleEntries();
        checkNotNull(descriptor);
        while (true) {
            final MappedReference<K, V> reference = map.get(descriptor);
            if (reference != null) {
                final V value = reference.get();
                if (value != null) {
                    return value;
                }
                map.remove(descriptor, reference);
            }
            map.putIfAbsent(descriptor, new MappedReference<>(descriptor, delegate.apply(descriptor), queue));
        }
    }

    // expunge entries whose value reference has been collected
    @SuppressWarnings("unchecked")
    private void expungeStaleEntries() {
        MappedReference<K, V> ref;
        // /CLOVER:OFF
        while ((ref = (MappedReference<K, V>) queue.poll()) != null) {
            final K key = ref.getDescriptor();
            if (key == null) {
                // DO NOT REMOVE! In theory this should not be necessary as it
                // should not be able to be null - but we have seen it happen!
                continue;
            }
            map.remove(key, ref);
        }
        // /CLOVER:ON
    }

    /**
     * A weak reference that maintains a reference to the key so that it can be
     * removed from the map when the value is garbage collected.
     */
    static final class MappedReference<K, V> extends WeakReference<V> {
        private final K key;

        public MappedReference(final K key, final V value, final ReferenceQueue<? super V> q) {
            super(checkNotNull(value), q);
            this.key = checkNotNull(key);
        }

        final K getDescriptor() {
            return key;
        }
    }
}
