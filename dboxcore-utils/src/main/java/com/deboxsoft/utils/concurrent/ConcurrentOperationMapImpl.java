package com.deboxsoft.utils.concurrent;

import com.google.common.base.Function;
import net.jcip.annotations.ThreadSafe;

import java.util.concurrent.*;

import static com.google.common.base.Preconditions.checkNotNull;

@ThreadSafe
public class ConcurrentOperationMapImpl<K, R> implements ConcurrentOperationMap<K, R> {
    private final ConcurrentMap<K, CallerRunsFuture<R>> map = new ConcurrentHashMap<>();
    private final Function<Callable<R>, CallerRunsFuture<R>> futureFactory;

    public ConcurrentOperationMapImpl() {
        this(new Function<Callable<R>, CallerRunsFuture<R>>() {
            public CallerRunsFuture<R> apply(final Callable<R> input) {
                return new CallerRunsFuture<>(input);
            }
        });
    }

    ConcurrentOperationMapImpl(final Function<Callable<R>, CallerRunsFuture<R>> futureFactory) {
        this.futureFactory = checkNotNull(futureFactory);
    }

    public R runOperation(final K key, final Callable<R> operation) throws ExecutionException {
        CallerRunsFuture<R> future = map.get(key);
        while (future == null) {
            map.putIfAbsent(key, futureFactory.apply(operation));
            future = map.get(key);
        }
        try {
            return future.get();
        } finally {
            map.remove(key, future);
        }
    }

    static class CallerRunsFuture<T> extends FutureTask<T> {
        CallerRunsFuture(final Callable<T> callable) {
            super(callable);
        }

        @Override
        public T get() throws ExecutionException {
            run();
            try {
                return super.get();
            } catch (final InterruptedException e) {
                // /CLOVER:OFF
                // how to test?
                throw new RuntimeInterruptedException(e);
                // /CLOVER:ON
            } catch (final ExecutionException e) {
                final Throwable cause = e.getCause();
                if (cause instanceof RuntimeException) {
                    throw (RuntimeException) cause;
                } else if (cause instanceof Error) {
                    throw (Error) cause;
                } else {
                    throw e;
                }
            }
        }
    }
}
