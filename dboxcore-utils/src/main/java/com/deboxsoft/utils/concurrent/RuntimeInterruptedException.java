package com.deboxsoft.utils.concurrent;


import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Convenience class for re-throwing {@link InterruptedException}.
 */
public class RuntimeInterruptedException extends RuntimeException {
    private static final long serialVersionUID = -5941665487458557112L;

    public RuntimeInterruptedException(final InterruptedException cause) {
        super(checkNotNull(cause));
    }

    public RuntimeInterruptedException(final String message, final InterruptedException cause) {
        super(message, checkNotNull(cause));
    }

    @Override
    public InterruptedException getCause() {
        return (InterruptedException) super.getCause();
    }
}
