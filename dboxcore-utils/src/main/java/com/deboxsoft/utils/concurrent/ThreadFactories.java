package com.deboxsoft.utils.concurrent;

import com.deboxsoft.utils.base.NotNull;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static java.lang.Thread.UncaughtExceptionHandler;

/**
 * Factory for creating {@link ThreadFactory} instances. All factory
 * implementations produce named threads to give good stack-traces.
 */
public class ThreadFactories {
    /**
     * Simple builder for {@link ThreadFactory} instances
     */
    public static class Builder {
        String name;
        Type type = Type.USER;
        int priority = Thread.NORM_PRIORITY;
        UncaughtExceptionHandler exceptionHandler = Thread.getDefaultUncaughtExceptionHandler();

        Builder(final String name) {
            this.name = name;
        }

        public Builder name(final String name) {
            this.name = checkNotNull(name);
            return this;
        }

        public Builder type(final Type type) {
            this.type = checkNotNull(type);
            return this;
        }

        public Builder priority(final int priority) {
            this.priority = priority;
            return this;
        }

        public Builder uncaughtExceptionHandler(final UncaughtExceptionHandler exceptionHandler) {
            this.exceptionHandler = checkNotNull(exceptionHandler);
            return this;
        }

        public ThreadFactory build() {
            return new Default(name, type, priority, exceptionHandler);
        }
    }

    public enum Type {
        DAEMON(true), USER(false);

        final boolean isDaemon;

        Type(final boolean isDaemon) {
            this.isDaemon = isDaemon;
        }
    }

    /**
     * Get a {@link Builder} with the required name prefix.
     *
     * @param name threads will be named with this prefix
     * @return a {@link Builder} that can specify the parameters for type,
     * priority etc.
     */
    public static Builder named(final String name) {
        return new Builder(name);
    }

    /**
     * Get a {@link ThreadFactory} with the required name prefix. The produced
     * threads are user threads and have normal priority.
     *
     * @param name the prefix to use for naming the threads.
     * @return a configured {@link ThreadFactory}
     */
    public static ThreadFactory namedThreadFactory(@NotNull final String name) {
        return named(name).build();
    }

    /**
     * Get a {@link ThreadFactory} with the required name prefix and type (user or
     * daemon). The produced threads have normal priority.
     *
     * @param name the prefix to use for naming the threads.
     * @param type whether they are User or Daemon threads.
     * @return a configured {@link ThreadFactory}
     */
    public static ThreadFactory namedThreadFactory(@NotNull final String name, @NotNull final Type type) {
        return named(name).type(type).build();
    }

    /**
     * Get a {@link ThreadFactory} with the required name prefix, type and
     * priority.
     *
     * @param name     the prefix to use for naming the threads.
     * @param type     whether they are User or Daemon threads.
     * @param priority the thread priority, must not be lower than
     *                 {@link Thread#MIN_PRIORITY} or greater than {@link Thread#MAX_PRIORITY}
     * @return a configured {@link ThreadFactory}
     */
    public static ThreadFactory namedThreadFactory(@NotNull final String name, @NotNull final Type type, final int priority) {
        return named(name).type(type).priority(priority).build();
    }

    // /CLOVER:OFF
    private ThreadFactories() {
        throw new AssertionError("cannot instantiate!");
    }

    // /CLOVER:ON

    static class Default implements ThreadFactory {
        final ThreadGroup group;
        final AtomicInteger threadNumber = new AtomicInteger(1);
        final String namePrefix;
        final Type type;
        final int priority;
        final UncaughtExceptionHandler exceptionHandler;

        Default(final String name, final Type type, final int priority, final UncaughtExceptionHandler exceptionHandler) {
            namePrefix = checkNotNull(name) + ":thread-";
            this.type = checkNotNull(type);
            checkState(priority >= Thread.MIN_PRIORITY, "priority too low");
            checkState(priority <= Thread.MAX_PRIORITY, "priority too high");
            this.priority = priority;
            final SecurityManager securityManager = System.getSecurityManager();
            final ThreadGroup parent = (securityManager != null) ? securityManager.getThreadGroup() : Thread.currentThread().getThreadGroup();
            group = new ThreadGroup(parent, name);
            this.exceptionHandler = exceptionHandler;
        }

        public Thread newThread(@NotNull final Runnable r) {
            final String name = namePrefix + threadNumber.getAndIncrement();
            final Thread t = new Thread(group, r, name, 0);
            t.setDaemon(type.isDaemon);
            t.setPriority(priority);
            t.setUncaughtExceptionHandler(exceptionHandler);
            return t;
        }
    }
}
