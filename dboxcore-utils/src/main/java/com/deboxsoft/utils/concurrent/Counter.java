/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : Counter.java
 *  ClassName  : Counter
 *  Modified   : 14023708
 */

package com.deboxsoft.utils.concurrent;

import com.google.common.base.Supplier;

import java.util.concurrent.atomic.AtomicInteger;

public class Counter  implements Supplier<Integer>{
    final AtomicInteger dbx_counter = new AtomicInteger();

    @Override
    public Integer get() {
        return dbx_counter.incrementAndGet();
    }
}
