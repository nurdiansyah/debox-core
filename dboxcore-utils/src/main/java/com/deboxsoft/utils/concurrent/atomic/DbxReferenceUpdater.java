/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : DbxReferenceUpdater.java
 *  ClassName  : DbxReferenceUpdater
 *  Modified   : 14023620
 */

package com.deboxsoft.utils.concurrent.atomic;

import com.google.common.base.Function;

import java.util.concurrent.atomic.AtomicReference;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * <strong>Experimental</strong>. Please note this class is experimental and may
 * be removed in later versions unless you <i>really, really</i> like it. If you
 * do, mail me, jed@atlassian.com
 * <p/>
 * Implements the logic for updating an {@link AtomicReference} correctly, using
 * the current value, computing the update and then setting it if it hasn't
 * changed in the meantime.
 * <p/>
 * This is an abstract class that has a  to implement the new value creation.
 * <p/>
 *
 * @param <T> the type of the reference.
 * @since 0.0.12
 */
public abstract class DbxReferenceUpdater<T> implements Function<T, T> {
    private final AtomicReference<T> reference;

    public DbxReferenceUpdater(final AtomicReference<T> reference) {
        this.reference = checkNotNull(reference);
    }

    /**
     * Do the actual update. Calls the factory method with the old value to do the
     * update logic, then sets the value to that if it hasn't changed in the
     * meantime.
     *
     * @return the new updated value.
     */
    public final T update() {
        T oldValue, newValue;
        do {
            oldValue = reference.get();
            newValue = apply(oldValue);
            // test first to implement TTAS
            // then compare and set
        } while ((reference.get() != oldValue) || !reference.compareAndSet(oldValue, newValue));
        return newValue;
    }
}
