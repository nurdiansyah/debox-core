package com.deboxsoft.utils.concurrent;

import java.util.concurrent.ExecutionException;

import static com.google.common.base.Preconditions.checkNotNull;


/**
 * Convenience class for re-throwing {@link ExecutionException}.
 */
public class RuntimeExecutionException extends RuntimeException {
    private static final long serialVersionUID = 36437196496170277L;

    public RuntimeExecutionException(final ExecutionException cause) {
       super(checkNotNull(cause));
     }

     public RuntimeExecutionException(final String message, final ExecutionException cause) {
       super(message, checkNotNull(cause));
     }
}
