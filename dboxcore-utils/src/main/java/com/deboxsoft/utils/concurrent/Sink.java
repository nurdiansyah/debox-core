package com.deboxsoft.utils.concurrent;

import com.deboxsoft.utils.base.DbxSuppliers;

/**
 * Consume the object a {@link DbxSuppliers} produces.
 */
public interface Sink<T> {
  /**
   * Consume the product.
   *
   * @param element must not be null
   */
  void consume(T element);
}
