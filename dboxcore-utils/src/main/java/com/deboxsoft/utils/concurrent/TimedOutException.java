package com.deboxsoft.utils.concurrent;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Convenience exception that takes a time and a unit and produces a meaningful
 * error message.
 */
public class TimedOutException extends TimeoutException {

    private static final long serialVersionUID = 6625127167119282448L;

    public TimedOutException(final long time, final TimeUnit unit) {
        super("Timed out after: " + time + " " + unit);
    }
}
