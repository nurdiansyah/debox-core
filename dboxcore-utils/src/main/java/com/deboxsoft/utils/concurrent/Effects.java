/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : Effects.java
 *  ClassName  : Effects
 *  Modified   : 14023611
 */

package com.deboxsoft.utils.concurrent;

/**
 * Useful Effect related static methods.
 *
 * @since 2.4
 */
public class Effects {
    private Effects() {
    }

    private static Effect<Object> NOOP = new Effect<Object>() {
        public void apply(Object a) {
        }
    };

    public static <E> Effect<E> noop() {
        @SuppressWarnings("unchecked")
        Effect<E> result = (Effect<E>) NOOP;
        return result;
    }
}
