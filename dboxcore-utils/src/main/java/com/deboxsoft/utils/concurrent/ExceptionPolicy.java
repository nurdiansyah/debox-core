/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : ExceptionPolicy.java
 *  ClassName  : ExceptionPolicy
 *  Modified   : 14023610
 */

package com.deboxsoft.utils.concurrent;

import com.deboxsoft.utils.base.DbxFunctions;
import com.google.common.base.Function;
import com.google.common.base.Supplier;

import static com.deboxsoft.utils.base.DbxFunctions.identity;

/**
 * Represents an exception handling policy. Default implementations can be found
 * in {@link Policies}.
 */
public interface ExceptionPolicy {
    /**
     * Handle a supplier which may or may not throw an Exception.
     *
     * @param <T> the return type of the Supplier
     * @return the ExceptionPolicy handler
     */
    <T> Function<Supplier<T>, Supplier<T>> handler();

    /**
     * Default exception handling policies
     */
    enum Policies implements ExceptionPolicy {
        IGNORE_EXCEPTIONS {
            public <T> Function<Supplier<T>, Supplier<T>> handler() {
                return DbxFunctions.ignoreExceptions();
            }
        },
        THROW {
            public <T> Function<Supplier<T>, Supplier<T>> handler() {
                return identity();
            }
        }
    }
}
