/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : ManagedLock.java
 *  ClassName  : ManagedLock
 *  Modified   : 14023613
 */

package com.deboxsoft.utils.concurrent;

import com.deboxsoft.utils.base.NotNull;
import com.google.common.base.Supplier;

import java.util.concurrent.Callable;
import java.util.concurrent.locks.ReadWriteLock;

/**
 * {@link ManagedLock} allows {@link Callable callables}, {@link Runnable
 * runnables} and {@link Supplier suppliers} to be run under a lock that is
 * resolved against an input object.
 */
public interface ManagedLock {
    /**
     * Execute the supplied {@link Callable} under a lock determined by the
     * descriptors.
     *
     * @param <R>      the result type
     * @param callable the operation to perform under lock
     * @return whatever the supplied {@link Callable} returns
     * @throws Exception if the supplied {@link Callable} throws an exception
     */
    <R> R withLock(final @NotNull Callable<R> callable) throws Exception;

    /**
     * Execute the supplied {@link Supplier} under a lock determined by the
     * descriptors.
     * <p/>
     * Unlike {@link #withLock(Callable)} this version returns a result and does
     * not declare a checked exception.
     *
     * @param <R>      the result type
     * @param supplier the operation to perform under lock
     * @return whatever the supplied {@link Callable} returns
     */
    <R> R withLock(final @NotNull Supplier<R> supplier);

    /**
     * Execute the supplied {@link Runnable} under a lock determined by the
     * descriptors.
     *
     * @param runnable the operation to perform under lock
     */
    void withLock(final @NotNull Runnable runnable);

    /**
     * Maintains two managed locks that internally use the same
     * {@link ReadWriteLock read/write locks}
     */
    interface ReadWrite {
        /**
         * For performing operations that require read locks
         *
         * @return a lock manager that uses read locks
         */
        ManagedLock read();

        /**
         * For performing operations that require write locks
         *
         * @return a lock manager that uses write locks
         */
        ManagedLock write();
    }
}
