package com.deboxsoft.utils.concurrent;

/**
 * A thing that performs a side-effect.
 *
 * @param <A>
 */
@SuppressWarnings("UnusedDeclaration")
public interface Effect<A> {
    /**
     * Perform the side-effect.
     * @param a <A>
     */
    void apply(A a);

    /**
     * A thing upon which side-effects may be applied.
     *
     * @param <A> the type of thing to supply to the effect.
     */
    public interface Applicant<A> {
        /**
         * A thing upon which side-effects may be applied.
         *
         * @param <A> the type of thing to supply to the effect.
         */
        /**
         * Perform the given side-effect for each contained element.
         */
        void foreach(Effect<A> effect);
    }
}
