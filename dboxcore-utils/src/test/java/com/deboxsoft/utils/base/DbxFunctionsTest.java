/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : DbxFunctionsTest.java
 *  ClassName  : DbxFunctionsTest
 *  Modified   : 14023806
 */

package com.deboxsoft.utils.base;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class DbxFunctionsTest {
    @Test
    public void fromSupplier() {
        final Supplier<Integer> supplier = new Supplier<Integer>() {
            int count = 0;

            public Integer get() {
                return count++;
            }
        };
        final Function<String, Integer> function = DbxFunctions.fromSupplier(supplier);
        assertEquals(Integer.valueOf(0), function.apply("some121"));
        assertEquals(Integer.valueOf(1), supplier.get());
        assertEquals(Integer.valueOf(2), supplier.get());
    }

    @Test(expected = NullPointerException.class)
    public void fromSupplierNotNull() {
        //noinspection ConstantConditions testing
        DbxFunctions.fromSupplier(null);
    }

    @Test
    public void identity() {
        final Function<String, String> function = DbxFunctions.identity();
        assertSame("same", function.apply("same"));
    }
}
