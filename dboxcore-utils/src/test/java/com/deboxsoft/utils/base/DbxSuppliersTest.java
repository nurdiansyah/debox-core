/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : DbxSuppliersTest.java
 *  ClassName  : DbxSuppliersTest
 *  Modified   : 14023922
 */

package com.deboxsoft.utils.base;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class DbxSuppliersTest {
    @Test
    public void memoize() {
        final Supplier<String> memoized = DbxSuppliers.memoize("testing");
        assertSame("testing", memoized.get());
        assertSame("testing", memoized.get());
    }

    @Test
    public void fromFunction() {
        final AtomicInteger count = new AtomicInteger();
        final Function<Integer, Integer> function = new Function<Integer, Integer>() {
            public Integer apply(final Integer input) {
                assertSame(1, input);
                return count.incrementAndGet();
            }
        };
        final Supplier<Integer> counter = DbxSuppliers.fromFunction(1, function);
        assertEquals(Integer.valueOf(1), counter.get());
        assertEquals(Integer.valueOf(2), counter.get());
        assertEquals(Integer.valueOf(3), counter.get());
        assertEquals(Integer.valueOf(4), counter.get());
    }
}
