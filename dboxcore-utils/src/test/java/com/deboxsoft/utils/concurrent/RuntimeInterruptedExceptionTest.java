/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : RuntimeInterruptedExceptionTest.java
 *  ClassName  : RuntimeInterruptedExceptionTest
 *  Modified   : 14023922
 */

package com.deboxsoft.utils.concurrent;

import org.junit.Before;
import org.junit.Test;

import static java.lang.Thread.interrupted;
import static org.junit.Assert.*;

public class RuntimeInterruptedExceptionTest {
    @Before
    public void clearInterruptStatus() {
        interrupted();
    }

    @Test
    public void testStringConstructor() {
        final InterruptedException cause = new InterruptedException("original");
        final RuntimeInterruptedException ex = new RuntimeInterruptedException("test", cause);
        assertFalse(interrupted());
        assertEquals("test", ex.getMessage());
        assertSame(cause, ex.getCause());
    }

    @Test
    public void testSimpleConstructor() {
        final InterruptedException cause = new InterruptedException("original");
        final RuntimeInterruptedException ex = new RuntimeInterruptedException(cause);
        assertFalse(interrupted());
        assertTrue(ex.getMessage().contains("original"));
        assertSame(cause, ex.getCause());
    }
}
