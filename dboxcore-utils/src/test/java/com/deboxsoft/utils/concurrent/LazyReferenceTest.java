/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : LazyReferenceTest.java
 *  ClassName  : LazyReferenceTest
 *  Modified   : 14023709
 */

package com.deboxsoft.utils.concurrent;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import static com.deboxsoft.utils.concurrent.TestUtil.pause;
import static org.junit.Assert.*;

public class LazyReferenceTest {
    private static final Logger logger = LoggerFactory.getLogger(LazyReferenceTest.class);

    @Test
    public void testConcurrentCreate() throws Exception {
        final int nThreads = 40;
        final Object[] results = new Object[nThreads];
        final AtomicInteger createCallCount = new AtomicInteger(0);
        assertEquals(0, createCallCount.get());
        final LazyReference<Object> dbx_reference = new LazyReference<Object>() {
            @Override
            protected Object create() throws Exception {
                createCallCount.incrementAndGet();
                if (logger.isTraceEnabled()) logger.trace("create object count : {}", createCallCount.get());
                pause(50);
                return new Object();
            }
        };

        /**
         * pool size must be large enough to accommodate all Callables running in
         * parallel as they latch-> block
         **/
        final ExecutorService dbx_pool = Executors.newFixedThreadPool(nThreads);
        final CountDownLatch dbx_latch = new CountDownLatch(nThreads);
        final List<Callable<Object>> dbx_tasks = new ArrayList<>(nThreads);

        for (int i = 0; i < nThreads; i++) {
            logger.trace("looping thread");
            final int j = i;
            dbx_tasks.add(new Callable<Object>() {
                @Override
                public Object call() throws Exception {
                    if (logger.isTraceEnabled()) logger.trace("call object from Callable ");
                    /**
                     * Put in a latch to synchronize all threads and try to get them to
                     * call ref.get() at the same time (to increase concurrency and make
                     * this test more useful)
                     */
                    dbx_latch.countDown();
                    dbx_latch.await();
                    results[j] = dbx_reference.get();
                    return results[j];
                }
            });
        }

        if (logger.isTraceEnabled()) logger.trace("invoke all thread in the pool");
        List<Future<Object>> dbx_futures;
        dbx_futures = dbx_pool.invokeAll(dbx_tasks);
        // Ensure the create() method was invoked once
        assertEquals(1, createCallCount.get());

        /**
         * Ensure that all the references are the same, use the futures in case of
         * exception
         **/
        final Object result = results[0];
        for (final Future<Object> dbx_future : dbx_futures) {
            assertSame(result, dbx_future.get());
        }

        for (int i = 0; i < results.length; i++) {
            assertSame("got back different reference in '" + i + "' place", result, results[i]);
        }
        dbx_pool.shutdown();
    }

    @Test
    public void testException() throws Exception {
        final Exception dbx_exception = new Exception();
        final LazyReference<Object> dbx_reference = new LazyReference<Object>() {
            @Override
            protected Object create() throws Exception {
                throw dbx_exception;
            }
        };

        try {
            dbx_reference.get();
            fail("expected runtimeexception");
        } catch (final RuntimeException e) {
            assertNotNull(e.getCause());
            assertTrue(e.getCause().equals(dbx_exception));
        }
    }

    @Test
    public void testGetNotInterrupable() throws Exception {
        final BooleanLatch dbx_latch = new BooleanLatch();
        final LazyReference<Integer> dbx_reference = new LazyReference<Integer>() {
            @Override
            protected Integer create() throws Exception {
                // do not interrupt
                while (true) {
                    try {
                        dbx_latch.await();
                        return 10;
                    } catch (InterruptedException ignored) {
                        logger.trace("interrupt");
                    }
                }
            }
        };
        final Thread dbx_client = new Thread(new Runnable() {
            @Override
            public void run() {
                dbx_reference.get();
            }
        }, "client thread");
        dbx_client.start();
        if (logger.isTraceEnabled()) logger.trace("client start");
        for (int i = 0; i < 10; i++) {
            pause();
            assertFalse(dbx_reference.isInitialized());
            dbx_client.interrupt();
        }
        pause();
        assertFalse(dbx_reference.isInitialized());

        dbx_latch.release();
        pause();
        assertTrue(dbx_reference.isInitialized());

        //noinspection ConstantConditions
        int result = dbx_reference.get();
        assertEquals(10, result);
    }

    @Test(expected = InterruptedException.class)
    public void testGetInterruptibilityThrowInterrupted() throws Exception {
        LazyReference<String> dbx_reference = new LazyReference<String>() {
            @Override
            protected String create() throws Exception {
                return "test";
            }
        };
        Thread.currentThread().interrupt();
        if (logger.isTraceEnabled()) logger.trace("interrupt still running");
        dbx_reference.getInterruptibly();
        if (logger.isTraceEnabled()) logger.trace("cek no running");
    }

    @Test
    public void testGetInterruptibility() throws Exception {
        final class Result<T> {
            final T dbx_result;
            final Exception dbx_exception;

            Result(T dbx_result) {
                if (logger.isTraceEnabled()) logger.trace("constructor result integer");
                this.dbx_result = dbx_result;
                this.dbx_exception = null;
            }

            Result(final Exception dbx_exception) {
                if (logger.isTraceEnabled()) logger.trace("constructor exception value");
                dbx_result = null;
                this.dbx_exception = dbx_exception;
            }
        }

        final BooleanLatch dbx_latch = new BooleanLatch();
        final LazyReference<Integer> dbx_reference = new LazyReference<Integer>() {
            @Override
            protected Integer create() throws Exception {
                // do not interrupt
                while (true) {
                    try {
                        dbx_latch.await();
                        return 10;
                    } catch (final InterruptedException ignore) {

                    }
                }
            }

        };
        final AtomicReference<Result<Integer>> dbx_result1 = new AtomicReference<>();
        final Thread dbx_client1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dbx_result1.compareAndSet(null, new Result<>(dbx_reference.getInterruptibly()));
                } catch (Exception e) {
                    dbx_result1.compareAndSet(null, new Result<Integer>(e));
                }
            }
        }, "thread client 1");
        dbx_client1.start();
        pause();
        final AtomicReference<Result<Integer>> dbx_result2 = new AtomicReference<>();
        final Thread dbx_client2 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dbx_result2.compareAndSet(null, new Result<>(dbx_reference.getInterruptibly()));
                } catch (InterruptedException e) {
                    dbx_result2.compareAndSet(null, new Result<Integer>(e));
                }
            }
        }, "thread client 2");
        dbx_client2.start();
        for (int i = 0; i < 10; i++) {
            pause();
            assertFalse(dbx_reference.isInitialized());
            dbx_client1.interrupt();
            dbx_client2.interrupt();
        }
        assertNull(dbx_result1.get());
        assertNotNull(dbx_result2.get().dbx_exception);
        assertEquals(InterruptedException.class, dbx_result2.get().dbx_exception.getClass());
        pause();
        assertFalse(dbx_reference.isInitialized());

        dbx_latch.release();
        pause();
        assertTrue(dbx_reference.isInitialized());

        {
            //noinspection ConstantConditions
            final int result = dbx_reference.get();
            assertEquals(10, result);
        }
        assertNotNull(dbx_result1.get());
        assertNotNull(dbx_result1.get().dbx_result);
        {
            final int result = dbx_result1.get().dbx_result;
            assertEquals(10, result);
        }
    }

    @Test(expected = CancellationException.class)
    public void cancellable() throws Exception {
        final LazyReference<String> ref = new LazyReference<String>() {
            // /CLOVER:OFF
            @Override
            protected String create() throws Exception {
                return "created!";
            }
            // /CLOVER:ON
        };
        ref.cancel();
        ref.get(); // throws
    }

    @Test
    public void getNotInterruptible() throws Exception {
        final LazyReference<String> ref = new LazyReference<String>() {
            @Override
            protected String create() throws Exception {
                return "test!";// exchange.get();
            }
        };
        Thread.currentThread().interrupt();
        ref.get();
        assertTrue(Thread.interrupted());
    }

    @Test
    public void initExConstructorWithBlankExecExCause() throws Exception {
        @SuppressWarnings("serial")
        final ExecutionException e = new ExecutionException("") {
        };
        final Exception ex = new LazyReference.InitializationException(e);
        assertSame(e, ex.getCause());
    }

    @Test
    public void initExConstructorWithRealExecExCause() throws Exception {
        final NoSuchMethodError er = new NoSuchMethodError();
        final ExecutionException e = new ExecutionException("", er);
        final Exception ex = new LazyReference.InitializationException(e);
        assertSame(er, ex.getCause());
    }
}
