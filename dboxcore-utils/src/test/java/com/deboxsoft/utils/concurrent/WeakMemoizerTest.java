/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : WeakMemoizerTest.java
 *  ClassName  : WeakMemoizerTest
 *  Modified   : 14023922
 */

package com.deboxsoft.utils.concurrent;

import com.deboxsoft.utils.base.DbxFunctions;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import org.junit.Test;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

import static com.deboxsoft.utils.concurrent.WeakMemoizer.MappedReference;
import static org.junit.Assert.*;

public class WeakMemoizerTest {
    static Function<Integer, MockTest> lock() {
        return DbxFunctions.fromSupplier(new Supplier<MockTest>() {
            public MockTest get() {
                return new MockTest();
            }
        });
    }

    @Test
    public void callingTwiceReturnsSame() throws Exception {
        final WeakMemoizer<Integer, MockTest> memoizer = WeakMemoizer.weakMemoizer(lock());
        assertSame(memoizer.apply(1), memoizer.apply(1));
    }

    @Test
    public void callingDifferentMemoizersReturnsDif2ferent() throws Exception {
        assertNotSame(WeakMemoizer.weakMemoizer(lock()).apply(1), WeakMemoizer.weakMemoizer(lock()).apply(1));
    }

    @Test
    public void lockReferenceNotNull() throws Exception {
        final MappedReference<String, String> ref = new MappedReference<>("test", "value", new ReferenceQueue<String>());
        assertNotNull(ref.getDescriptor());
        assertNotNull(ref.get());
    }

    @Test(expected = NullPointerException.class)
    public void referenceNullDescriptor() throws Exception {
        new MappedReference<String, String>(null, "value", new ReferenceQueue<String>());
    }

    @Test(expected = NullPointerException.class)
    public void referenceNullValue() throws Exception {
        new MappedReference<>("ref", null, new ReferenceQueue<String>());
    }

    @Test
    public void many() throws Exception {
        final WeakMemoizer<Integer, MockTest> memoizer = WeakMemoizer.weakMemoizer(lock());

        final int size = 10000;
        for (int i = 0; i < 10; i++) {
            System.gc();
            for (int j = 0; j < size; j++) {
                assertSame(memoizer.apply(j), memoizer.apply(j));
            }
        }
    }

    @Test
    public void losesReference() throws Exception {
        final WeakMemoizer<Integer, MockTest> memoizer = WeakMemoizer.weakMemoizer(lock());

        final WeakReference<MockTest> one = new WeakReference<>(memoizer.apply(1));
        for (int i = 0; i < 10; i++) {
            System.gc();
        }
        assertNotNull(memoizer.apply(1));
        assertNull(one.get());
    }

    private static class MockTest{
        String test = "test";

        private MockTest() {
        }
    }
}
