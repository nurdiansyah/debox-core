/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : StrongTest.java
 *  ClassName  : StrongTest
 *  Modified   : 14023922
 */

package com.deboxsoft.utils.concurrent;

import com.google.common.base.Supplier;
import org.junit.Test;

import static java.lang.Integer.valueOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class StrongTest {
    @Test
    public void lazilyPopulated() throws Exception {
        final Counter counter = new Counter();
        final Supplier<Integer> lazy = new Lazy.Strong<>(counter);
        assertEquals(0, counter.dbx_counter.get());
        assertEquals(valueOf(1), lazy.get());
        assertEquals(1, counter.dbx_counter.get());
    }

    @Test
    public void memoized() throws Exception {
        final Counter counter = new Counter();
        final Supplier<Integer> lazy = new Lazy.Strong<>(counter);
        assertEquals(valueOf(1), lazy.get());
        assertEquals(1, counter.dbx_counter.get());
        assertEquals(valueOf(1), lazy.get());
        assertEquals(1, counter.dbx_counter.get());
    }

    @Test
    public void supplierIsGarbageCollectible() throws Exception {
        final Lazy.Strong<Integer> lazy = new Lazy.Strong<>(new Counter());
        assertEquals(valueOf(1), lazy.get());
        assertNull(lazy.supplier);
    }


}
