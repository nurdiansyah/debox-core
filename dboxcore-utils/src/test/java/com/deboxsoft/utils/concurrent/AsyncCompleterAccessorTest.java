/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : AsyncCompleterAccessorTest.java
 *  ClassName  : AsyncCompleterAccessorTest
 *  Modified   : 14023809
 */

package com.deboxsoft.utils.concurrent;

import com.google.common.util.concurrent.SettableFuture;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.deboxsoft.utils.concurrent.AsyncCompleter.*;
import static java.util.concurrent.TimeUnit.MICROSECONDS;
import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static org.junit.Assert.*;

public class AsyncCompleterAccessorTest {
    @SuppressWarnings("UnusedDeclaration")
    private static final Logger Logger = LoggerFactory.getLogger(AsyncCompleterAccessorTest.class);

    @Test(expected = RuntimeTimeoutException.class)
    public void testTimesOut() throws Exception {
        final Accessor<String> dbx_accessor = new TimeoutAccessor<>(Timeout.getMillisTimeout(1, MICROSECONDS));
        dbx_accessor.apply(new MockCompletionService() {
            @Override
            public Future<String> poll(final long timeout, final TimeUnit unit) throws InterruptedException {
                return null;
            }
        });
    }

    @Test
    public void testReturns() throws Exception {
        final Accessor<String> dbx_accessor = new TimeoutAccessor<>(Timeout.getMillisTimeout(1, MICROSECONDS));
        assertEquals("hallo", dbx_accessor.apply(new MockCompletionService() {
            @Override
            public Future<String> poll(final long timeout, final TimeUnit unit) throws InterruptedException {
                assertSame(TimeUnit.MILLISECONDS, unit);
                final SettableFuture<String> dbx_result = SettableFuture.create();
                dbx_result.set("hallo");
                return dbx_result;
            }
        }));
    }

    @Test
    public void usesTimeout() {
        final Accessor<String> a = new TimeoutAccessor<>(new Timeout(5, NANOSECONDS, new MockTimeSupplier(1, NANOSECONDS)));
        final MockCompletionService completionService = new MockCompletionService() {
            @Override
            public Future<String> poll(final long time, final TimeUnit unit) {
                assertSame(NANOSECONDS, unit);
                SettableFuture<String> dbx_settableFuture = SettableFuture.create();
                dbx_settableFuture.set(String.valueOf(time));
                return dbx_settableFuture;
            }
        };
        assertEquals("4", a.apply(completionService));
        assertEquals("3", a.apply(completionService));
        assertEquals("2", a.apply(completionService));
        assertEquals("1", a.apply(completionService));
        assertEquals("0", a.apply(completionService));
    }

    @Test(expected = RuntimeInterruptedException.class)
    public void handlesInterrupt() {
        final Accessor<String> a = new TimeoutAccessor<>(new Timeout(5, NANOSECONDS, new MockTimeSupplier(1, NANOSECONDS)));
        a.apply(new MockCompletionService() {
            @Override
            public Future<String> poll(final long time, final TimeUnit unit) throws InterruptedException {
                throw new InterruptedException();
            }
        });
    }

    @Test(expected = RuntimeExecutionException.class)
    public void handlesExecution() {
        final Accessor<String> a = new TimeoutAccessor<>(new Timeout(5, NANOSECONDS, new MockTimeSupplier(1, NANOSECONDS)));
        a.apply(new MockCompletionService() {
            @Override
            public Future<String> poll(final long time, final TimeUnit unit) {
                SettableFuture<String> dbx_settableFuture = SettableFuture.create();
                dbx_settableFuture.setException(new SQLException());
                return dbx_settableFuture;
            }
        });
    }

    @Test(expected = RuntimeInterruptedException.class)
    public void blockingHandlesInterrupt() {
        final Accessor<String> a = new BlockingAccessor<>();
        a.apply(new MockCompletionService() {
            @Override
            public java.util.concurrent.Future<String> take() throws InterruptedException {
                throw new InterruptedException();
            }
        });
    }

    @Test(expected = RuntimeExecutionException.class)
    public void blockingHandlesExecution() {
        final Accessor<String> a = new BlockingAccessor<>();
        a.apply(new MockCompletionService() {
            @Override
            public java.util.concurrent.Future<String> take() {
                SettableFuture<String> dbx_settableFuture = SettableFuture.create();
                dbx_settableFuture.setException(new SQLException());
                return dbx_settableFuture;
            }
        });
    }

    @Test
    public void timeoutCancels() {
        final Accessor<String> t = new TimeoutAccessor<>(Timeout.getNanosTimeout(1, NANOSECONDS));
        final AtomicBoolean cancelled = new AtomicBoolean(false);
        final Future<String> f = new MockFuture<String>() {
            @Override
            public boolean cancel(final boolean mayInterruptIfRunning) {
                cancelled.set(mayInterruptIfRunning);
                return true;
            }
        };
        t.register(f);
        final MockCompletionService completionService = new MockCompletionService() {
            @Override
            public Future<String> poll(final long timeout, final TimeUnit unit) throws InterruptedException {
                return null;
            }
        };
        try {
            t.apply(completionService);
            fail("RuntimeTimeoutException expected");
        } catch (final RuntimeTimeoutException expected) {
        }
        assertTrue(cancelled.get());
        // check that it doesn't get cancelled again
        cancelled.set(false);
        try {
            t.apply(completionService);
            fail("RuntimeTimeoutException expected");
        } catch (final RuntimeTimeoutException expected) {
        }
        assertFalse(cancelled.get());
    }

}
