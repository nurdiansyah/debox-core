/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : CallerExecutor.java
 *  ClassName  : CallerExecutor
 *  Modified   : 14023910
 */

package com.deboxsoft.utils.concurrent;

import com.deboxsoft.utils.base.NotNull;

import java.util.concurrent.Executor;

class CallerExecutor implements Executor {
  public void execute(@NotNull final Runnable command) {
    command.run();
  }
}
