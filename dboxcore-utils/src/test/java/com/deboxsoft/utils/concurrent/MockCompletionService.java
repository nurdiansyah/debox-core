/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : MockCompletionService.java
 *  ClassName  : MockCompletionService
 *  Modified   : 14023809
 */

package com.deboxsoft.utils.concurrent;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@SuppressWarnings("NullableProblems")
public class MockCompletionService implements java.util.concurrent.CompletionService<String> {
    @Override
    public Future<String> submit(final Callable<String> task) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Future<String> submit(final Runnable task, final String result) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Future<String> take() throws InterruptedException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Future<String> poll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Future<String> poll(final long timeout, final TimeUnit unit) throws InterruptedException {
        throw new UnsupportedOperationException();
    }
}
