/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : LimitedExecutorTest.java
 *  ClassName  : LimitedExecutorTest
 *  Modified   : 14023911
 */

package com.deboxsoft.utils.concurrent;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;

public class LimitedExecutorTest {
    @Test
    public void only() throws InterruptedException {
        final AtomicInteger count = new AtomicInteger(0);
        final Executor executor = new LimitedExecutor(new NaiveExecutor(), 2);
        final PhasedLatch ready = new PhasedLatch();
        final CountDownLatch release = new CountDownLatch(1);
        class Adder implements Runnable {
          @Override public void run() {
            count.incrementAndGet();
            ready.release();
            try {
              release.await();
            } catch (final InterruptedException ignored) {}
          }
        }
        executor.execute(new Adder());
        executor.execute(new Adder());
        executor.execute(new Adder());
        executor.execute(new Adder());
        executor.execute(new Adder());
        ready.awaitPhase(1);
        assertEquals(2, count.get());
        assertEquals(2, count.get());
        release.countDown();
        ready.awaitPhase(4);
        assertEquals(5, count.get());
      }
}
