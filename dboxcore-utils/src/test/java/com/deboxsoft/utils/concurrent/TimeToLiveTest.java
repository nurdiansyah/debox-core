/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : TimeToLiveTest.java
 *  ClassName  : TimeToLiveTest
 *  Modified   : 14023922
 */

package com.deboxsoft.utils.concurrent;

import com.google.common.base.Predicate;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TimeToLiveTest {
    @Test
    public void timesOut() {
        final Timeout timeout = new Timeout(3, TimeUnit.NANOSECONDS, new MockTimeSupplier(2, TimeUnit.NANOSECONDS));
        final Predicate<Void> ttl = new Lazy.TimeToLive(timeout);
        assertTrue(ttl.apply(null));
        assertTrue(ttl.apply(null));
        assertFalse(ttl.apply(null));
    }

    @Test
    public void staysTimedOut() {
        final Timeout timeout = new Timeout(2, TimeUnit.NANOSECONDS, new MockTimeSupplier(2, TimeUnit.NANOSECONDS));
        final Predicate<Void> ttl = new Lazy.TimeToLive(timeout);
        assertTrue(ttl.apply(null));
        for (int i = 0; i < 1000; i++) {
            assertFalse(ttl.apply(null));
        }
    }
}
