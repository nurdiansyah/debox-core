/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : MockFuture.java
 *  ClassName  : MockFuture
 *  Modified   : 14023910
 */

package com.deboxsoft.utils.concurrent;

import com.deboxsoft.utils.base.NotNull;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class MockFuture<T> implements Future<T> {
    public T get() throws InterruptedException, ExecutionException {
      throw new UnsupportedOperationException();
    }

    public T get(final long timeout, @NotNull final TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
      throw new UnsupportedOperationException();
    }

    public boolean isCancelled() {
      throw new UnsupportedOperationException();
    }

    public boolean isDone() {
      throw new UnsupportedOperationException();
    }

    public boolean cancel(final boolean mayInterruptIfRunning) {
      throw new UnsupportedOperationException();
    }
}
