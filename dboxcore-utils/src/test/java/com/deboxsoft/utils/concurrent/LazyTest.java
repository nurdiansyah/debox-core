/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : LazyTest.java
 *  ClassName  : LazyTest
 *  Modified   : 14023706
 */

package com.deboxsoft.utils.concurrent;

import com.google.common.base.Supplier;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

import static com.deboxsoft.utils.concurrent.TestUtil.pause;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class LazyTest {
    @Test
    public void testStrong() throws Exception {
        Supplier<Integer> s = Lazy.supplier(new Counter());
        assertSame(Lazy.Strong.class, s.getClass());
        assertEquals(Integer.valueOf(1), s.get());
    }

    @Test
    public void testTimeToLive() throws Exception {
        Supplier<Integer> s = Lazy.timeToLive(new Counter(), 10, TimeUnit.MILLISECONDS);
        assertSame(Expiring.class, s.getClass());
        assertEquals(Integer.valueOf(1), s.get());
        assertEquals(Integer.valueOf(1), s.get());
        pause(10);
        assertEquals(Integer.valueOf(2), s.get());
    }

    @Test
    public void testTimeToIdle() throws Exception {
        Supplier<Integer> s = Lazy.timeToIdle(new Counter(), 10, TimeUnit.MILLISECONDS);
        assertSame(Expiring.class, s.getClass());
        assertEquals(Integer.valueOf(1), s.get());
        pause(2);
        assertEquals(Integer.valueOf(1), s.get());
        pause(10);
        assertEquals(Integer.valueOf(2), s.get());
    }
}
