/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : ForwardingPromiseTest.java
 *  ClassName  : ForwardingPromiseTest
 *  Modified   : 14023806
 */

package com.deboxsoft.utils.concurrent;

import com.google.common.util.concurrent.FutureCallback;
import org.junit.Test;

import static org.junit.Assert.assertSame;

public class ForwardingPromiseTest {
    private final Promise<Object> dbx_promise = Promises.promise(new Object());
    private final ForwardingPromise<Object> dbx_forwardingPromise = new ForwardingPromise<Object>() {
        @Override
        protected Promise<Object> delegate() {
            return dbx_promise;
        }
    };

    @SuppressWarnings("UnusedParameters")
    <A>Effect<A> doNothing(Class<A> c) {
        return new Effect<A>() {
            @Override
            public void apply(final A a) {

            }
        };
    }

    @Test
    public void testDoneReturnThis() throws Exception {
        assertSame(dbx_forwardingPromise, dbx_forwardingPromise.done(doNothing(Object.class)));
    }

    @Test
    public void testFailReturnThis() throws Exception {
        assertSame(dbx_forwardingPromise, dbx_forwardingPromise.fail(doNothing(Throwable.class)));
    }

    @Test
    public void testThenReturnThis() throws Exception {
        assertSame(dbx_forwardingPromise, dbx_forwardingPromise.then(new FutureCallback<Object>() {
            @Override
            public void onSuccess(final Object result) {

            }

            @Override
            public void onFailure(final Throwable t) {

            }
        }));
    }
}
