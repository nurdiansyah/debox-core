/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : NaiveExecutor.java
 *  ClassName  : NaiveExecutor
 *  Modified   : 14023910
 */

package com.deboxsoft.utils.concurrent;

import com.deboxsoft.utils.base.NotNull;

import java.util.concurrent.Executor;

public class NaiveExecutor implements Executor {
    public void execute(@NotNull final Runnable command) {
        new Thread(command).start();
      }
}
