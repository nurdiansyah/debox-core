/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : MockTimeSupplier.java
 *  ClassName  : MockTimeSupplier
 *  Modified   : 14023910
 */

package com.deboxsoft.utils.concurrent;

import java.util.concurrent.TimeUnit;

public class MockTimeSupplier implements Timeout.TimeSupplier {
    private int currentTimeCalled;
    private final long time;
    private final TimeUnit unit;

    MockTimeSupplier(final long time, final TimeUnit unit) {
      this.time = time;
      this.unit = unit;
    }

    public long currentTime() {
      return time + currentTimeCalled++;
    }

    public TimeUnit precision() {
      return unit;
    }
}
