/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : java.java
 *  ClassName  : java
 *  Modified   : 14023922
 */

package com.deboxsoft.utils.concurrent;

import com.google.common.base.Predicate;
import org.junit.Test;

import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TimeToIdleTest {
    @Test
    public void timesOut() {
        final MockTimeSupplier supplier = new MockTimeSupplier(2, NANOSECONDS);
        final Predicate<Void> ttl = new Lazy.TimeToIdle(Timeout.timeoutFactory(2, NANOSECONDS, supplier));
        assertTrue(ttl.apply(null));
        assertTrue(ttl.apply(null));
        // advance
        supplier.currentTime();
        for (int i = 0; i < 1000; i++) {
            assertFalse(String.valueOf(i), ttl.apply(null));
        }
    }

    @Test
    public void resetsAfterSuccess() {
        final MockTimeSupplier supplier = new MockTimeSupplier(2, NANOSECONDS);
        final Predicate<Void> ttl = new Lazy.TimeToIdle(Timeout.timeoutFactory(2, NANOSECONDS, supplier));
        for (int i = 0; i < 1000; i++) {
            assertTrue(String.valueOf(i), ttl.apply(null));
        }
    }
}
