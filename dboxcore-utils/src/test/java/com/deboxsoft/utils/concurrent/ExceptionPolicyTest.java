/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : ExceptionPolicyTest.java
 *  ClassName  : ExceptionPolicyTest
 *  Modified   : 14023911
 */

package com.deboxsoft.utils.concurrent;

import com.google.common.base.Supplier;
import org.junit.Test;

import static org.junit.Assert.assertNull;

public class ExceptionPolicyTest {
    @Test(expected = TestException.class)
    public void exceptionsThrow() {
        exception(ExceptionPolicy.Policies.THROW);
    }

    @Test
    public void exceptionsIgnored() {
        assertNull(exception(ExceptionPolicy.Policies.IGNORE_EXCEPTIONS));
    }

    private Object exception(final ExceptionPolicy.Policies handler) {
        final Supplier<Object> wrapped = handler.handler().apply(new Supplier<Object>() {
            public Object get() {
                throw new TestException();
            }
        });
        @SuppressWarnings("ConstantConditions") final Object result = wrapped.get();
        return result;
    }

    public class TestException extends RuntimeException {
        private static final long serialVersionUID = -2371420516340597047L;
    }
}
