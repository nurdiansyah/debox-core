/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : TestUtil.java
 *  ClassName  : TestUtil
 *  Modified   : 14023709
 */

package com.deboxsoft.utils.concurrent;

import org.junit.Ignore;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

@Ignore
final class TestUtil {
    static final int WAIT = 10;

    static void pause(final int millis) {
        try {
            Thread.sleep(millis);
        } catch (final InterruptedException e) {
            // /CLOVER:OFF
            throw new RuntimeException(e);
            // /CLOVER:ON
        }
    }

    static void pause() {
        pause(WAIT);
    }

    @SuppressWarnings("unchecked")
    static <T> T serialize(final T map) {
        final ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        try {
            new ObjectOutputStream(bytes).writeObject(map);
            return (T) new ObjectInputStream(new ByteArrayInputStream(bytes.toByteArray())).readObject();
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }
}
