/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-lib
 *  Module     : dboxutils
 *  File       : logback.groovy
 *  ClassName  : logback.groovy
 *  Modified   : 14012611
 */


import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.status.OnConsoleStatusListener

import static ch.qos.logback.classic.Level.*

def devel = true
def baseTrace = "com.deboxsoft.utils"
displayStatusOnConsole()
//scan('10 minutes')  // Scan for changes every 5 minutes.
setupAppenders(devel)
setupLoggers(devel, baseTrace)




def displayStatusOnConsole() {
    statusListener(OnConsoleStatusListener)
}

def setupAppenders(devel) {


    appender('systemOut', ConsoleAppender) {
        encoder(PatternLayoutEncoder) {
            if(devel){
                pattern = "[%-5level|%d{dd/MM HH:mm:ss.SSS}|%thread|%logger{36}:%line] - %msg%n"
            }else{
                pattern = "[%-5level|%d{dd/MM HH:mm:ss.SSS}|%thread|%logger{36}] - %msg%n"
            }
        }
    }
}

def setupLoggers(devel, baseTrace) {
    if (!devel) {
        root  INFO, ['systemOut']
    }
    else {
        if(baseTrace != null && baseTrace != ""){
            logger(baseTrace, TRACE)
        }
        root  INFO, ['systemOut']
        logger("com.deboxsoft", DEBUG)
    }
}
