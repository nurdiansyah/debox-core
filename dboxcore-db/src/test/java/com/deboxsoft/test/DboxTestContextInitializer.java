package com.deboxsoft.test;/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : TestContextLoader.java
 *  ClassName  : TestContextLoader
 *  Modified   : 14024909
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.mock.env.MockPropertySource;

public class DboxTestContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    private static final Logger logger = LoggerFactory.getLogger(DboxTestContextInitializer.class);
    @Override
    public void initialize(final ConfigurableApplicationContext applicationContext) {
        if (logger.isTraceEnabled()) logger.trace("initialize application context");
        MutablePropertySources dbx_propertySources = applicationContext.getEnvironment().getPropertySources();
        MockPropertySource dbx_mockPropertySource = new MockPropertySource();
        dbx_propertySources.replace(StandardEnvironment.SYSTEM_ENVIRONMENT_PROPERTY_SOURCE_NAME, dbx_mockPropertySource);
    }
}
