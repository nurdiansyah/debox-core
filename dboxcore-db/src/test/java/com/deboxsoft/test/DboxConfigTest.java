package com.deboxsoft.test;/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DboxConfigTest.java
 *  ClassName  : DboxConfigTest
 *  Modified   : 14024910
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.deboxsoft.db")
public class DboxConfigTest {
    private static final Logger logger = LoggerFactory.getLogger(DboxConfigTest.class);

    public DboxConfigTest() {
        if (logger.isTraceEnabled()) logger.trace("instance config test");

    }
}
