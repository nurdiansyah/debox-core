/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : TestMock.java
 *  ClassName  : TestMock
 *  Modified   : 14024917
 */

package com.deboxsoft.test;

public class TestMock {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(final String dbx_name) {
        name = dbx_name;
    }
}
