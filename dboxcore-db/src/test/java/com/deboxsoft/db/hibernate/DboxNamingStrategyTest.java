/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-db
 *  File       : DboxNamingStrategyTest.java
 *  ClassName  : DboxNamingStrategyTest
 *  Modified   : 14037009
 */

package com.deboxsoft.db.hibernate;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DboxNamingStrategyTest {

    @Test
    public void testReplaceUCToUnderscore() throws Exception {
        DboxNamingStrategy strategy = new DboxNamingStrategy();
        assertEquals("FIRST_NAME", strategy.columnName("firstName"));
        assertEquals("FIRST_NAME", strategy.propertyToColumnName("firstName"));
        assertEquals("LAST_NAME", strategy.propertyToColumnName("lastName"));
    }


}
