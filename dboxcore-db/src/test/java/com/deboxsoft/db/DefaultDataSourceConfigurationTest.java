/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DefaultDataSourceConfigurationTest.java
 *  ClassName  : DefaultDataSourceConfigurationTest
 *  Modified   : 14024917
 */

package com.deboxsoft.db;

import com.deboxsoft.db.api.DataSourceConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

@ContextConfiguration(locations = "classpath:datasource-configuration-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class DefaultDataSourceConfigurationTest {

    @Autowired
    private DataSourceConfiguration dbx_dataSourceConfiguration;

    @Test
    public void testCekConfiguration() throws Exception {
        assertEquals("", dbx_dataSourceConfiguration.getUser());
        assertEquals("", dbx_dataSourceConfiguration.getPassword());
    }
}
