/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DboxBondCPDataSource.java
 *  ClassName  : DboxBondCPDataSource
 *  Modified   : 14024716
 */

package com.deboxsoft.db;

import com.jolbox.bonecp.BoneCPDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.util.concurrent.TimeUnit;

public class DboxBondCPDataSource extends BoneCPDataSource implements Closeable, DrainableDataSource {

    private static final long serialVersionUID = -4149481806169318154L;
    private static final Logger logger = LoggerFactory.getLogger(DboxBondCPDataSource.class);
    private long drainPollInterval = TimeUnit.SECONDS.toMillis(2L);

    public DboxBondCPDataSource(final DefaultDataSourceConfiguration dbx_dataSourceConfiguration) {
        setDriverClass(dbx_dataSourceConfiguration.getDriverClassName());
        setUser(dbx_dataSourceConfiguration.getUser());
        setPassword(dbx_dataSourceConfiguration.getPassword());
        setJdbcUrl(dbx_dataSourceConfiguration.getUrl());
        setDriverProperties(dbx_dataSourceConfiguration.getProperties());
    }

    @Override
    public boolean drain(final long dbx_timeout, final TimeUnit dbx_timeUnit) {
        long start = System.currentTimeMillis();
        long end = start + dbx_timeUnit.toMillis(dbx_timeout);
        if (logger.isDebugEnabled()) logger.debug("Draining the connection pool");
        for (int leased = getTotalLeased(); leased > 0; leased = getTotalLeased()) {
            long tillEnd = end - System.currentTimeMillis();
            if (tillEnd <= 0){
                logger.warn("The connection pool did not drain in {} {}; {} connections are still leased", dbx_timeout, dbx_timeUnit, leased);
                return false;
            }
            long interval = Math.min(drainPollInterval, tillEnd);
            logger.debug("{} connections still leased; waiting {} milliseconds", Integer.valueOf(leased), interval);
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                return false;
            }
        }
        logger.debug("The connection pool has drained in {} milliseconds", Long.valueOf(System.currentTimeMillis() - start));
        return true;
    }

    public void setConnectionTimeoutInSeconds(int timeout){
        setConnectionTimeout(timeout, TimeUnit.SECONDS);
    }
}
