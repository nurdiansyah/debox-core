/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DetailedDatabase.java
 *  ClassName  : DetailedDatabase
 *  Modified   : 14024811
 */

package com.deboxsoft.db.api;

import com.deboxsoft.utils.base.NotNull;

public interface DetailedDatabase extends Database {
    @NotNull
    public DatabaseSupportLevel getSupportLevel();
}
