/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-db
 *  File       : SwapableEntityManagerFactory.java
 *  ClassName  : SwapableEntityManagerFactory
 *  Modified   : 14025508
 */

package com.deboxsoft.db.api;

import com.deboxsoft.utils.base.NotNull;
import org.springframework.core.InfrastructureProxy;

import javax.persistence.EntityManagerFactory;

public interface SwappableEntityManagerFactory extends InfrastructureProxy, EntityManagerFactory {

    @NotNull
    public EntityManagerFactory swap(@NotNull EntityManagerFactory dbx_target);
}
