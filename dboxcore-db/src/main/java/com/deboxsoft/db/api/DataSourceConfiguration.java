/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DatasourceConfiguration.java
 *  ClassName  : DatasourceConfiguration
 *  Modified   : 14024709
 */

package com.deboxsoft.db.api;

import com.deboxsoft.utils.base.NotNull;
import com.deboxsoft.utils.base.Nullable;

import java.util.Properties;

public interface DataSourceConfiguration {
    public final static String USER_KEY = "user";
    public final static String PASSWORD_KEY = "password";
    public final static String CONNECTION_TIMEOUT_KEY = "connection.timeout";

    @NotNull
    public String getDriverClassName();

    @Nullable
    public String getPassword();

    @NotNull
    public Properties getProperties();

    @NotNull
    public String getUrl();

    @NotNull
    public String getUser();

    public boolean isPasswordSet();
}
