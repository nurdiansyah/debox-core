/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : MutableDataSourceConfiguration.java
 *  ClassName  : MutableDataSourceConfiguration
 *  Modified   : 14024709
 */

package com.deboxsoft.db.api;

public interface MutableDataSourceConfiguration extends DataSourceConfiguration{

    /**
     * update data source configuration
     * @param dbx_dataSourceConfiguration
     * @return old DataSourceConfiguration
     */
    public DataSourceConfiguration update(DataSourceConfiguration dbx_dataSourceConfiguration);
}
