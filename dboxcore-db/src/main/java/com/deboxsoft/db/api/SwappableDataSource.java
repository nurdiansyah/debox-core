/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : SwapableDataSource.java
 *  ClassName  : SwapableDataSource
 *  Modified   : 14024721
 */

package com.deboxsoft.db.api;

import com.deboxsoft.utils.base.NotNull;
import org.springframework.core.InfrastructureProxy;

import javax.sql.DataSource;

public interface SwappableDataSource extends DataSource, InfrastructureProxy {

    @NotNull
    public DataSource swap(@NotNull DataSource dbx_target);
}
