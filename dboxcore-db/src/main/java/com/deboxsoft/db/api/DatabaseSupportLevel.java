/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DatabaseSupportLevel.java
 *  ClassName  : DatabaseSupportLevel
 *  Modified   : 14024810
 */

package com.deboxsoft.db.api;

public enum DatabaseSupportLevel {
    SUPPORTED,
    UNKNOWN,
    UNSUPPORTED
}
