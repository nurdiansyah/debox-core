/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DatabaseType.java
 *  ClassName  : DatabaseType
 *  Modified   : 14024710
 */

package com.deboxsoft.db.api;

import com.deboxsoft.utils.base.NotNull;
import com.deboxsoft.utils.collection.Option;
import com.google.common.collect.ImmutableMap;
import org.springframework.jdbc.datasource.AbstractDriverBasedDataSource;
import org.springframework.util.ClassUtils;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkNotNull;

public enum DatabaseType {
    MSSQL("mssql", "com.deboxsoft.database.mssql", "jdbc:sqlserver", "com.microsoft.sqlserver.jdbc.SQLServerDriver", false, Option.some(1433)),

    MYSQL("mysql", "com.deboxsoft.database.mysql", "jdbc:mysql", "com.mysql.jdbc.Driver", false, Option.some(3306)),

    ORACLE("oracle", "com.deboxsoft.database.oracle", "jdbc:oracle:thin", "oracle.jdbc.driver.OracleDriver", true, Option.some(1521)),

    POSTGRES("postgres", "com.deboxsoft.database.postgres", "jdbc:postgresql", "org.postgresql.Driver", false, Option.some(5432));

    private final String dbx_key;
    private final String dbx_name;
    private final String dbx_protocol;
    private final String dbx_driverClassName;
    private final boolean dbx_usesSid;
    private final Option<String> dbx_defaultDatabaseName;
    private final Option<String> dbx_defaultHostHame;
    private final Option<Integer> dbx_defaultPort;
    private final Option<String> dbx_defaultUserName;

    private DatabaseType(final String dbx_key, final String dbx_name, final String dbx_protocol, final String dbx_driverClassName, final boolean dbx_usesSid, final Option<Integer> dbx_defaultPort) {
        this(dbx_key, dbx_name, dbx_protocol, dbx_driverClassName, dbx_usesSid, Option.<String>none(), Option.<String>none(), dbx_defaultPort, Option.<String>none());
    }
    private DatabaseType(final String dbx_key, final String dbx_name, final String dbx_protocol, final String dbx_driverClassName,
                         final boolean dbx_usesSid, Option<String> dbx_defaultDatabaseName, Option<String> dbx_defaultHostHame, Option<Integer> dbx_defaultPort,
                         Option<String> dbx_defaultUserName) {
        this.dbx_key = dbx_key;
        this.dbx_name = dbx_name;
        this.dbx_protocol = dbx_protocol;
        this.dbx_driverClassName = dbx_driverClassName;
        this.dbx_usesSid = dbx_usesSid;
        this.dbx_defaultDatabaseName = dbx_defaultDatabaseName;
        this.dbx_defaultHostHame = dbx_defaultHostHame;
        this.dbx_defaultPort = dbx_defaultPort;
        this.dbx_defaultUserName =  dbx_defaultUserName;
    }

    /**
     * get database type by key
     * @param dbx_key
     * @return {@link Option}
     */
    public static Option<DatabaseType> forKey(@NotNull String dbx_key){
        checkNotNull(dbx_key);
        return Option.option(keyToEnum().get(dbx_key));
    }

    /**
     * get database type by class name
     * @param dbx_driverClassName
     * @return {@link Option}
     */
    public static Option<DatabaseType> forDriverClass(@NotNull String dbx_driverClassName){
        checkNotNull(dbx_driverClassName);
        return Option.option(driverToEnum().get(dbx_driverClassName));
    }

    @NotNull
    public String getKey() {
        return dbx_key;
    }

    @NotNull
    public String getName() {
        return dbx_name;
    }

    @NotNull
    public String getProtocol() {
        return dbx_protocol;
    }

    @NotNull
    public String getDriverClassName() {
        return dbx_driverClassName;
    }

    public boolean usesSid() {
        return dbx_usesSid;
    }

    @NotNull
    public Option<String> getDefaultDatabaseName() {
        return dbx_defaultDatabaseName;
    }

    @NotNull
    public Option<String> getDefaultHostHame() {
        return dbx_defaultHostHame;
    }

    @NotNull
    public Option<Integer> getDefaultPort() {
        return dbx_defaultPort;
    }

    @NotNull
    public Option<String> getDefaultUserName() {
        return dbx_defaultUserName;
    }

    public boolean isDriverAvailable(){
        return ClassUtils.isPresent(dbx_driverClassName, getClass().getClassLoader());
    }

    /**
     * mapping connection timeout
     * @param dbx_timeout dalam seconds
     * @return
     */
    public Map<String, String> getPropertyMap(long dbx_timeout){
        return getPropertyMap(dbx_timeout, TimeUnit.SECONDS);
    }

    /**
     * mapping connection timeout
     * @param dbx_timeout
     * @param dbx_timeUnit
     * @return
     */
    public Map<String, String> getPropertyMap(long dbx_timeout, TimeUnit dbx_timeUnit){
        return ImmutableMap.of(DataSourceConfiguration.CONNECTION_TIMEOUT_KEY, String.valueOf(dbx_timeUnit.toSeconds(dbx_timeout)));
    }

    /**
     * apply connection timeout timeout
     * @param dbx_timeout
     * @param dbx_basedDataSource
     */
    public void applyTimeout(long dbx_timeout, AbstractDriverBasedDataSource dbx_basedDataSource){
        setDataSourceProperties(getPropertyMap(dbx_timeout), dbx_basedDataSource);
    }


    private static ImmutableMap<String, DatabaseType> keyToEnum() {
        ImmutableMap.Builder<String, DatabaseType> dbx_builder = ImmutableMap.builder();
        for (DatabaseType dbx_databaseType : values()) {
            dbx_builder.put(dbx_databaseType.getKey(), dbx_databaseType);
        }
        return dbx_builder.build();
    }

    private static ImmutableMap<String, DatabaseType> driverToEnum() {
        ImmutableMap.Builder<String, DatabaseType> dbx_builder = ImmutableMap.builder();
        for (DatabaseType dbx_databaseType : values()) {
            dbx_builder.put(dbx_databaseType.getDriverClassName(), dbx_databaseType);
        }
        return dbx_builder.build();
    }

    private void setDataSourceProperties(Map<String, String> dbx_map, AbstractDriverBasedDataSource dbx_basedDataSource){
        Properties dbx_connectionProperties = dbx_basedDataSource.getConnectionProperties();
        if(dbx_connectionProperties == null){
            dbx_connectionProperties = new Properties();
        }
        dbx_connectionProperties.putAll(dbx_map);
        dbx_basedDataSource.setConnectionProperties(dbx_connectionProperties);
    }
}
