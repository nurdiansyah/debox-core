/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : SwapableSessionFactory.java
 *  ClassName  : SwapableSessionFactory
 *  Modified   : 14024721
 */

package com.deboxsoft.db.api;

import com.deboxsoft.utils.base.NotNull;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.springframework.core.InfrastructureProxy;

public interface SwappableSessionFactory extends InfrastructureProxy, SessionFactoryImplementor {
    @NotNull
    public SessionFactoryImplementor swap(@NotNull SessionFactoryImplementor dbx_target);
}
