/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : Database.java
 *  ClassName  : Database
 *  Modified   : 14024709
 */

package com.deboxsoft.db.api;

import com.deboxsoft.app.Version;
import com.deboxsoft.utils.base.NotNull;

public interface Database {

    public static String HSQL = "HSQL Database Engine";
    public static String H2 = "H2";
    public static String SQL_SERVER = "Microsoft SQL Server";
    public static String MYSQL = "MySQL";
    public static String ORACLE = "Oracle";
    public static String POSTGRESQL = "PostgreSQL";

    public int getMajorVersion();

    public int getMinorVersion();

    @NotNull
    public String getName();

    public Integer getPatchVersion();

    @NotNull
    public Version getVersion();
}
