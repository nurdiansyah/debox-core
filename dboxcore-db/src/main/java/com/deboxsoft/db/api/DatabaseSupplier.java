/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DatabaseSupplier.java
 *  ClassName  : DatabaseSupplier
 *  Modified   : 14024810
 */

package com.deboxsoft.db.api;

import com.deboxsoft.utils.base.NotNull;
import com.google.common.base.Supplier;

import java.sql.Connection;
public interface DatabaseSupplier extends Supplier<DetailedDatabase> {
    @NotNull
    public DetailedDatabase getForConnection(@NotNull Connection paramConnection);
}
