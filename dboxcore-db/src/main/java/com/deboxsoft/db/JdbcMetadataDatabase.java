/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : JdbcMetadataDatabase.java
 *  ClassName  : JdbcMetadataDatabase
 *  Modified   : 14024811
 */

package com.deboxsoft.db;

import com.deboxsoft.app.Version;
import com.deboxsoft.db.api.Database;
import com.deboxsoft.utils.base.NotNull;
import com.google.common.collect.Iterables;

public class JdbcMetadataDatabase implements Database {


    private final String dbx_productName;
    @NotNull
    private final Version dbx_version;
    private final int dbx_majorVersion;
    private final int dbx_minorVersion;


    public JdbcMetadataDatabase(final String dbx_productName, @NotNull Version dbx_version,
                                final int dbx_majorVersion, final int dbx_minorVersion) {
        this.dbx_productName = dbx_productName;
        this.dbx_version = dbx_version;
        this.dbx_majorVersion = dbx_majorVersion;
        this.dbx_minorVersion = dbx_minorVersion;
    }

    public int getMajorVersion() {
        return dbx_majorVersion;
    }

    public int getMinorVersion() {
        return dbx_minorVersion;
    }

    @NotNull
    @Override
    public String getName() {
        return dbx_productName;
    }

    @Override
    public Integer getPatchVersion() {
        return Iterables.get(dbx_version.getVersion(), 2, 0);
    }

    @NotNull
    @Override
    public Version getVersion() {
        return this.dbx_version;
    }
}
