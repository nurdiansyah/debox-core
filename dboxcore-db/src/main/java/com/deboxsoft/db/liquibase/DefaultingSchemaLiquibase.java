/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-db
 *  File       : DefaultingSchemaLiquibase.java
 *  ClassName  : DefaultingSchemaLiquibase
 *  Modified   : 14025612
 */

package com.deboxsoft.db.liquibase;

import liquibase.Liquibase;
import liquibase.database.AbstractJdbcDatabase;
import liquibase.database.Database;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.SpringLiquibase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.support.JdbcUtils;

import java.sql.Connection;
import java.sql.SQLException;

public class DefaultingSchemaLiquibase extends SpringLiquibase {
    private static final Logger logger = LoggerFactory.getLogger(DefaultingSchemaLiquibase.class);

    protected Database createDatabase(Connection c) throws DatabaseException {
        Database database = super.createDatabase(c);
        if ((database instanceof AbstractJdbcDatabase)) {
            database.setDefaultSchemaName(database.getDefaultSchemaName());
        }

        return database;
    }

    protected void performUpdate(Liquibase liquibase) throws LiquibaseException {
        super.performUpdate(liquibase);

        Database database = liquibase.getDatabase();
        if (!database.isAutoCommit()) {
            database.setAutoCommit(true);
        }
    }

    public void afterPropertiesSet() throws LiquibaseException {
        String shouldRunProperty = System.getProperty("liquibase.should.run", "true");
        if ((!Boolean.valueOf(shouldRunProperty))) {
            logger.info("Liquibase did not run because 'liquibase.should.run' system property was set to false");

            return;
        }

        Connection c = null;
        try {
            c = getDataSource().getConnection();
            Liquibase liquibase = createLiquibase(c);
            performUpdate(liquibase);
        } catch (SQLException e) {
            throw new DatabaseException(e);
        } finally {
            if (c != null) try {
                c.rollback();
            } catch (SQLException ignored) {
            } finally {
                JdbcUtils.closeConnection(c);
            }
        }
    }
}
