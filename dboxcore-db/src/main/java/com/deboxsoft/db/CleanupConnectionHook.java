/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : CleanupConnectionHook.java
 *  ClassName  : CleanupConnectionHook
 *  Modified   : 14024720
 */

package com.deboxsoft.db;

import com.jolbox.bonecp.ConnectionHandle;
import com.jolbox.bonecp.hooks.AbstractConnectionHook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;

public class CleanupConnectionHook extends AbstractConnectionHook{
    private static final Logger logger = LoggerFactory.getLogger(CleanupConnectionHook.class);

    @Override
    public void onCheckIn(final ConnectionHandle dbx_handle) {
        if (dbx_handle.isPossiblyBroken())
            return;

        try {
            Connection dbx_connection = dbx_handle.getInternalConnection();
            if (!dbx_connection.isClosed() && !dbx_connection.getAutoCommit()){
                logger.debug("A connection was checked into the connection pool with auto-commit disabled. Re-enabling auto-commit before returning to the pool.");
                dbx_connection.setAutoCommit(true);
            }

        } catch (SQLException e) {
            logger.warn("Could not check or set the auto-commit status for the connection being checked in.", e);
        }
    }
}
