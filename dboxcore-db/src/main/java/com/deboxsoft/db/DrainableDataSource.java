/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DrainableDataSource.java
 *  ClassName  : DrainableDataSource
 *  Modified   : 14024717
 */

package com.deboxsoft.db;

import javax.sql.DataSource;
import java.util.concurrent.TimeUnit;

public interface DrainableDataSource extends DataSource{
    public boolean drain(long dbx_timeout, TimeUnit dbx_timeUnit);
}
