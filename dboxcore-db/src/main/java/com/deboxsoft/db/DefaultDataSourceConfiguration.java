/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DefaultDatasourceConfiguration.java
 *  ClassName  : DefaultDatasourceConfiguration
 *  Modified   : 14024709
 */

package com.deboxsoft.db;

import com.deboxsoft.db.api.DataSourceConfiguration;
import com.deboxsoft.db.api.DatabaseType;
import com.deboxsoft.db.api.MutableDataSourceConfiguration;
import com.deboxsoft.utils.base.NotNull;
import com.deboxsoft.utils.base.Nullable;
import com.deboxsoft.utils.concurrent.Effect;
import com.deboxsoft.utils.config.PropertiesUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.Properties;

public class DefaultDataSourceConfiguration implements MutableDataSourceConfiguration {
    private String dbx_driverClassName;
    private String dbx_user;
    private String dbx_password;
    private String dbx_url;
    private long dbx_connectionTimeout;

    public DefaultDataSourceConfiguration(String dbx_driverClassName, String dbx_user, String dbx_password, String dbx_url) {
        this.dbx_driverClassName = dbx_driverClassName;
        this.dbx_user = dbx_user;
        this.dbx_password = dbx_password;
        this.dbx_url = dbx_url;
    }

    @NotNull
    public String getDriverClassName() {
        return dbx_driverClassName;
    }

    @NotNull
    public String getUser() {
        return dbx_user;
    }

    @Override
    public boolean isPasswordSet() {
        return StringUtils.isNotEmpty(dbx_password);
    }
    @Nullable
    public String getPassword() {
        return dbx_password;
    }

    @NotNull
    @Override
    public Properties getProperties() {
        final Properties dbx_properties = new Properties();
        PropertiesUtils.putIfNotBlank(dbx_properties,USER_KEY, dbx_user);
        PropertiesUtils.putIfNotBlank(dbx_properties, PASSWORD_KEY, dbx_user);
        if (dbx_connectionTimeout > 0L){
            DatabaseType.forDriverClass(dbx_driverClassName).foreach(new Effect<DatabaseType>() {
                @Override
                public void apply(final DatabaseType a) {
                    dbx_properties.putAll(a.getPropertyMap(dbx_connectionTimeout));
                }
            });
        }
        return dbx_properties;
    }

    @NotNull
    public String getUrl() {
        return dbx_url;
    }

    @Override
    public DataSourceConfiguration update(DataSourceConfiguration dbx_newConfiguration) {
        DataSourceConfiguration dbx_old = new DefaultDataSourceConfiguration(dbx_driverClassName, dbx_user, dbx_password, dbx_url);
        dbx_driverClassName = dbx_newConfiguration.getDriverClassName();
        dbx_user = dbx_newConfiguration.getUser();
        dbx_password = dbx_newConfiguration.getPassword();
        dbx_url = dbx_newConfiguration.getUrl();
        return dbx_old;
    }

    public String toString() {
        return this.dbx_user + "@" + this.dbx_url + " via " + this.dbx_driverClassName;
    }

    public void setConnectionTimeout(final long dbx_connectionTimeout) {
        this.dbx_connectionTimeout = dbx_connectionTimeout;
    }
}
