/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DefaultDatabaseSupplier.java
 *  ClassName  : DefaultDatabaseSupplier
 *  Modified   : 14024811
 */

package com.deboxsoft.db;

import com.deboxsoft.app.Version;
import com.deboxsoft.db.api.Database;
import com.deboxsoft.db.api.DatabaseSupplier;
import com.deboxsoft.db.api.DatabaseSupportLevel;
import com.deboxsoft.db.api.DetailedDatabase;
import com.deboxsoft.db.exception.UnsupportedDatabaseException;
import com.deboxsoft.utils.base.NotNull;
import com.deboxsoft.utils.concurrent.ResettableLazyReference;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DefaultDatabaseSupplier implements DatabaseSupplier {
    private static final Pattern PATTERN_ORACLE_VERSION = Pattern.compile(".+\\s([\\d\\.]+)\\s.*");
    private static final Logger logger = LoggerFactory.getLogger(DefaultDatabaseSupplier.class);
    private final ResettableLazyReference<DetailedDatabase> dbx_dbReference;
    private static final Map<String, Function<Database, DatabaseSupportLevel>> NAMES_TO_SUPPORT_LEVELS = ImmutableMap.<String, Function<Database, DatabaseSupportLevel>>builder().
            put(Database.HSQL, new HsqlSupportLevel()).
            put(Database.H2, new H2SupportLevel()).
            put(Database.SQL_SERVER, new SqlServerSupportLevel()).
            put(Database.MYSQL, new MySqlSupportLevel()).
            put(Database.ORACLE, new OracleSupportLevel()).
            put(Database.POSTGRESQL, new PostgresSupportLevel()).build();
    private boolean dbx_ignoreUnsupported;

    public DefaultDatabaseSupplier(final DataSource dbx_dataSource) {
        if (logger.isTraceEnabled()) logger.trace("initial DefaultDatabaseSupplier");
        this.dbx_dbReference = new ResettableLazyReference<DetailedDatabase>() {
            @Override
            protected DetailedDatabase create() {
                logger.debug("create detailDatabase from datasource : {}", dbx_dataSource.toString());
                return DefaultDatabaseSupplier.detailsFor(dbx_dataSource);
            }
        };
    }

    @Value("${jdbc.ignoreunsupported}")
    public void setIgnoreUnsupported(final boolean dbx_ignoreUnsupported) {
        this.dbx_ignoreUnsupported = dbx_ignoreUnsupported;
    }

    @PostConstruct
    public void validate() {
        if (this.dbx_ignoreUnsupported) {
            logger.warn("Not checking database support level; the check has been disabled");
            return;
        }
        DetailedDatabase database = get();

        if (DatabaseSupportLevel.UNSUPPORTED == database.getSupportLevel())
            throw new UnsupportedDatabaseException("The configured database is unsupported", database);
    }

    @NotNull
    @Override
    public DetailedDatabase getForConnection(@NotNull final Connection paramConnection) {
        return detailsFor(databaseFor(paramConnection));
    }

    @Override
    public DetailedDatabase get() {
        return dbx_dbReference.get();
    }

    @NotNull
    private static DetailedDatabase detailsFor(final Database dbx_database) {
        logger.debug("- instance detailedDatabase {}!!", dbx_database.getName());
        return new DelegatingDetailDatabase(dbx_database, supportFor(dbx_database));
    }

    @NotNull
    private static DetailedDatabase detailsFor(final DataSource dbx_dataSource) {
        return detailsFor(databaseFor(dbx_dataSource));
    }

    @NotNull
    private static Database databaseFor(final DataSource dbx_dataSource) {
        Connection dbx_connection = null;
        try {
            if (logger.isDebugEnabled()) logger.debug("- datasource to database");
            dbx_connection = DataSourceUtils.getConnection(dbx_dataSource);
            return databaseFor(dbx_connection);
        } finally {
            DataSourceUtils.releaseConnection(dbx_connection, dbx_dataSource);
        }
    }

    @NotNull
    private static Database databaseFor(final Connection dbx_connection) {
        try {
            DatabaseMetaData dbx_metaData = dbx_connection.getMetaData();
            String dbx_productName = dbx_metaData.getDatabaseProductName();
            String dbx_productVersion = dbx_metaData.getDatabaseProductVersion();
            int dbx_majorVersion = dbx_metaData.getDatabaseMajorVersion();
            int dbx_minorVersion = dbx_metaData.getDatabaseMinorVersion();
            Version dbx_version = parseVersion(dbx_productName, dbx_productVersion, dbx_majorVersion, dbx_minorVersion);
            return new JdbcMetadataDatabase(dbx_productName, dbx_version, dbx_majorVersion, dbx_minorVersion);
        } catch (SQLException e) {
            throw new DataRetrievalFailureException("Tidak dapat load database metadata", e);
        }
    }

    private static Version parseVersion(final String dbx_productName, final String dbx_productVersion, final int dbx_majorVersion, final int dbx_minorVersion) {
        if (Database.ORACLE.equals(dbx_productName)) {
            Matcher matcher = PATTERN_ORACLE_VERSION.matcher(dbx_productVersion);
            if (matcher.matches()) {
                return new Version(matcher.group(1));
            }

            logger.warn("Could not parse Oracle version [{}]; using major and minor versions", dbx_productVersion);
            return new Version(new Integer[]{dbx_majorVersion, dbx_minorVersion});
        }
        return new Version(dbx_productVersion);
    }


    private static DatabaseSupportLevel supportFor(final Database dbx_database) {
        Function<Database, DatabaseSupportLevel> dbx_supportLevel = NAMES_TO_SUPPORT_LEVELS.get(dbx_database.getName());
        if (dbx_supportLevel == null) {
            logger.error("{} is not supported database", dbx_database.getName());
            return DatabaseSupportLevel.UNSUPPORTED;
        }
        return dbx_supportLevel.apply(dbx_database);
    }

    private static class HsqlSupportLevel implements Function<Database, DatabaseSupportLevel> {
        @Override
        public DatabaseSupportLevel apply(final Database dbx_database) {
            return DatabaseSupportLevel.SUPPORTED;
        }
    }

    private static class H2SupportLevel implements Function<Database, DatabaseSupportLevel> {
        @Override
        public DatabaseSupportLevel apply(final Database dbx_database) {
            return DatabaseSupportLevel.SUPPORTED;
        }
    }

    private static class SqlServerSupportLevel implements Function<Database, DatabaseSupportLevel> {
        @Override
        public DatabaseSupportLevel apply(final Database dbx_database) {
            return DatabaseSupportLevel.UNSUPPORTED;
        }
    }

    private static class MySqlSupportLevel implements Function<Database, DatabaseSupportLevel> {
        @Override
        public DatabaseSupportLevel apply(final Database dbx_database) {
            if (dbx_database.getMajorVersion() == 5) {
                return DatabaseSupportLevel.SUPPORTED;
            }
            return dbx_database.getMajorVersion() < 5 ? DatabaseSupportLevel.UNSUPPORTED : DatabaseSupportLevel.UNKNOWN;
        }
    }

    private static class OracleSupportLevel implements Function<Database, DatabaseSupportLevel> {
        @Override
        public DatabaseSupportLevel apply(final Database dbx_database) {
            return DatabaseSupportLevel.UNSUPPORTED;
        }
    }

    private static class PostgresSupportLevel implements Function<Database, DatabaseSupportLevel> {

        @Override
        public DatabaseSupportLevel apply(final Database dbx_database) {
            return DatabaseSupportLevel.UNSUPPORTED;
        }
    }
}
