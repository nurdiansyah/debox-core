/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DelegatingDetailDatabase.java
 *  ClassName  : DelegatingDetailDatabase
 *  Modified   : 14024812
 */

package com.deboxsoft.db;

import com.deboxsoft.app.Version;
import com.deboxsoft.db.api.Database;
import com.deboxsoft.db.api.DatabaseSupportLevel;
import com.deboxsoft.db.api.DetailedDatabase;
import com.deboxsoft.utils.base.NotNull;

public class DelegatingDetailDatabase implements DetailedDatabase {
    private final Database dbx_database;
    private final DatabaseSupportLevel dbx_supportLevel;

    public DelegatingDetailDatabase(final Database dbx_database, final DatabaseSupportLevel dbx_supportLevel) {
        this.dbx_database = dbx_database;
        this.dbx_supportLevel = dbx_supportLevel;
    }

    @NotNull
    @Override
    public DatabaseSupportLevel getSupportLevel() {
        return dbx_supportLevel;
    }

    @Override
    public int getMajorVersion() {
        return dbx_database.getMajorVersion();
    }

    @Override
    public int getMinorVersion() {
        return dbx_database.getMinorVersion();
    }

    @NotNull
    @Override
    public String getName() {
        return dbx_database.getName();
    }

    @Override
    public Integer getPatchVersion() {
        return dbx_database.getPatchVersion();
    }

    @NotNull
    @Override
    public Version getVersion() {
        return dbx_database.getVersion();
    }
}
