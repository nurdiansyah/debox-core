/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-db
 *  File       : ConfigurableHibernatePersistence.java
 *  ClassName  : ConfigurableHibernatePersistence
 *  Modified   : 14025421
 */

package com.deboxsoft.db.hibernate.jpa;

import org.hibernate.EmptyInterceptor;
import org.hibernate.Interceptor;
import org.hibernate.ejb.Ejb3Configuration;
import org.hibernate.ejb.HibernatePersistence;

import javax.persistence.EntityManagerFactory;
import javax.persistence.spi.PersistenceUnitInfo;
import java.util.Map;

public class ConfigurableHibernatePersistence extends HibernatePersistence {
    private Interceptor interceptor;

    @SuppressWarnings("deprecation")
    @Override
    public EntityManagerFactory createContainerEntityManagerFactory(final PersistenceUnitInfo info, final Map properties) {
        Ejb3Configuration dbx_configuration = new Ejb3Configuration();
        dbx_configuration = dbx_configuration.configure(info, properties);

        if (this.interceptor != null) {
            if (dbx_configuration.getInterceptor() == null || EmptyInterceptor.class.equals(dbx_configuration.getInterceptor().getClass())) {
                dbx_configuration.setInterceptor(this.interceptor);
            } else {
                throw new IllegalStateException("Hibernate interceptor already set in persistence.xml (" + dbx_configuration.getInterceptor() + ")");
            }
        }
        return super.createContainerEntityManagerFactory(info, properties);
    }



}
