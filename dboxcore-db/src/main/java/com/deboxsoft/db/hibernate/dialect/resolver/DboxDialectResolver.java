/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DboxDialexResolver.java
 *  ClassName  : DboxDialexResolver
 *  Modified   : 14024810
 */

package com.deboxsoft.db.hibernate.dialect.resolver;

import com.deboxsoft.db.api.Database;
import com.deboxsoft.db.hibernate.dialect.DboxMysql5Dialect;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.H2Dialect;
import org.hibernate.dialect.HSQLDialect;
import org.hibernate.dialect.MySQLDialect;
import org.hibernate.service.jdbc.dialect.internal.AbstractDialectResolver;

import java.sql.DatabaseMetaData;
import java.sql.SQLException;

public class DboxDialectResolver extends AbstractDialectResolver {
    private static final long serialVersionUID = -435626415557496621L;


    @Override
    protected Dialect resolveDialectInternal(final DatabaseMetaData metaData) throws SQLException {
        String dbx_databaseName = metaData.getDatabaseProductName();
        int dbx_databaseMajorVersion = metaData.getDatabaseMajorVersion();
        if (Database.MYSQL.equals(dbx_databaseName)) {
            if (dbx_databaseMajorVersion >= 5) {
                return new DboxMysql5Dialect();
            }
            return new MySQLDialect();
        }
        if (Database.HSQL.equals(dbx_databaseName)) {
            return new HSQLDialect();
        }

        if (Database.H2.equals(dbx_databaseName)) {
            return new H2Dialect();
        }
        return null;
    }
}
