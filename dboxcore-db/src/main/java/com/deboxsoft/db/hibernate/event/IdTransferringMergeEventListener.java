/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : IdTransferringMergeEventListener.java
 *  ClassName  : IdTransferringMergeEventListener
 *  Modified   : 14025109
 */

package com.deboxsoft.db.hibernate.event;

import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.event.internal.DefaultMergeEventListener;
import org.hibernate.event.spi.MergeEvent;
import org.hibernate.persister.entity.EntityPersister;

import java.util.Map;

public class IdTransferringMergeEventListener extends DefaultMergeEventListener {
    private static final long serialVersionUID = 2851117273469670424L;

    @Override
    protected void entityIsTransient(final MergeEvent event, final Map copyCache) {
        super.entityIsTransient(event, copyCache);
        SessionImplementor dbx_session = event.getSession();
        EntityPersister dbx_persister = dbx_session.getEntityPersister(event.getEntityName(), event.getEntity());
        dbx_persister.setIdentifier(event.getOriginal(), dbx_persister.getIdentifier(event.getResult(), dbx_session), dbx_session);
    }
}
