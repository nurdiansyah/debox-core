/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DboxMysqlDialeg.java
 *  ClassName  : DboxMysqlDialeg
 *  Modified   : 14024810
 */

package com.deboxsoft.db.hibernate.dialect;

import org.hibernate.dialect.MySQL5InnoDBDialect;

import java.sql.Types;

public class DboxMysql5Dialect extends MySQL5InnoDBDialect {
    public DboxMysql5Dialect() {
        registerColumnType(Types.BOOLEAN, "bit");
    }
}
