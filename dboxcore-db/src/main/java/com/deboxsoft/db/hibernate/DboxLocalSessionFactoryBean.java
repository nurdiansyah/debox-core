/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DboxLocalFactoryBean.java
 *  ClassName  : DboxLocalFactoryBean
 *  Modified   : 14025012
 */

package com.deboxsoft.db.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.event.service.spi.DuplicationStrategy;
import org.hibernate.event.service.spi.EventListenerGroup;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.service.spi.ServiceRegistryImplementor;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.util.CollectionUtils;

import javax.sql.DataSource;
import javax.validation.ValidatorFactory;
import java.util.Collection;
import java.util.Map;
import java.util.Properties;

public class DboxLocalSessionFactoryBean extends LocalSessionFactoryBean {
    private final static DuplicationStrategy DUPLICATION_STRATEGY = new DuplicationStrategy() {
        @Override
        public boolean areMatch(final Object listener, final Object original) {
            Class<?> dbx_OriClass = original.getClass();
            Class<?> dbx_listenerClass = listener.getClass();
            return (!dbx_listenerClass.equals(dbx_OriClass)) && (dbx_OriClass.isInstance(listener));
        }

        @Override
        public Action getAction() {
            return Action.REPLACE_ORIGINAL;
        }
    };
    private Map<EventType<Object>, Object> dbx_eventListeners;
    private ValidatorFactory dbx_validatorFactory;

    public DboxLocalSessionFactoryBean(DataSource dbx_dataSource) {
        this.setDataSource(dbx_dataSource);
    }

    @Override
    public Class<?> getObjectType() {
        return SessionFactoryImplementor.class;
    }

    @Override
    protected SessionFactory buildSessionFactory(final LocalSessionFactoryBuilder sfb) {
        if (dbx_validatorFactory != null) {
            Properties dbx_validatorProperties = new Properties();
            dbx_validatorProperties.put("javax.persistence.validation.factory", dbx_validatorFactory);
            sfb.addProperties(dbx_validatorProperties);
        }

        SessionFactoryImplementor dbx_sessionFactory = (SessionFactoryImplementor) super.buildSessionFactory(sfb);
        registerEventListener(dbx_sessionFactory);
        return dbx_sessionFactory;
    }

    private void registerEventListener(final SessionFactoryImplementor dbx_sessionFactory) {
        if (!CollectionUtils.isEmpty(dbx_eventListeners)) {
            EventListenerRegistry dbx_listenerRegistry;
            ServiceRegistryImplementor dbx_registryImplementor = dbx_sessionFactory.getServiceRegistry();
            dbx_listenerRegistry = dbx_registryImplementor.getService(EventListenerRegistry.class);
            for (Map.Entry<EventType<Object>, Object> dbx_entry : dbx_eventListeners.entrySet()) {
                EventListenerGroup<Object> dbx_group = dbx_listenerRegistry.getEventListenerGroup(dbx_entry.getKey());
                dbx_group.addDuplicationStrategy(DUPLICATION_STRATEGY);

                Object value = dbx_entry.getValue();
                if (value instanceof Collection) {
                    dbx_group.appendListeners(((Collection) value).toArray());
                } else {
                    dbx_group.appendListener(value);
                }
            }
        }
    }

    public void setEventListeners(final Map<EventType<Object>, Object> dbx_eventListeners) {
        this.dbx_eventListeners = dbx_eventListeners;
    }

    public void setValidatorFactory(final ValidatorFactory dbx_validatorFactory) {
        this.dbx_validatorFactory = dbx_validatorFactory;
    }
}
