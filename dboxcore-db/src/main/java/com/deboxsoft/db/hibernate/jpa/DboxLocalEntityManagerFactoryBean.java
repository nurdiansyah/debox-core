/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-db
 *  File       : DboxLocalEntityManagerFactoryBean.java
 *  ClassName  : DboxLocalEntityManagerFactoryBean
 *  Modified   : 14025411
 */

package com.deboxsoft.db.hibernate.jpa;

import org.hibernate.ejb.EntityManagerFactoryImpl;
import org.hibernate.event.service.spi.DuplicationStrategy;
import org.hibernate.event.service.spi.EventListenerGroup;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.service.spi.ServiceRegistryImplementor;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Collection;
import java.util.Map;

public class DboxLocalEntityManagerFactoryBean extends LocalContainerEntityManagerFactoryBean {
    private final static DuplicationStrategy DUPLICATION_STRATEGY = new DuplicationStrategy() {
        @Override
        public boolean areMatch(final Object listener, final Object original) {
            Class<?> dbx_OriClass = original.getClass();
            Class<?> dbx_listenerClass = listener.getClass();
            return (!dbx_listenerClass.equals(dbx_OriClass)) && (dbx_OriClass.isInstance(listener));
        }

        @Override
        public Action getAction() {
            return Action.REPLACE_ORIGINAL;
        }
    };
    private Map<EventType<Object>, Object> dbx_eventListeners;

    public DboxLocalEntityManagerFactoryBean(DataSource dbx_dataSource) {
        setDataSource(dbx_dataSource);
    }

    @Override
    public Class<? extends EntityManagerFactory> getObjectType() {
        return EntityManagerFactoryImpl.class;
    }


    @Override
    protected EntityManagerFactory createNativeEntityManagerFactory() {
        EntityManagerFactoryImpl dbx_entityManagerFactory = (EntityManagerFactoryImpl) super.createNativeEntityManagerFactory();
        registerEventListener(dbx_entityManagerFactory);
        return dbx_entityManagerFactory;
    }

    private void registerEventListener(final EntityManagerFactoryImpl dbx_emfi) {
        if (!CollectionUtils.isEmpty(dbx_eventListeners)) {
            EventListenerRegistry dbx_listenerRegistry;
            ServiceRegistryImplementor dbx_registryImplementor = dbx_emfi.getSessionFactory().getServiceRegistry();
            dbx_listenerRegistry = dbx_registryImplementor.getService(EventListenerRegistry.class);
            for (Map.Entry<EventType<Object>, Object> dbx_entry : dbx_eventListeners.entrySet()) {
                EventListenerGroup<Object> dbx_group = dbx_listenerRegistry.getEventListenerGroup(dbx_entry.getKey());
                dbx_group.addDuplicationStrategy(DUPLICATION_STRATEGY);

                Object value = dbx_entry.getValue();
                if (value instanceof Collection) {
                    dbx_group.appendListeners(((Collection) value).toArray());
                } else {
                    dbx_group.appendListener(value);
                }
            }
        }
    }

    public void setEventListeners(final Map<EventType<Object>, Object> dbx_eventListeners) {
        this.dbx_eventListeners = dbx_eventListeners;
    }

}
