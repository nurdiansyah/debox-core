/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-db
 *  File       : NoEmptyStringsInterceptor.java
 *  ClassName  : NoEmptyStringsInterceptor
 *  Modified   : 14025112
 */

package com.deboxsoft.db.hibernate;

import org.hibernate.CallbackException;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

import java.io.Serializable;

public class NoEmptyStringsInterceptor extends EmptyInterceptor {
    private static final long serialVersionUID = -2120493056034741974L;

    @Override
    public boolean onFlushDirty(final Object entity, final Serializable id, final Object[] currentState, final Object[] previousState, final String[] propertyNames, final Type[] types) {
        checkStrings(entity, id, currentState, propertyNames);
        return false;
    }

    @Override
    public boolean onSave(final Object entity, final Serializable id, final Object[] state, final String[] propertyNames, final Type[] types) {
        checkStrings(entity, id, state, propertyNames);
        return false;
    }

    private static void checkStrings(final Object dbx_entity, final Serializable dbx_id, final Object[] dbx_state, final String[] dbx_propertyNames) {
        StringBuilder errors = null;
        if (dbx_entity.getClass().getPackage().getName().startsWith("com.deboxsoft")) {
            for (int i = 0; i < dbx_state.length; i++) {
                if ("".equals(dbx_state[i])) {
                    if (errors == null) {
                        errors = new StringBuilder().append(dbx_entity.getClass().getName()).append(" with id '").append(dbx_id).append("' has empty string properties: [");
                    } else {
                        errors.append(", ");
                    }
                    errors.append(dbx_propertyNames[i]);
                }
            }
        }

        if (errors != null) {
            errors.append("]");
            throw new CallbackException(errors.toString());
        }
    }
}