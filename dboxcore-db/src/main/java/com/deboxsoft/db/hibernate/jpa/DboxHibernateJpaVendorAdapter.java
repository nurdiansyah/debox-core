/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-db
 *  File       : DboxHibernateJpaVendorAdapter.java
 *  ClassName  : DboxHibernateJpaVendorAdapter
 *  Modified   : 14025423
 */

package com.deboxsoft.db.hibernate.jpa;

import com.deboxsoft.db.hibernate.dialect.resolver.DboxDialectResolver;
import org.hibernate.cfg.Environment;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import java.util.HashMap;
import java.util.Map;

public class DboxHibernateJpaVendorAdapter extends HibernateJpaVendorAdapter {

    @Override
    public Map<String, Object> getJpaPropertyMap() {
        Map<String, Object> jpaProperties = new HashMap<>();
        if (getDatabasePlatform() != null) {
            jpaProperties.put(Environment.DIALECT, getDatabasePlatform());
        } else if (getDatabase() != null) {
            Class databaseDialectClass = determineDatabaseDialectClass(getDatabase());
            if (databaseDialectClass != null) {
                jpaProperties.put(Environment.DIALECT, databaseDialectClass.getName());
            }
        }else {
            jpaProperties.put(Environment.DIALECT, DboxDialectResolver.class);
        }

        if (isGenerateDdl()) {
            jpaProperties.put(Environment.HBM2DDL_AUTO, "update");
        }
        if (isShowSql()) {
            jpaProperties.put(Environment.SHOW_SQL, "true");
        }
        jpaProperties.put(Environment.DIALECT_RESOLVERS, DboxDialectResolver.class.getName());
        return jpaProperties;
    }
}
