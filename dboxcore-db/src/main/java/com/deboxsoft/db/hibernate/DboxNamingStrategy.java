/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-db
 *  File       : DboxNamingStrategy.java
 *  ClassName  : DboxNamingStrategy
 *  Modified   : 14025514
 */

package com.deboxsoft.db.hibernate;

import org.hibernate.cfg.ImprovedNamingStrategy;

public class DboxNamingStrategy extends ImprovedNamingStrategy {
    private static final long serialVersionUID = 2483678364823463711L;
    private static final String PREFIX_TABLE = "DBX_";

    @Override
    public String classToTableName(String className) {
        className = className.replace("Entity", "");
        return PREFIX_TABLE + tableName(className);
    }

    @Override
    public String propertyToColumnName(String propertyName) {
        propertyName = propertyName.replaceAll("([A-Z])", "_$1");
        return (propertyName).toUpperCase();
    }

    @Override
    public String tableName(String tableName) {
        return addUnderscores(tableName).toUpperCase();
    }

    @Override
    public String columnName(String columnName) {
        columnName = columnName.replaceAll("([A-Z])", "_$1");
        return  (columnName).toUpperCase();
    }

    @Override
    public String foreignKeyColumnName(String propertyName, String propertyEntityName, String propertyTableName, String referencedColumnName) {
        propertyTableName = propertyTableName.replace("Entity", "").replace(PREFIX_TABLE, "");
        return propertyTableName.toUpperCase() + "_" + referencedColumnName.toUpperCase();
    }

    @Override
    public String collectionTableName(String ownerEntity, String ownerEntityTable, String associatedEntity, String associatedEntityTable, String propertyName) {
        return "DBX_" + ownerEntityTable.toUpperCase() + "__" + associatedEntityTable.toUpperCase();
    }


}
