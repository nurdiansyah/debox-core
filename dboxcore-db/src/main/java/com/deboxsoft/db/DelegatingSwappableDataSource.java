/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DelegatingSwappableDataSource.java
 *  ClassName  : DelegatingSwappableDataSource
 *  Modified   : 14024721
 */

package com.deboxsoft.db;

import com.deboxsoft.db.api.SwappableDataSource;
import com.deboxsoft.utils.base.NotNull;
import com.google.common.io.Closeables;
import org.slf4j.LoggerFactory;
import org.springframework.core.InfrastructureProxy;

import javax.annotation.PreDestroy;
import javax.sql.DataSource;
import java.io.Closeable;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

public class DelegatingSwappableDataSource implements SwappableDataSource, Closeable{
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DelegatingSwappableDataSource.class);
    private volatile DataSource dbx_delegate;

    public DelegatingSwappableDataSource(final DataSource dbx_delegate) {
        this.dbx_delegate = dbx_delegate;
    }
    @PreDestroy
    @Override
    public void close() throws IOException {
        if (dbx_delegate instanceof Closeable) Closeables.close((Closeable) dbx_delegate, true);
    }

    /**
     * switching dataSource
     * @param dbx_target
     * @return
     */
    @NotNull
    @Override
    public DataSource swap(@NotNull final DataSource dbx_target) {
        DataSource bak = dbx_delegate;
        this.dbx_delegate = dbx_target;
        return bak;
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (logger.isDebugEnabled()) logger.debug("mencoba utk koneksi dg spesifik : {} ", dbx_delegate.toString());
        return dbx_delegate.getConnection();
    }

    @Override
    public Connection getConnection(final String username, final String password) throws SQLException {
        if (logger.isDebugEnabled()) logger.debug("getConnection(username, password)");
        return dbx_delegate.getConnection(username, password);
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return dbx_delegate.getLogWriter();
    }

    @Override
    public void setLogWriter(final PrintWriter out) throws SQLException {
        dbx_delegate.setLogWriter(out);
    }

    @Override
    public void setLoginTimeout(final int seconds) throws SQLException {
        dbx_delegate.setLoginTimeout(seconds);
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return dbx_delegate.getLoginTimeout();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return dbx_delegate.getParentLogger();
    }

    @Override
    public Object getWrappedObject() {
        DataSource dbx_delegate = this.dbx_delegate;
        return dbx_delegate instanceof InfrastructureProxy ?((InfrastructureProxy) dbx_delegate).getWrappedObject():dbx_delegate;
    }

    @Override
    public <T> T unwrap(final Class<T> iface) throws SQLException {
        return dbx_delegate.unwrap(iface);
    }

    @Override
    public boolean isWrapperFor(final Class<?> iface) throws SQLException {
        return dbx_delegate.isWrapperFor(iface);
    }
}
