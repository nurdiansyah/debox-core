/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-db
 *  File       : DelegatingSwappableEntityManager.java
 *  ClassName  : DelegatingSwappableEntityManager
 *  Modified   : 14025508
 */

package com.deboxsoft.db;

import com.deboxsoft.db.api.SwappableEntityManagerFactory;
import com.deboxsoft.utils.base.NotNull;
import org.springframework.core.InfrastructureProxy;

import javax.persistence.Cache;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnitUtil;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.metamodel.Metamodel;
import java.util.Map;

public class DelegatingSwappableEntityManager implements SwappableEntityManagerFactory {


    private EntityManagerFactory dbx_entityManagerFactory;

    public DelegatingSwappableEntityManager(EntityManagerFactory dbx_entityManagerFactory) {
        this.dbx_entityManagerFactory = dbx_entityManagerFactory;
    }


    @NotNull
    @Override
    public EntityManagerFactory swap(@NotNull final EntityManagerFactory dbx_target) {
        EntityManagerFactory tmp = dbx_entityManagerFactory;
        dbx_entityManagerFactory = dbx_target;
        return tmp;
    }

    @Override
    public EntityManager createEntityManager() {
        return dbx_entityManagerFactory.createEntityManager();
    }

    @Override
    public EntityManager createEntityManager(final Map map) {
        return dbx_entityManagerFactory.createEntityManager(map);
    }

    @Override
    public CriteriaBuilder getCriteriaBuilder() {
        return dbx_entityManagerFactory.getCriteriaBuilder();
    }

    @Override
    public Metamodel getMetamodel() {
        return dbx_entityManagerFactory.getMetamodel();
    }

    @Override
    public boolean isOpen() {
        return dbx_entityManagerFactory.isOpen();
    }

    @Override
    public void close() {
        dbx_entityManagerFactory.close();
    }

    @Override
    public Map<String, Object> getProperties() {
        return dbx_entityManagerFactory.getProperties();
    }

    @Override
    public Cache getCache() {
        return dbx_entityManagerFactory.getCache();
    }

    @Override
    public PersistenceUnitUtil getPersistenceUnitUtil() {
        return dbx_entityManagerFactory.getPersistenceUnitUtil();
    }

    @Override
    public Object getWrappedObject() {
        return dbx_entityManagerFactory instanceof InfrastructureProxy?
                ((InfrastructureProxy)dbx_entityManagerFactory).getWrappedObject() :
                dbx_entityManagerFactory;
    }
}
