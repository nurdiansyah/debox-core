/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : DelegatingSwappableSessionFactory.java
 *  ClassName  : DelegatingSwappableSessionFactory
 *  Modified   : 14025018
 */

package com.deboxsoft.db;

import com.deboxsoft.db.api.SwappableSessionFactory;
import com.deboxsoft.utils.base.NotNull;
import org.hibernate.*;
import org.hibernate.cache.spi.QueryCache;
import org.hibernate.cache.spi.Region;
import org.hibernate.cache.spi.UpdateTimestampsCache;
import org.hibernate.cfg.Settings;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.function.SQLFunctionRegistry;
import org.hibernate.engine.ResultSetMappingDefinition;
import org.hibernate.engine.jdbc.spi.JdbcServices;
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.hibernate.engine.profile.FetchProfile;
import org.hibernate.engine.query.spi.QueryPlanCache;
import org.hibernate.engine.spi.*;
import org.hibernate.exception.spi.SQLExceptionConverter;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.id.factory.IdentifierGeneratorFactory;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.metadata.CollectionMetadata;
import org.hibernate.persister.collection.CollectionPersister;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.proxy.EntityNotFoundDelegate;
import org.hibernate.service.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.service.spi.ServiceRegistryImplementor;
import org.hibernate.stat.Statistics;
import org.hibernate.stat.spi.StatisticsImplementor;
import org.hibernate.type.Type;
import org.hibernate.type.TypeResolver;
import org.springframework.core.InfrastructureProxy;

import javax.annotation.PreDestroy;
import javax.naming.NamingException;
import javax.naming.Reference;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@SuppressWarnings("deprecation")
public class DelegatingSwappableSessionFactory implements SwappableSessionFactory {
    private static final long serialVersionUID = -3648647109160298067L;

    private volatile SessionFactoryImplementor dbx_delegate;

    public DelegatingSwappableSessionFactory(final SessionFactoryImplementor dbx_delegate) {
        this.dbx_delegate = dbx_delegate;
    }

    public void addObserver(SessionFactoryObserver dbx_observer) {
        dbx_delegate.addObserver(dbx_observer);
    }

    @Override
    public CustomEntityDirtinessStrategy getCustomEntityDirtinessStrategy() {
        return dbx_delegate.getCustomEntityDirtinessStrategy();
    }

    @Override
    public CurrentTenantIdentifierResolver getCurrentTenantIdentifierResolver() {
        return dbx_delegate.getCurrentTenantIdentifierResolver();
    }

    @Override
    public IdentifierGeneratorFactory getIdentifierGeneratorFactory() {
        return dbx_delegate.getIdentifierGeneratorFactory();
    }

    @Override
    public Type getIdentifierType(final String className) throws MappingException {
        return dbx_delegate.getIdentifierType(className);
    }

    @Override
    public String getIdentifierPropertyName(final String className) throws MappingException {
        return dbx_delegate.getIdentifierPropertyName(className);
    }

    @Override
    public Type getReferencedPropertyType(final String className, final String propertyName) throws MappingException {
        return dbx_delegate.getReferencedPropertyType(className, propertyName);
    }

    @Override
    public SessionFactoryOptions getSessionFactoryOptions() {
        return dbx_delegate.getSessionFactoryOptions();
    }

    @Override
    public Session openSession() throws HibernateException {
        return dbx_delegate.openSession();
    }

    @Override
    public Session getCurrentSession() throws HibernateException {
        return dbx_delegate.getCurrentSession();
    }

    @Override
    public StatelessSessionBuilder withStatelessOptions() {
        return dbx_delegate.withStatelessOptions();
    }

    @Override
    public StatelessSession openStatelessSession() {
        return dbx_delegate.openStatelessSession();
    }

    @Override
    public StatelessSession openStatelessSession(final Connection connection) {
        return dbx_delegate.openStatelessSession(connection);
    }

    @Override
    public ClassMetadata getClassMetadata(final Class entityClass) {
        return dbx_delegate.getClassMetadata(entityClass);
    }

    @Override
    public ClassMetadata getClassMetadata(final String entityName) {
        return dbx_delegate.getClassMetadata(entityName);
    }

    @Override
    public CollectionMetadata getCollectionMetadata(final String roleName) {
        return dbx_delegate.getCollectionMetadata(roleName);
    }

    @Override
    public Map<String, ClassMetadata> getAllClassMetadata() {
        return dbx_delegate.getAllClassMetadata();
    }

    @Override
    public Map getAllCollectionMetadata() {
        return dbx_delegate.getAllCollectionMetadata();
    }

    @Override
    public Statistics getStatistics() {
        return dbx_delegate.getStatistics();
    }

    @PreDestroy
    @Override
    public void close() throws HibernateException {
        dbx_delegate.close();
    }

    @Override
    public boolean isClosed() {
        return dbx_delegate.isClosed();
    }

    @Override
    public Cache getCache() {
        return dbx_delegate.getCache();
    }

    @Override
    public boolean containsFetchProfileDefinition(final String name) {
        return dbx_delegate.containsFetchProfileDefinition(name);
    }

    @Override
    public TypeHelper getTypeHelper() {
        return dbx_delegate.getTypeHelper();
    }

    @Override
    public Reference getReference() throws NamingException {
        return dbx_delegate.getReference();
    }

    @Deprecated
    @Override
    public void evict(final Class persistentClass) throws HibernateException {
        dbx_delegate.evict(persistentClass);
    }

    @Deprecated
    @Override
    public void evict(final Class persistentClass, final Serializable id) throws HibernateException {
        dbx_delegate.evict(persistentClass, id);
    }

    @Deprecated
    @Override
    public void evictEntity(final String entityName) throws HibernateException {
        dbx_delegate.evictEntity(entityName);
    }

    @Deprecated
    @Override
    public void evictEntity(final String entityName, final Serializable id) throws HibernateException {
        dbx_delegate.evictEntity(entityName, id);
    }

    @Deprecated
    @Override
    public void evictCollection(final String roleName) throws HibernateException {
        dbx_delegate.evictCollection(roleName);
    }

    @Deprecated
    @Override
    public void evictCollection(final String roleName, final Serializable id) throws HibernateException {
        dbx_delegate.evictCollection(roleName, id);
    }

    @Deprecated
    @Override
    public void evictQueries(final String cacheRegion) throws HibernateException {
        dbx_delegate.evictQueries(cacheRegion);
    }

    @Deprecated
    @Override
    public void evictQueries() throws HibernateException {
        dbx_delegate.evictQueries();
    }

    @Override
    public Set getDefinedFilterNames() {
        return dbx_delegate.getDefinedFilterNames();
    }

    @Override
    public FilterDefinition getFilterDefinition(final String filterName) throws HibernateException {
        return dbx_delegate.getFilterDefinition(filterName);
    }

    @NotNull
    @Override
    public SessionFactoryImplementor swap(@NotNull final SessionFactoryImplementor dbx_target) {
        SessionFactoryImplementor dbx_sessionFactoryOld = dbx_delegate;
        dbx_delegate = dbx_target;
        return dbx_sessionFactoryOld;
    }

    @Override
    public SessionBuilderImplementor withOptions() {
        return dbx_delegate.withOptions();
    }

    @Override
    public TypeResolver getTypeResolver() {
        return dbx_delegate.getTypeResolver();
    }

    @Override
    public Properties getProperties() {
        return dbx_delegate.getProperties();
    }

    @Override
    public EntityPersister getEntityPersister(final String entityName) throws MappingException {
        return dbx_delegate.getEntityPersister(entityName);
    }

    @Override
    public Map<String, EntityPersister> getEntityPersisters() {
        return dbx_delegate.getEntityPersisters();
    }

    @Override
    public CollectionPersister getCollectionPersister(final String role) throws MappingException {
        return dbx_delegate.getCollectionPersister(role);
    }

    @Override
    public Map<String, CollectionPersister> getCollectionPersisters() {
        return dbx_delegate.getCollectionPersisters();
    }

    @Override
    public JdbcServices getJdbcServices() {
        return dbx_delegate.getJdbcServices();
    }

    @Override
    public Dialect getDialect() {
        return dbx_delegate.getDialect();
    }

    @Override
    public Interceptor getInterceptor() {
        return dbx_delegate.getInterceptor();
    }

    @Override
    public QueryPlanCache getQueryPlanCache() {
        return dbx_delegate.getQueryPlanCache();
    }

    @Override
    public Type[] getReturnTypes(final String queryString) throws HibernateException {
        return dbx_delegate.getReturnTypes(queryString);
    }

    @Override
    public String[] getReturnAliases(final String queryString) throws HibernateException {
        return dbx_delegate.getReturnAliases(queryString);
    }

    @Override
    @Deprecated
    public ConnectionProvider getConnectionProvider() {
        return dbx_delegate.getConnectionProvider();
    }

    @Override
    public String[] getImplementors(final String className) throws MappingException {
        return dbx_delegate.getImplementors(className);
    }

    @Override
    public String getImportedClassName(final String name) {
        return dbx_delegate.getImportedClassName(name);
    }

    @Override
    public QueryCache getQueryCache() {
        return dbx_delegate.getQueryCache();
    }

    @Override
    public QueryCache getQueryCache(final String regionName) throws HibernateException {
        return dbx_delegate.getQueryCache(regionName);
    }

    @Override
    public UpdateTimestampsCache getUpdateTimestampsCache() {
        return dbx_delegate.getUpdateTimestampsCache();
    }

    @Override
    public StatisticsImplementor getStatisticsImplementor() {
        return dbx_delegate.getStatisticsImplementor();
    }

    @Override
    public NamedQueryDefinition getNamedQuery(final String queryName) {
        return dbx_delegate.getNamedQuery(queryName);
    }

    @Override
    public NamedSQLQueryDefinition getNamedSQLQuery(final String queryName) {
        return dbx_delegate.getNamedSQLQuery(queryName);
    }

    @Override
    public ResultSetMappingDefinition getResultSetMapping(final String name) {
        return dbx_delegate.getResultSetMapping(name);
    }

    @Override
    public IdentifierGenerator getIdentifierGenerator(final String rootEntityName) {
        return dbx_delegate.getIdentifierGenerator(rootEntityName);
    }

    @Override
    public Region getSecondLevelCacheRegion(final String regionName) {
        return dbx_delegate.getSecondLevelCacheRegion(regionName);
    }

    @Override
    public Region getNaturalIdCacheRegion(final String regionName) {
        return dbx_delegate.getNaturalIdCacheRegion(regionName);
    }

    @Override
    public Map getAllSecondLevelCacheRegions() {
        return dbx_delegate.getAllSecondLevelCacheRegions();
    }

    @Override
    public SQLExceptionConverter getSQLExceptionConverter() {
        return dbx_delegate.getSQLExceptionConverter();
    }

    @Override
    public SqlExceptionHelper getSQLExceptionHelper() {
        return dbx_delegate.getSQLExceptionHelper();
    }

    @Override
    public Settings getSettings() {
        return dbx_delegate.getSettings();
    }

    @Override
    public Session openTemporarySession() throws HibernateException {
        return dbx_delegate.openTemporarySession();
    }

    @Override
    public Set<String> getCollectionRolesByEntityParticipant(final String entityName) {
        return dbx_delegate.getCollectionRolesByEntityParticipant(entityName);
    }

    @Override
    public EntityNotFoundDelegate getEntityNotFoundDelegate() {
        return dbx_delegate.getEntityNotFoundDelegate();
    }

    @Override
    public SQLFunctionRegistry getSqlFunctionRegistry() {
        return dbx_delegate.getSqlFunctionRegistry();
    }

    @Override
    public FetchProfile getFetchProfile(final String name) {
        return dbx_delegate.getFetchProfile(name);
    }

    @Override
    public ServiceRegistryImplementor getServiceRegistry() {
        return dbx_delegate.getServiceRegistry();
    }

    @Override
    public Object getWrappedObject() {
        return dbx_delegate instanceof InfrastructureProxy ? ((InfrastructureProxy) dbx_delegate).getWrappedObject() : dbx_delegate;
    }
}
