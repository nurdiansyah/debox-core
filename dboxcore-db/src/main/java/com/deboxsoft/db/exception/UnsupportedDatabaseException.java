/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-db
 *  File       : UnsupportedDatabaseException.java
 *  ClassName  : UnsupportedDatabaseException
 *  Modified   : 14024811
 */

package com.deboxsoft.db.exception;

import com.deboxsoft.db.api.Database;
import com.deboxsoft.utils.base.NotNull;
import com.google.common.base.Preconditions;

public class UnsupportedDatabaseException extends UnsupportedOperationException {
    private static final long serialVersionUID = 5579053896565676780L;

    private final Database database;

    public UnsupportedDatabaseException(@NotNull String message, @NotNull Database database) {
        super(Preconditions.checkNotNull(message, "message"));

        this.database = Preconditions.checkNotNull(database, "database");
    }

    @NotNull
    public Database getDatabase() {
        return this.database;
    }
}
