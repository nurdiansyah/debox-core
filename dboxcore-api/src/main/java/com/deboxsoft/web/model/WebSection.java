package com.deboxsoft.web.model;

public interface WebSection extends WebFragment, WebOrder, WebCondition {
    String getLocation();
}
