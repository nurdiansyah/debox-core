package com.deboxsoft.web.model;

import com.deboxsoft.web.conditions.Condition;

public interface WebFragment {
    String getKey();

    int getOrder();

    WebLabel getWebLabel();

    WebLabel getTooltip();

    Condition getCondition();

    WebParam getWebParams();
}
