/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-plugin
 *  Module     : dboxplugin-web-fragment
 *  File       : WebLabel.java
 *  ClassName  : WebLabel
 *  Modified   : 140410512
 */

package com.deboxsoft.web.model;

import java.util.Map;

public interface WebLabel extends WebParam {
    String getKey();

    String getNoKeyValue();

    String getDisplayableLabel(Map<String, Object> context);
}
