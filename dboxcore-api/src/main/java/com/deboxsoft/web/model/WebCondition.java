package com.deboxsoft.web.model;

import com.deboxsoft.web.conditions.Condition;

public interface WebCondition {
    Condition getCondition();
}
