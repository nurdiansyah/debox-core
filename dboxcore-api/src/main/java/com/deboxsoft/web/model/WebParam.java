/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-plugin
 *  Module     : dboxplugin-web-fragment
 *  File       : WebParam.java
 *  ClassName  : WebParam
 *  Modified   : 140410512
 */

package com.deboxsoft.web.model;

import java.util.Map;
import java.util.SortedMap;

/**
 * Represents arbitrary number of key/value pairs
 */
public interface WebParam {
    SortedMap<String, String> getParams();

    Object get(String key);

    String getRenderedParam(String paramKey, Map<String, Object> context);

}
