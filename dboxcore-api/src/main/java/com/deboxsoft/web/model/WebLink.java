package com.deboxsoft.web.model;

import java.util.Map;

public interface WebLink {
    String getRenderedUrl(Map<String, Object> contextMap);

    String getDisplayableUrl(String basePath, Map<String, Object> contextMap);

    boolean hasAccessKey();

    String getAccessKey(Map<String, Object> context);
}
