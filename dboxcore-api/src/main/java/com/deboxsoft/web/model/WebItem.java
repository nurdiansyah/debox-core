/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-api
 *  File       : MenuItem.java
 *  ClassName  : MenuItem
 *  Modified   : 140513111
 */

package com.deboxsoft.web.model;

public interface WebItem extends WebFragment, WebOrder, WebCondition {
    WebLink getLink();
    WebIcon getIcon();
    String getStyleClass();
}
