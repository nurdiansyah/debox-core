package com.deboxsoft.web.model;

public interface WebIcon {
    WebLink getUrl();
    int getWidth();
    int getHeight();
}
