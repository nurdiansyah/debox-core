/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-plugin
 *  Module     : dboxplugin-web-fragment
 *  File       : CompositeCondition.java
 *  ClassName  : CompositeCondition
 *  Modified   : 140410511
 */

package com.deboxsoft.web.conditions;

/**
 * Interface for composite conditions
 */
public interface CompositeCondition<T extends Condition> extends Condition {
    public void addCondition(T condition);
}
