/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-plugin
 *  Module     : dboxplugin-web-fragment
 *  File       : BaseCondition.java
 *  ClassName  : BaseCondition
 *  Modified   : 140410511
 */

package com.deboxsoft.web.conditions;

import java.util.Map;

/**
 * Marker interface for conditions
 */
public interface Condition {
    /**
     * Determine whether the web fragment should be displayed
     *
     * @return true if the user should see the fragment, false otherwise
     */
    boolean canDisplay(Map<String, Object> context);
}
