/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-plugin
 *  Module     : dboxplugin-web-fragment
 *  File       : WebInterfaceManager.java
 *  ClassName  : WebInterfaceManager
 *  Modified   : 140410511
 */

package com.deboxsoft.web;

import com.deboxsoft.web.model.WebItem;
import com.deboxsoft.web.model.WebPanel;
import com.deboxsoft.web.model.WebSection;

import java.util.List;
import java.util.Map;

/**
 * A simple manager to provide sections of the web interface through plugins.
 */
public interface WebInterfaceManager {

    /**
     * @return True if there are any sections for the given location.
     */
    boolean hasSectionsForLocation(String location);

    /**
     * @return A list of all WebSectionModuleDescriptors for the given location.
     */
    List<WebSection> getSections(String location);

    /**
     * @return A list of all AbstractWebLinkFragmentModuleDescriptor <i>viewable in a given context</i> in the given location.
     */
    List<WebSection> getDisplayableSections(String location, Map<String, Object> context);

    /**
     * @return A list of all WebItemModuleDescriptors for the given section.
     */
    List<WebItem> getItems(String section);

    /**
     * @return A list of all AbstractWebLinkFragmentModuleDescriptor <i>viewable in a given context</i> in the given section.
     */
    List<WebItem> getDisplayableItems(String section, Map<String, Object> context);

    /**
     * @param location
     * @return A list of all {@link WebPanel} module instances
     * for the given location.
     */
    List<WebPanel> getWebPanels(String location);

    /**
     * @param location
     * @param context
     * @return A list of all {@link WebPanel} module instances
     * <i>viewable in a given context</i> in the given location.
     */
    List<WebPanel> getDisplayableWebPanels(String location, Map<String, Object> context);

}
