/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-api
 *  File       : MenuSection.java
 *  ClassName  : MenuSection
 *  Modified   : 140513111
 */

package com.deboxsoft.web.menus;

import com.deboxsoft.web.model.WebItem;

import java.util.List;

public interface MenuSection {
    String getId();
    String getClassName();
    String getLabel();
    List<WebItem> getMenuItems();
}
