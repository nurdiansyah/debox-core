/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-platform-plugin
 *  File       : MenuManager.java
 *  ClassName  : MenuManager
 *  Modified   : 140512809
 */

package com.deboxsoft.web.menus;

import com.deboxsoft.web.WebInterfaceContext;

import java.util.List;

public interface MenuManager {
    Menu getMenu(String id, String menuKey);
    Menu getMenu(String id, List<String> menuKeys);
    Menu getMenu(String id, String menuKey, WebInterfaceContext context);
    Menu getMenu(String id, List<String> menuKeys, WebInterfaceContext context);
}
