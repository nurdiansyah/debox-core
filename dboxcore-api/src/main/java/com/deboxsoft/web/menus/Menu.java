/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-api
 *  File       : WebMenu.java
 *  ClassName  : WebMenu
 *  Modified   : 140513111
 */

package com.deboxsoft.web.menus;

import com.deboxsoft.web.model.WebItem;

import java.util.List;
import java.util.Map;

public interface Menu {
    String getId();
    boolean isEmpty( );
    List<MenuSection> getSections();
    void addSection(String sectionName, String label, List<WebItem> items, Map<String, String> params);
}
