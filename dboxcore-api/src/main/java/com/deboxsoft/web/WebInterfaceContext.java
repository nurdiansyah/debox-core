package com.deboxsoft.web;

import com.deboxsoft.app.user.DboxUser;

import java.util.Map;

public interface WebInterfaceContext {
    DboxUser getCurrentUser();
    Object getParameter(String key);
    boolean hasParameter(String key);
    Map<String, Object> toMap();
}
