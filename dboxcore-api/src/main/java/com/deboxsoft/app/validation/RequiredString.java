/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-api
 *  File       : RequiredString.java
 *  ClassName  : RequiredString
 *  Modified   : 14024817
 */

package com.deboxsoft.app.validation;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.Constraint;
import javax.validation.OverridesAttribute;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;
import javax.validation.constraints.Pattern;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = {})
@Documented
@Length
@NotBlank
@Pattern(regexp = ".*", flags = {javax.validation.constraints.Pattern.Flag.DOTALL})
@ReportAsSingleViolation
@Retention(RetentionPolicy.RUNTIME)
@Target({java.lang.annotation.ElementType.METHOD, java.lang.annotation.ElementType.FIELD})
public @interface RequiredString {
    public Class<?>[] groups() default {};

    public String message() default "require String";

    @OverridesAttribute(constraint = Length.class, name = "min")
    public int minimumSize() default 1;

    public Class<? extends Payload>[] payload() default {};

    @OverridesAttribute(constraint = Pattern.class, name = "regexp")
    public String regexp() default "";

    @OverridesAttribute(constraint = Length.class, name = "max")
    public int size() default 125;
}
