/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-api
 *  File       : MigrationSuccessedEvent.java
 *  ClassName  : MigrationSuccessedEvent
 *  Modified   : 14024819
 */

package com.deboxsoft.app.event.migration;

import com.deboxsoft.event.api.AsynchronousPreferred;
import com.deboxsoft.utils.base.NotNull;

@AsynchronousPreferred
public class MigrationSuccessedEvent extends MigrationEndedEvent {
    private static final long serialVersionUID = 1295607088475838545L;

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public MigrationSuccessedEvent(@NotNull final Object source) {
        super(source);
    }
}
