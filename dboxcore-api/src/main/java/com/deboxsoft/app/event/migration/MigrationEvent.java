/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-api
 *  File       : MigrationEvent.java
 *  ClassName  : MigrationEvent
 *  Modified   : 14024819
 */

package com.deboxsoft.app.event.migration;

import com.deboxsoft.app.event.DboxEvent;
import com.deboxsoft.utils.base.NotNull;

public abstract class MigrationEvent extends DboxEvent {
    private static final long serialVersionUID = -6453599879100337213L;

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public MigrationEvent(@NotNull final Object source) {
        super(source);
    }
}
