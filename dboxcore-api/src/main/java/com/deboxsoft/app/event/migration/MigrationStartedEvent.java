/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-api
 *  File       : MigrationStartedEvent.java
 *  ClassName  : MigrationStartedEvent
 *  Modified   : 14024819
 */

package com.deboxsoft.app.event.migration;

import com.deboxsoft.event.api.AsynchronousPreferred;
import com.deboxsoft.utils.base.NotNull;

@AsynchronousPreferred
public class MigrationStartedEvent extends MigrationEvent {
    private static final long serialVersionUID = 447810202967325604L;

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public MigrationStartedEvent(@NotNull final Object source) {
        super(source);
    }
}
