/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-api
 *  File       : DboxEvent.java
 *  ClassName  : DboxEvent
 *  Modified   : 14024814
 */

package com.deboxsoft.app.event;

import com.deboxsoft.app.user.DboxUser;
import com.deboxsoft.utils.base.NotNull;

import java.util.Date;
import java.util.EventObject;

import static com.google.common.base.Preconditions.checkNotNull;

public abstract class DboxEvent extends EventObject {
    private static final long serialVersionUID = 3796176817364443010L;
    private final Date dbx_date;
    private final DboxUser user;


    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    protected DboxEvent(@NotNull final Object source) {
        super(checkNotNull(source));
        this.dbx_date = new Date();
        this.user = null;
    }

    public DboxUser getUser() {
        return user;
    }

    public Date getDate() {
        return dbx_date;
    }
}
