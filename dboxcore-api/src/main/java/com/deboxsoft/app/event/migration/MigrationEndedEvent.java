/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-api
 *  File       : MigrationEndedEvent.java
 *  ClassName  : MigrationEndedEvent
 *  Modified   : 14024819
 */

package com.deboxsoft.app.event.migration;

import com.deboxsoft.utils.base.NotNull;

public abstract class MigrationEndedEvent extends MigrationEvent {
    private static final long serialVersionUID = 301075002889041114L;

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public MigrationEndedEvent(@NotNull final Object source) {
        super(source);
    }
}
