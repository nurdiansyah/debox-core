/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-api
 *  File       : MigrationFailedEvent.java
 *  ClassName  : MigrationFailedEvent
 *  Modified   : 14024819
 */

package com.deboxsoft.app.event.migration;

import com.deboxsoft.event.api.AsynchronousPreferred;
import com.deboxsoft.utils.base.NotNull;

@AsynchronousPreferred
public class MigrationFailedEvent extends MigrationEndedEvent {
    private static final long serialVersionUID = -6322367992630131034L;

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public MigrationFailedEvent(@NotNull final Object source) {
        super(source);
    }
}
