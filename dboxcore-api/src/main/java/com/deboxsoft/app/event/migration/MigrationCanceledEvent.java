/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-api
 *  File       : MigrationCanceledEvent.java
 *  ClassName  : MigrationCanceledEvent
 *  Modified   : 14024819
 */

package com.deboxsoft.app.event.migration;

import com.deboxsoft.event.api.AsynchronousPreferred;
import com.deboxsoft.utils.base.NotNull;

@AsynchronousPreferred
public class MigrationCanceledEvent extends MigrationEndedEvent {
    private static final long serialVersionUID = 5506041147133542921L;

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public MigrationCanceledEvent(@NotNull final Object source) {
        super(source);
    }
}
