/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-api
 *  File       : PluginI18nService.java
 *  ClassName  : PluginI18nService
 *  Modified   : 14025322
 */

package com.deboxsoft.app.i18n;

import java.util.Locale;
import java.util.Map;

public interface PluginI18nService extends I18nService {
    public Map<String, String> getAllTranslationsForPrefix(String dbx_prefix);

    public Map<String, String> getAllTranslationsForPrefix(String dbx_prefix, Locale dbx_locale);

    public String getMessagePattern(String dbx_key, String dbx_fallbackPattern);
}
