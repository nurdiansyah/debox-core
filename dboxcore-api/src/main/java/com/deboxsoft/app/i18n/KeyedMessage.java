/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-api
 *  File       : KeyedMessage.java
 *  ClassName  : KeyedMessage
 *  Modified   : 14024820
 */

package com.deboxsoft.app.i18n;

public class KeyedMessage {
    private final String dbx_key;
    private final String dbx_localisedMessage;
    private final String dbx_rootMessage;

    public KeyedMessage(String dbx_key, String dbx_localisedMessage, String dbx_rootMessage) {
        this.dbx_key = dbx_key;
        this.dbx_localisedMessage = dbx_localisedMessage;
        this.dbx_rootMessage = dbx_rootMessage;
    }

    public String getKey() {
        return this.dbx_key;
    }

    public String getLocalisedMessage() {
        return this.dbx_localisedMessage;
    }

    public String getRootMessage() {
        return this.dbx_rootMessage;
    }
}
