/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-api
 *  File       : I18nService.java
 *  ClassName  : I18nService
 *  Modified   : 14024820
 */

package com.deboxsoft.app.i18n;

import java.util.Locale;

public interface I18nService {
    
    public String getText(Locale dbx_locale, String dbx_key, String dbx_fallbackMessage, Object... dbx_arguments);

    public String getText(String dbx_key, String dbx_fallbackMessage, Object... dbx_arguments);

    public KeyedMessage getKeyedText(String dbx_key, String dbx_fallbackMessage, Object... dbx_arguments);

}
