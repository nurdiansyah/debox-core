/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-api
 *  File       : DboxUser.java
 *  ClassName  : DboxUser
 *  Modified   : 14024814
 */

package com.deboxsoft.app.user;

import com.deboxsoft.app.validation.RequiredString;
import org.hibernate.validator.constraints.Email;

public interface DboxUser {
    public static final String SLUG_REGEXP = "[^\\\\/]+";
    public static final int MAX_SLUG_LENGTH = 127;

    @RequiredString(size = 255)
    public String getDisplayName();

    public Long getId();

    public boolean isActive();

    @Email
    public String getEmail();

    /**
     * Retrieves the "slug" for this user, which is a URL-friendly variant of its name. Each user's slug is guaranteed to be unique within the system. Although URL-friendly, appropriate encoding should still be performed on this slug whenever it is used in a URL.
     *
     * @return
     */
    @RequiredString(size = MAX_SLUG_LENGTH, regexp = SLUG_REGEXP)
    public String getSlug();
}
