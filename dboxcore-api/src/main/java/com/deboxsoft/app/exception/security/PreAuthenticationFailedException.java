/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-api
 *  File       : PreAuthenticationFailedException.java
 *  ClassName  : PreAuthenticationFailedException
 *  Modified   : 14036511
 */

package com.deboxsoft.app.exception.security;

import com.deboxsoft.app.exception.ServiceException;
import com.deboxsoft.app.i18n.KeyedMessage;

public class PreAuthenticationFailedException extends ServiceException {
    public PreAuthenticationFailedException(final KeyedMessage dbx_message) {
        super(dbx_message);
    }
}
