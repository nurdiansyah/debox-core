/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-api
 *  File       : ServiceException.java
 *  ClassName  : ServiceException
 *  Modified   : 14024820
 */

package com.deboxsoft.app.exception;

import com.deboxsoft.app.i18n.KeyedMessage;
import com.deboxsoft.utils.base.NotNull;
import com.deboxsoft.utils.base.Nullable;

import static com.google.common.base.Preconditions.checkNotNull;

public class ServiceException extends RuntimeException {

    private static final long serialVersionUID = -2222363907603455211L;

    private final String dbx_localizedMessage;
    private final String dbx_messageKey;
    private transient KeyedMessage dbx_keyedMessage;


    public ServiceException(final KeyedMessage dbx_message) {
        this(dbx_message, null);
    }

    public ServiceException(@NotNull final KeyedMessage dbx_keyedMessage,
                            @Nullable final Throwable dbx_cause) {
        super(checkNotNull(dbx_keyedMessage, "dbx_keyedMessage").getRootMessage(), dbx_cause);
        dbx_localizedMessage = dbx_keyedMessage.getLocalisedMessage();
        dbx_messageKey = dbx_keyedMessage.getKey();
        this.dbx_keyedMessage = dbx_keyedMessage;

    }

    @NotNull
    public String getLocalizedMessage() {
        return dbx_localizedMessage;
    }

    @NotNull
    public String getMessageKey() {
        return dbx_messageKey;
    }

    @NotNull
    public KeyedMessage getKeyedMessage() {
        return dbx_keyedMessage;
    }
}
