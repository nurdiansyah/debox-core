/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-api
 *  File       : TransferableState.java
 *  ClassName  : TransferableState
 *  Modified   : 14036321
 */

package com.deboxsoft.app.concurrent;

public interface TransferableState {
    public void apply();
    public void remove();
}
