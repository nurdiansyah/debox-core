/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-api
 *  File       : StatefulService.java
 *  ClassName  : StatefulService
 *  Modified   : 14036321
 */

package com.deboxsoft.app.concurrent;

import com.deboxsoft.utils.base.NotNull;
import com.google.common.base.Function;

public interface StatefulService {
    public static final Function<StatefulService, TransferableState> TO_STATE = new Function<StatefulService, TransferableState>() {
        @Override
        public TransferableState apply(final StatefulService input) {
            return input.getState();
        }
    };

    @NotNull
    public TransferableState getState();
}
