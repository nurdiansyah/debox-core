/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-api
 *  File       : PluginState.java
 *  ClassName  : PluginState
 *  Modified   : 140410907
 */

package com.deboxsoft.app.plugin;

public interface PluginState {
    String getName();

    void setName(String name);

    boolean isEnabled();

    void setEnabled(boolean enabled);
}
