/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dbox-core
 *  Module     : dbox-api
 *  File       : Version.java
 *  ClassName  : Version
 *  Modified   : 14024709
 */

package com.deboxsoft.app;

import com.deboxsoft.utils.base.NotNull;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Version implements Comparable<Version> {

    public static final Pattern STRICT_NUMERIC_VALIDATOR = Pattern.compile("^\\d+(\\.\\d+)*$");

    private static final Pattern STARTS_NUMERICAL_PATTERN = Pattern.compile("^(\\d+).*");
    private final List<Integer> version;
    private final String versionString;

    public Version(Integer[] elements) {
        this.version = normalise(Lists.newArrayList(elements));

        this.versionString = StringUtils.join(this.version, ".");
    }

    public Version(String version) {
        String[] components = version.split("[\\.\\-]");

        List<Integer> list = Lists.newArrayListWithCapacity(components.length);
        for (String component : components) {
            try {
                list.add(Integer.valueOf(component));
            } catch (NumberFormatException e) {
                Matcher matcher = STARTS_NUMERICAL_PATTERN.matcher(component);
                if (matcher.matches()) {
                    list.add(Integer.valueOf(matcher.group(1)));
                }

                break;
            }
        }
        this.version = normalise(list);

        this.versionString = StringUtils.join(this.version, ".");
    }

    public int compareTo(@NotNull Version o) {
        int mySize = this.version.size();
        int theirSize = o.version.size();

        int maxSize = Math.max(theirSize, mySize);
        for (int i = 0; i < maxSize; i++) {
            int mine = i < mySize ? this.version.get(i) : 0;
            int theirs = i < theirSize ? o.version.get(i) : 0;

            int diff = mine - theirs;
            if (diff != 0) {
                return diff;
            }
        }

        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (getClass() != obj.getClass())) {
            return false;
        }

        Version other = (Version) obj;
        return this.versionString.equals(other.versionString);
    }

    @NotNull
    public List<Integer> getVersion() {
        return this.version;
    }

    public int hashCode() {
        return this.versionString.hashCode();
    }

    public String toString() {
        return this.versionString;
    }

    private List<Integer> normalise(List<Integer> list) {
        while (list.size() < 3) {
            list.add(0);
        }

        return ImmutableList.copyOf(list);
    }
}

