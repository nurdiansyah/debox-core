/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-api
 *  File       : Operation.java
 *  ClassName  : Operation
 *  Modified   : 14036112
 */

package com.deboxsoft.app.utils;

public interface Operation<T, E extends Throwable> {
    public T perform() throws E;
}
