/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-api
 *  File       : ThreadLocalDelegateExecutorFactory.java
 *  ClassName  : ThreadLocalDelegateExecutorFactory
 *  Modified   : 140411017
 */

package com.deboxsoft.app.executor;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;

public interface ThreadLocalDelegateExecutorFactory {
    Executor createExecutor(Executor paramExecutor);

    ExecutorService createExecutorService(ExecutorService paramExecutorService);

    ScheduledExecutorService createScheduledExecutorService(ScheduledExecutorService paramScheduledExecutorService);

    Runnable createRunnable(Runnable paramRunnable);

    <T> Callable<T> createCallable(Callable<T> paramCallable);
}
