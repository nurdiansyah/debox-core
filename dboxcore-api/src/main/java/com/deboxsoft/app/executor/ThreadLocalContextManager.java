/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-api
 *  File       : ThreadLocalManager.java
 *  ClassName  : ThreadLocalManager
 *  Modified   : 140411020
 */

package com.deboxsoft.app.executor;

public interface ThreadLocalContextManager<T> {
    T getThreadLocalContext();
    void setThreadLocalContext(T param);
    void clearThreadLocalContext();
}
