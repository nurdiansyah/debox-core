/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-api
 *  File       : SecurityService.java
 *  ClassName  : SecurityService
 *  Modified   : 14036112
 */

package com.deboxsoft.app.security;

import com.deboxsoft.app.exception.security.PreAuthenticationFailedException;
import com.deboxsoft.app.utils.Operation;
import com.deboxsoft.utils.base.NotNull;

import javax.management.relation.Role;

public interface SecurityService {

    /**
     * Execute an Operation anonymously (not as any user)
     *
     * @param reason    reason for running as a different user. Helpful for logging/debugging
     * @param operation the Operation to perform.
     * @param <T>       return class Operation.perform()
     * @param <E>       class extends Throwable
     * @return the return value of Operation.perform()
     * @throws Throwable when thrown by Operation.perform()
     */
    public <T, E extends Throwable> T doAnonymous(@NotNull String reason, @NotNull Operation<T, E> operation) throws Throwable;

    /**
     * Execute an Operation as a particular user.
     *
     * @param reason    reason for running as a different user. Helpful for logging/debugging
     * @param username  a String naming the user to impersonate.
     * @param operation the Operation to perform.
     * @param <T>       return class Operation.perform()
     * @param <E>       class extends Throwable
     * @return the return value of Operation.perform()
     * @throws PreAuthenticationFailedException when thrown by Operation.perform()
     */
    public <T, E extends Throwable> T doAsUser(@NotNull String reason, @NotNull String username, @NotNull Operation<T, E> operation) throws PreAuthenticationFailedException;


    /**
     *
     /**
     * Executes an Operation as the current user, but with elevated permission.
     *
     * Note: nested invocations results in all elevated permissions taking effect rather than replacing the previous elevated permissions
     * @param reason reason for running as a different user. Helpful for logging/debugging
     * @param permission a String naming the user to impersonate.
     * @param operation the Operation to perform.
     * @param <T> return class Operation.perform()
     * @param <E> class extends Throwable
     * @return the return value of Operation.perform()
     * @throws Throwable when thrown by Operation.perform()
     */
    public <T, E extends Throwable> T doWithPermission(@NotNull String reason, @NotNull Role permission, @NotNull Operation<T, E> operation) throws Throwable;

}
