/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-api
 *  File       : Permission.java
 *  ClassName  : Permission
 *  Modified   : 14036209
 */

package com.deboxsoft.app.security.permission;

import java.util.Set;

public interface Permission {

    /**
     * Get Id permission
     * @return id
     */
    public int getId();

    /**
     * Get the set of permissions that inherit this permission (excluding this permission).
     * @return set collection permission
     */
    public Set<Permission> getImplyingPermissions();

    /**
     * Gets all permissions this permission inherits.
     * @return set collection permission
     */
    public Set<Permission> getInheritedPermissions();

    /**
     * Gets all permissions that inherit this permission (including this permission).
     * @return set collection permission
     */
    public Set<Permission> getInheritingPermissions();

    public int getWeight();

    public boolean isGlobal();

    public boolean isResources();

}
