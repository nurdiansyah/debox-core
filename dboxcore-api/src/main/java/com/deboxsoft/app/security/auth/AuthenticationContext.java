/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-api
 *  File       : DboxAuthencticationContext.java
 *  ClassName  : DboxAuthencticationContext
 *  Modified   : 14036209
 */

package com.deboxsoft.app.security.auth;

import com.deboxsoft.app.user.DboxUser;
import com.deboxsoft.utils.base.Nullable;

public interface AuthenticationContext {
    public boolean isAuthenticated();

    @Nullable
    public DboxUser getCurrentUser();

    public AuthenticationToken getCurrentToken();

}
