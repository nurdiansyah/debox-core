/*
 * Copyright (c) 2014. deboxsoft Solutions, Inc.
 *  All rights reserved
 *  ======================================================================================
 *
 *  developer  : nurdiansyah
 *  URL        : deboxsoft.com
 *  Project    : dboxcore
 *  Module     : dboxcore-api
 *  File       : AuthenticationToken.java
 *  ClassName  : AuthenticationToken
 *  Modified   : 14036322
 */

package com.deboxsoft.app.security.auth;

import com.deboxsoft.app.user.DboxUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.CredentialsContainer;

public interface AuthenticationToken extends Authentication, CredentialsContainer {
    @Override
    DboxUser getPrincipal();
}
